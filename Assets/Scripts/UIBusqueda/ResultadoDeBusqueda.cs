﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ResultadoDeBusqueda : MonoBehaviour
{
	public GameObject foto;
    public Text titulo;
    public Text agave;
    public EstrellasDeCalificacion estrellas;
	public GameObject progressCarga;
	public GameObject Ficha;
	public GameObject Padre;
    public string id;



	public void ShowResult(CulturaMezcalJsons.SearchResultInfo info)
    {
        titulo.text = info.Nombre;
        agave.text = info.Agave;
        estrellas.MostrarCalificacion(info.Calificacion);
		gameObject.name = info.ID;
        id = info.ID;
        foto.GetComponent<WebImage>().LoadImage(info.Foto);
    }

	public void VerFicha()
    {
		GameObject fichaM = (GameObject)GameObject.Instantiate (Ficha, GetLastParent());
		fichaM.GetComponent<FichaMezcales> ().Iniciar (gameObject.name);
	}

    private Transform GetLastParent()
    {
        Transform currentParent = transform.parent;
        while(currentParent.parent != null)
        {
            currentParent = currentParent.parent;
        }
        return currentParent;
    }

    public void LoadImage(string url) 
    {
        foto.GetComponent<WebImage>().LoadImage(url);
    }
}


