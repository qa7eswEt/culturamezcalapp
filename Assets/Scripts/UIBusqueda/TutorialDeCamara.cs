﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialDeCamara : MonoBehaviour
{
    public static TutorialDeCamara instance;

    public GameObject content;

    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
        }
    }
}
