﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VerProductosRelacionados : MonoBehaviour {
	public GameObject PrefabFichaNueva;
    public GameObject Ficha;
    public GameObject ListaMezcales;
    // Use this for initialization
    void Start () {
        Ficha = gameObject.transform.root.gameObject;
	}
	
	public void ir () {
		var FichaM = (GameObject)Instantiate (PrefabFichaNueva, GetLastParent());
		FichaM.transform.GetComponent<FichaMezcales> ().Iniciar (gameObject.name);
	}

    private Transform GetLastParent()
    {
        Transform currentParent = transform.parent;
        while (currentParent.parent != null)
        {
            currentParent = currentParent.parent;
        }
        return currentParent;
    }

    public void irEtiqueta()
    {

        CulturaMezcalJsons.BuscarTag us = new CulturaMezcalJsons.BuscarTag();
        us.Tag = transform.parent.name;
        us.Valor = transform.parent.GetComponent<Text>().text;

        string json = JsonUtility.ToJson(us);
        WSClient.instance.StartWebService("mezcal/tag", json, Relacionados, (r) => MessageBox.instance.ShowMessage("Hubo un problema de conexión."));
    }
    public void irEtiqueta2()
    {

        CulturaMezcalJsons.BuscarTag us = new CulturaMezcalJsons.BuscarTag();
        us.Tag = transform.parent.name;
        us.Valor = transform.GetComponent<Text>().text;

        string json = JsonUtility.ToJson(us);
        WSClient.instance.StartWebService("mezcal/tag", json, Relacionados2, (r) => MessageBox.instance.ShowMessage("Hubo un problema de conexión."));
    }
    private void Relacionados2(WSResponse res)
    {
        string response = res.responseTxt;
        if (!response.Contains("No Content") && response != null)
        {
            CulturaMezcalJsons.BusquedaTag rt = new CulturaMezcalJsons.BusquedaTag();
            rt = JsonUtility.FromJson<CulturaMezcalJsons.BusquedaTag>(response);

            var listaM = (GameObject)Instantiate(ListaMezcales, transform.root, false);

            Debug.Log(gameObject.transform.parent);
            listaM.transform.localPosition = new Vector2(0, 0);
            listaM.transform.GetComponent<ListaMezcales>().llenar(rt, transform.GetComponent<Text>().text);
        }
    }
    private void Relacionados(WSResponse res)
    {
        string response = res.responseTxt;
        if (!response.Contains("No Content")&&response!=null) {
            CulturaMezcalJsons.BusquedaTag rt = new CulturaMezcalJsons.BusquedaTag();
            rt = JsonUtility.FromJson<CulturaMezcalJsons.BusquedaTag>(response);
         
            var listaM = (GameObject)Instantiate(ListaMezcales,transform.root,false);
            
            Debug.Log(gameObject.transform.parent);
            listaM.transform.localPosition = new Vector2(0, 0);
            listaM.transform.GetComponent<ListaMezcales>().llenar(rt, transform.parent.GetComponent<Text>().text);
        }
    }

 
}
