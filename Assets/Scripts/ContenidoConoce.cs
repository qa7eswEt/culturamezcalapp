﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ContenidoConoce : MonoBehaviour
{
    public GameObject menuPrincipal;
    public GameObject objetoContenido;
    public GameObject[] paginasDeContenido;
    public Button botonDeRegresar;
    public GameObject fondoBlanco;
    public Text Titulo;
    public string[] secciones;

    private int paginaActual;

    void Start()
    {
        
    }

	public void PrenderPagina(int indice)
    {
        Titulo.text = secciones[indice];
        paginaActual = indice;
        botonDeRegresar.gameObject.SetActive(true);
        objetoContenido.SetActive(true);
        foreach(GameObject pag in paginasDeContenido)
        {
            pag.SetActive(false);
        }
        paginasDeContenido[indice].SetActive(true);
        paginasDeContenido[indice].GetComponent<SideAnimation>().Show();
        fondoBlanco.GetComponent<SideAnimation>().FadeInImage();
    }

    public void OpenCulturaSite()
    {
		WebViewManager.Instance.OpenLink("https://culturamezcal.com/compra.html");
    }

    public void EsconderPagina()
    {
        paginasDeContenido[paginaActual].GetComponent<SideAnimation>().Hide();
        Titulo.text = "CONOCE";
        fondoBlanco.GetComponent<SideAnimation>().FadeOutImage();
    }
}
