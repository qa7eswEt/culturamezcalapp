﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class ZoomImage : MonoBehaviour
{

    public GameObject ImagenAmplia;

    
    public void Ampliar()
    {
        ImagenAmplia.transform.parent.gameObject.SetActive(true);
        ImagenAmplia.GetComponent<RawImage>().texture = GetComponent<RawImage>().texture;
    }
}