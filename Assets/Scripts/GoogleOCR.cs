﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using System.Collections.Generic;
using GoogleJsonClasses;
using System;

public class GoogleOCR : MonoBehaviour
{
    public delegate void OCRSuccessEvent(string result, Texture2D photoTaken);

    public OCRSuccessEvent OnOCRSuccess;

    private int maxImageWidth = 1024;
    private string AndroidKey = "AIzaSyAp64B9GRf-T9qEpAkR-8dbbsecGzA2tOw";
    private string IOSKey = "AIzaSyA5Bm0X6gJMMsTzaAXeGI2nvY_tSAv42ms";
    private string base64Image;
    

    public void GetImageFromCamera()
    {

        //PhotoPick.TakePhotoFromCamera(OnImagePicked);
        PhotoPick.TakeImage(OnImagePicked);
        //Debes de darle permisos a la camara
        
    }

    private void OnImagePicked(Texture2D texture)
    {
        WSClient.instance.ShowLoadingScreen(true);
        if (texture.width > maxImageWidth)
        {
            float targetWidth = ((float)texture.width / texture.height) * maxImageWidth;
            TextureScale.Bilinear(texture, Mathf.RoundToInt(texture.width*0.5f), Mathf.RoundToInt(texture.height * 0.5f));
        }
        StartCoroutine(SendImageToOCR(texture));
    }

    private IEnumerator SendImageToOCR(Texture2D texture)
    {
        byte[] imageBytes = texture.EncodeToJPG(50);
        
        System.IO.File.WriteAllBytes(Application.persistentDataPath + "/ScannedImage.jpg", imageBytes);
        Debug.Log("image saved to:" + Application.persistentDataPath + "/ScannedImage.jpg");
        base64Image = Convert.ToBase64String(imageBytes);

        Dictionary<string, string> headersDictionary = new Dictionary<string, string>();
        headersDictionary.Add("Content-Type", "text/json");

        string url = "https://vision.googleapis.com/v1/images:annotate?key=";
#if UNITY_ANDROID
        url = url + AndroidKey;
#elif UNITY_IOS
        url = url + IOSKey;       
#endif
        Requests bodyRequest = new Requests();
        bodyRequest.requests = new AnnotateImageRequest();
        bodyRequest.requests.image = new Image();
        bodyRequest.requests.image.content = System.Convert.ToBase64String(imageBytes);
        bodyRequest.requests.features = new Feature[1];
        bodyRequest.requests.features[0] = new Feature();

        WWW w = new WWW(url, System.Text.Encoding.UTF8.GetBytes(JsonUtility.ToJson(bodyRequest)), headersDictionary);
        yield return w;
        if (!string.IsNullOrEmpty(w.error))
        {
            Debug.Log(w.error);
            WSClient.instance.ShowLoadingScreen(false);
            MessageBox.instance.ShowMessage("Hubo un problema de conexión.");

        }
        else
        {
            Debug.Log(w.text);
            OCRResponses responses = JsonUtility.FromJson<OCRResponses>(w.text);
            if (OnOCRSuccess != null && responses != null) OnOCRSuccess(ParseOCRString(responses), texture);
        }
        WSClient.instance.ShowLoadingScreen(false);
    }

    private string ParseOCRString(OCRResponses json)
    {
        string result = "";
        if (json.responses.Length > 0 && json.responses[0].textAnnotations != null)
        {
            foreach (OCRResponse resp in json.responses)
            {
                foreach (TextAnotation annotation in resp.textAnnotations)
                {
                    result += " " + annotation.description;
                }
            }
        }
        return result;
    }
}

namespace GoogleJsonClasses
{
    [System.Serializable]
    class Requests
    {
        public AnnotateImageRequest requests;
    }

    [System.Serializable]
    class AnnotateImageRequest
    {
        public Image image;
        public Feature[] features;
    }

    [System.Serializable]
    class Image
    {
        /// <summary>
        /// Image content, represented as a stream of bytes. A base64-encoded string.
        /// </summary>
        public string content;
    }

    [System.Serializable]
    class Feature
    {
        public string type = "TEXT_DETECTION";
        public int maxResults = 10;
    }

    [System.Serializable]
    class OCRResponses
    {
        public OCRResponse[] responses;
    }

    [System.Serializable]
    class OCRResponse
    {
        public TextAnotation[] textAnnotations;
    }

    [System.Serializable]
    class TextAnotation
    {
        public string locale;
        public string description;
        public BoundingPoly boundingPoly;
    }

    [System.Serializable]
    class BoundingPoly
    {
        public Vertice[] vertices;
    }

    [System.Serializable]
    class Vertice
    {
        public int x;
        public int y;
    }
}