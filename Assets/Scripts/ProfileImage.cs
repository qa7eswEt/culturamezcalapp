﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ProfileImage : MonoBehaviour
{
    public CameraCut cameraCut;
    //public CropImage cropper;
    public RawImage rawImage;
    public GameObject spinner;
    public bool autoSave = true;

    /// <summary>
    /// Indica si es la primera vez que se hace refresh durante la ejecucion de la app.
    /// </summary>
    private bool hasRefreshHappened = false;

    void OnEnable()
    {
        RefreshImage();
    }

    public void TakeImageFromCamera()
    {
        
       PhotoPick.TakeImage(PhotoPickCallback);
    }

    public void TakeImageFromGallery()
    {
        PhotoPick.TakeFromGAllery(PhotoPickCallback);
    }

    private void PhotoPickCallback(Texture2D texture)
    {
        //WSClient.instance.GenerateThumnbail(texture.EncodeToJPG(50), ThumbnailCallback);
        //cropper.OnCropFinished += ThumbnailCallback;
        //cropper.StartCrop(texture);
        
        cameraCut.StartCrop(texture, ThumbnailCallback);

    }



    private void ThumbnailCallback(Texture2D tex)
    {
        rawImage.gameObject.SetActive(true);

        rawImage.texture = tex;
        if(autoSave)
        {
            Debug.Log("Saving photo to: " + Application.persistentDataPath + "/ProfileImage.jpg");
            System.IO.File.WriteAllBytes(Application.persistentDataPath + "/ProfileImage.jpg", tex.EncodeToJPG());
        }
    }

    public void RefreshImage()
    {
        print("COLOCANDO FOTO: " + PlayerPrefs.HasKey("UserID") + " " + System.IO.File.Exists(Application.persistentDataPath + "/ProfileImage.jpg") + PlayerPrefs.HasKey("FB"));
        if (PlayerPrefs.HasKey("UserID"))
        {
            if(System.IO.File.Exists(Application.persistentDataPath + "/ProfileImage.jpg"))
            {
                rawImage.enabled = true;
                Texture2D tex = new Texture2D(2, 2);
                tex.LoadImage(System.IO.File.ReadAllBytes(Application.persistentDataPath + "/ProfileImage.jpg"));
                rawImage.texture = tex;
            }
            else
            {
                spinner.SetActive(true);
            }
            StartCoroutine(LoadProfilePicture(WSClient.WS_URL + "user/" + PlayerPrefs.GetString("UserID") + "/photo"));
        }
        hasRefreshHappened = true;
    }

    IEnumerator LoadProfilePicture(string url)
    {
        WWW www = new WWW(url);
        yield return www;
        if(string.IsNullOrEmpty(www.error))
        {
            rawImage.enabled = true;
            Texture2D tex = new Texture2D(2, 2, TextureFormat.RGB24, false);
            www.LoadImageIntoTexture(tex);
            System.IO.File.WriteAllBytes(Application.persistentDataPath + "/ProfileImage.jpg", tex.EncodeToJPG(50));
            rawImage.texture = tex;
        }
        else if(PlayerPrefs.HasKey("FB"))
        {
            StartCoroutine(LoadProfilePicture(PlayerPrefs.GetString("FB_PicURL")));
        }
        else
        {
            StartCoroutine(LoadProfilePicture(url));
        }
        spinner.SetActive(false);
    }

    public void Facebook()
    {
        FBController.instance.LoginFB((succes, loginResult) =>
        {
            if (succes) LoginFB(loginResult);
        });
    }

    void LoginFB(FacebookJsons.FBLoginResult result)     // Cargar imagen procedente de Facebook
    {
        Debug.Log("Saving photo to: " + Application.persistentDataPath + "/ProfileImage.jpg");
        Texture2D tex = result.profilePicture as Texture2D;
        System.IO.File.WriteAllBytes(Application.persistentDataPath + "/ProfileImage.jpg", tex.EncodeToJPG(50));
    }
}
