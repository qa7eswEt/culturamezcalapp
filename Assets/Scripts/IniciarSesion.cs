﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class IniciarSesion : MonoBehaviour
{
    public InputField correo;
    public InputField contrasena;
    public GameObject Mensaje;
    public Text etiqueta;
    public GameObject registroPanel;
    public GameObject appleLoginButtons;
    public static IniciarSesion iniciar;

    Coroutine loginCoroutine = null;

    private void Awake()
    {
        if (iniciar == null)
            iniciar = this;
    }
    void Start()
    {
        appleLoginButtons.SetActive(Application.platform == RuntimePlatform.IPhonePlayer);
    }

    public void Iniciar()
    {

        if (correo.text != "" && contrasena.text != "")
        {

            CulturaMezcalJsons.Login log = new CulturaMezcalJsons.Login();
            log.Correo = correo.text;
            log.Password = contrasena.text;
            string json = JsonUtility.ToJson(log);
            WSClient.instance.StartWebService("user/login", json, Login, (r) =>
            {
                switch (r.status)
                {
                    case 401: MessageBox.instance.ShowMessage("Datos de usuario incorrectos."); break;
                    default: MessageBox.instance.ShowMessage("Hubo un problema de conexión."); break;
                }
            });
        }
        else
        {
            MessageBox.instance.ShowMessage("Rellena todos los campos");
        }
    }

    void Login(WSResponse response)
    {
        string result = response.responseTxt;
        CulturaMezcalJsons.Perfil pf = new CulturaMezcalJsons.Perfil();
        pf = JsonUtility.FromJson<CulturaMezcalJsons.Perfil>(result);

        if (correo.text == pf.Correo)
        {
            //Guardar el password y el mail si activo o no la huella
            if (FingerPrintCtrl.fingerPrint.UserWantToUseFingerPrint)
            {
                PlayerDataCtrl.DataCtrl.SavePlayerData(correo.text, contrasena.text);
            }
            else
            {
                PlayerDataCtrl.DataCtrl.SavePlayerData("", "");
            }
            PlayerPrefs.SetString("Correo", correo.text);
            PlayerPrefs.SetString("Password", contrasena.text);
            PlayerPrefs.SetString("UserID", pf.UserID);
            PlayerPrefs.SetString("Nombre", pf.Nombre);
            BarraDeNavegacion.instance.ActivateUserProfile();
            gameObject.SetActive(false);
        }
    }

    void LoginApple(CulturaMezcalJsons.Perfil perfil, string appleId)
    {
        //Guardar el password y el mail si activo o no la huella
        PlayerDataCtrl.DataCtrl.SavePlayerData("", "");
        PlayerPrefs.SetString("Correo", perfil.Correo);
        PlayerPrefs.SetString("UserID", perfil.UserID);
        PlayerPrefs.SetString("Nombre", perfil.Nombre);
        PlayerPrefs.SetInt("Apple", 1);
        PlayerPrefs.SetString("AppleId", appleId);
        BarraDeNavegacion.instance.ActivateUserProfile();
        gameObject.SetActive(false);
    }

    public void Facebook()
    {
        StartCoroutine(LoginWithFB());
    }

    IEnumerator LoginWithFB()
    {
        // Get profile facebook data
        FacebookJsons.FBLoginResult fbLogin = null;
        bool facebookLoginFinished = false;
        bool success = false;
        FBController.instance.LoginFB((succ, loginRes) =>
        {
            facebookLoginFinished = true;
            success = succ;
            fbLogin = loginRes;
        });
        
        yield return new WaitUntil(() => facebookLoginFinished);

        string errorMsg = "Hubo un problema de conexión.";
        CulturaMezcalJsons.Perfil pf = null;
        if (success)
        {
            // Save photo
            Debug.Log("Saving photo to: " + Application.persistentDataPath + "/ProfileImage.jpg");
            Texture2D tex = fbLogin.profilePicture as Texture2D;
            System.IO.File.WriteAllBytes(Application.persistentDataPath + "/ProfileImage.jpg", tex.EncodeToJPG(50));
            
            // Login to CMS
            bool retry = true;
            bool registryChecked = false;
            while (retry)
            {
                retry = false;
                
                // Try login with fb data
                yield return StartCoroutine(
                TryLoginWithFB(fbLogin, (s, p) =>
                    {
                        success = s;
                        pf = p;
                    }
                ));
                
                if (!success && !registryChecked)
                {
                    // Try register, retry on success, only once
                    yield return StartCoroutine(
                    TryRegisterWithFB(fbLogin, (s, error) =>
                        {
                            registryChecked = true;
                            retry = s;
                            if (!s && error != null) errorMsg = error;
                        })
                    );
                }
            }
        }

        if (success)
        {
            PlayerPrefs.SetString("UserID", pf.UserID);
            PlayerPrefs.SetString("Nombre", pf.Nombre);
            PlayerPrefs.SetString("Correo", pf.Correo);
            PlayerPrefs.SetInt("FB", 1);

            BarraDeNavegacion.instance.ActivateUserProfile();
            gameObject.SetActive(false);
        }
        else
        {
            MessageBox.instance.ShowMessage(errorMsg);
        }
    }

    IEnumerator TryLoginWithFB(FacebookJsons.FBLoginResult fbLogin, Action<bool, CulturaMezcalJsons.Perfil> callback = null)
    {
        CulturaMezcalJsons.Login login = new CulturaMezcalJsons.Login();
        login.Correo = fbLogin.id; // Id is both mail and password for faccebook login
        login.Password = fbLogin.id;
        PlayerPrefs.SetString("Password", fbLogin.id);
        
        WSResponse response = null;
        bool success = false;
        bool responseRecieved = false;
        WSClient.instance.StartWebService("user/login", JsonUtility.ToJson(login),
            (r) =>
            {
                responseRecieved = true;
                success = true;
                response = r;
            },
            (r) =>
            {
                responseRecieved = true;
                success = false;
                response = r;
            }
        );

        yield return new WaitUntil(() => responseRecieved);

        callback?.Invoke(success, success ? JsonUtility.FromJson<CulturaMezcalJsons.Perfil>(response.responseTxt) : null);
    }
    
    IEnumerator TryRegisterWithFB(FacebookJsons.FBLoginResult fbLogin, Action<bool, string> successCallback = null)
    {
        var nuevoUsuario = new CulturaMezcalJsons.AddUsuario();
        nuevoUsuario.Nombre = fbLogin.profileName;
        nuevoUsuario.Correo = fbLogin.email;
        nuevoUsuario.Password = fbLogin.id; // Password is id
        nuevoUsuario.Tipo = "FB";

        string errorMsg = null;
        bool success = false;
        bool responseRecieved = false;
        WSClient.instance.StartWebService("user/new", JsonUtility.ToJson(nuevoUsuario),
            (r) =>
            {
                responseRecieved = true;
                success = true;
            },
            (r) =>
            {
                responseRecieved = true;
                success = false;
                if (r.status == 208) errorMsg = "Este usuario ya está registrado."; // User already registered
            }
        );

        yield return new WaitUntil(() => responseRecieved);

        successCallback?.Invoke(success, errorMsg);
    }

    public void OlvidePswd()
    {
        if (!string.IsNullOrEmpty(correo.text))
        {
            OlvidePass json = new OlvidePass();
            json.Correo = correo.text;
            WSClient.instance.StartWebService("user/new/password", JsonUtility.ToJson(json), ResetSuccess, (r) => MessageBox.instance.ShowMessage("Hubo un problema de conexión."));
        }
        else
        {
            MessageBox.instance.ShowMessage("Introduce el correo con el que creaste tu cuenta para reiniciar tu contraseña");
        }
    }

    void ResetSuccess(WSResponse response)
    {
        MessageBox.instance.ShowMessage("Se te enviará un correo para reiniciar tu contraseña a " + correo.text);
    }
    

    public void TryLoginWithAppleId()
    {
#if UNITY_IOS
        if (loginCoroutine == null)
            loginCoroutine = StartCoroutine(LoginWithAppleId());
#endif
    }

#if UNITY_IOS
    IEnumerator LoginWithAppleId()
    {
        // Get apple id
        AppleAuthentication.AppleCredentialsPayload appleCredentialsResponse = null;
        var appleAuthentication = FindObjectOfType<AppleAuthentication>();
        appleAuthentication.TryLogin((res) => appleCredentialsResponse = res);

        yield return new WaitUntil(() => appleCredentialsResponse != null);
        if (appleCredentialsResponse.success)
        {
            // Check if user is missing email and we have it on our pending user collection
            AppleAuthentication.FailedUserData.User failedUser = null;
            if (string.IsNullOrEmpty(appleCredentialsResponse.email))
            {
                var failedUsers = AppleAuthentication.FailedUserData.LoadUserFile();
                failedUser = failedUsers.FindUser(appleCredentialsResponse.user);
                if (failedUser != null)
                {
                    appleCredentialsResponse.fullname = failedUser.fullname;
                    appleCredentialsResponse.email = failedUser.email;
                }
            }

            if (!string.IsNullOrEmpty(appleCredentialsResponse.email))
            {
                // First time apple login, register
                Debug.Log($"First time apple login. Register user.");
                var registerAppleRequest = new CulturaMezcalJsons.RegistroAppleRequest()
                {
                    Nombre = appleCredentialsResponse.fullname,
                    Correo = appleCredentialsResponse.email,
                    AuthorizationCode = appleCredentialsResponse.authorizationCode,
                    IdentityToken = appleCredentialsResponse.identityToken,
                    Tipo = "Apple"
                };

                bool responseRecieved = false;
                CulturaMezcalJsons.RegistroAppleResponse response = null;
                WSClient.instance.StartWebService("user/new/apple", JsonUtility.ToJson(registerAppleRequest),
                (r) =>
                {
                    responseRecieved = true;
                    response = JsonUtility.FromJson<CulturaMezcalJsons.RegistroAppleResponse>(r.responseTxt);

                    // Remove failed user registry
                    if (failedUser != null)
                    {
                        var failedUsers = AppleAuthentication.FailedUserData.LoadUserFile();
                        failedUsers.RemoveUser(failedUser);
                        AppleAuthentication.FailedUserData.SaveUserFile(failedUsers);
                    }
                },
                (r) =>
                {
                    responseRecieved = true;

                    // Manage errors (No conection, repeated registry)
                    if (r.status == 204){ //User already registered
                        // Remove failed user registry
                        if (failedUser != null)
                        {
                            var failedUsers = AppleAuthentication.FailedUserData.LoadUserFile();
                            failedUsers.RemoveUser(failedUser);
                            AppleAuthentication.FailedUserData.SaveUserFile(failedUsers);
                        }
                    } else {
                        // Save user for later registry if failed
                        if (failedUser == null)
                        {
                            var failedUsers = AppleAuthentication.FailedUserData.LoadUserFile();
                            failedUsers.AddUser(new AppleAuthentication.FailedUserData.User()
                            {
                                appleId = appleCredentialsResponse.user,
                                fullname = appleCredentialsResponse.fullname,
                                email = appleCredentialsResponse.email,
                            });
                            AppleAuthentication.FailedUserData.SaveUserFile(failedUsers);
                        }
                    }
                });
                yield return new WaitUntil(() => responseRecieved);

                // Login after register
                if (response != null)
                {
                    LoginApple(new CulturaMezcalJsons.Perfil()
                    {
                        UserID = response.UserID,
                        Foto = null,
                        Nombre = registerAppleRequest.Nombre,
                        Correo = registerAppleRequest.Correo,
                    }, appleCredentialsResponse.user);
                }
            }
            else
            {
                // Login
                var loginAppleRequest = new CulturaMezcalJsons.LoginAppleRequest()
                {
                    AuthorizationCode = appleCredentialsResponse.authorizationCode,
                    IdentityToken = appleCredentialsResponse.identityToken
                };

                bool responseRecieved = false;
                CulturaMezcalJsons.Perfil response = null;
                WSClient.instance.StartWebService("user/login/apple", JsonUtility.ToJson(loginAppleRequest),
                    (r) =>
                    {
                        responseRecieved = true;
                        response = JsonUtility.FromJson<CulturaMezcalJsons.Perfil>(r.responseTxt);
                    },
                    (r) =>
                    {
                        responseRecieved = true;
                    }
                );

                yield return new WaitUntil(() => responseRecieved);
                if (response != null)
                {
                    LoginApple(response, appleCredentialsResponse.user);
                }
            }
        }
        else
        {
            MessageBox.instance.ShowMessage("Error al iniciar sesión con Apple");
        }

        loginCoroutine = null;
    }
#endif
}

public class OlvidePass
{
    public string Correo;
}
