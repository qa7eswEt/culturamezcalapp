﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QuestionsSizeAjuster : MonoBehaviour
{

    public GameObject[] Questions;
    public VerticalLayoutGroup[] VerticalLayouts;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void AdjustQuestionsSize() 
    {
        for(int i = 0; i < Questions.Length; i ++)
        {
            Questions[i].SetActive(true);
            Questions[i].SetActive(false);
            VerticalLayouts[i].enabled = false;
            VerticalLayouts[i].enabled = true;
        }
    }

    public void AdjustQuestionSize(int i) 
    {
        
    }

    IEnumerator EnableQuiestion() 
    {
        yield return new WaitForEndOfFrame();
        
    }
}
