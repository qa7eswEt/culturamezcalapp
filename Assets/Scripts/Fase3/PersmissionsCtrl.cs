﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class PersmissionsCtrl : MonoBehaviour
{
    [Header("UI Canvas Objects")]
    public GameObject PermissionsCanvas;
    public Text MessageText;
    public GameObject AcceptBtn;
    public Text CancelBtnText;

    [Header("Data")]
    public string RequestMessage;
    public string DeniedMessage;
    public string DeniedMessageios;
    public string CancelText;
    public string CloseText;

    [Header("Callbacks")]
    public UnityEvent CallbackSearch;
    public UnityEvent CallbackProfile;

    public enum FromWhere
    {
        Search,
        Profile
    };

    public FromWhere ControlOrigin;

    public static PersmissionsCtrl Persmissions;

    private void Awake()
    {
        if (Persmissions == null)
        {
            Persmissions = this;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }



    public void CheckPermissions(bool isSearch)
    {
        if (isSearch)
        {
            ControlOrigin = FromWhere.Search;
        }
        else
        {
            ControlOrigin = FromWhere.Profile;
        }
        Debug.Log("type asigned " + ControlOrigin);
        if (NativeCamera.CheckPermission() == NativeCamera.Permission.Granted && NativeGallery.CheckPermission() == NativeGallery.Permission.Granted)
        {
            PermissionsCanvas.SetActive(false);
            InvokeCallback();
        }
        else
        {
            Debug.Log("Invoke canvas");
            PermissionsCanvas.SetActive(true);
            MessageText.text = RequestMessage;
            CancelBtnText.text = CancelText;
            AcceptBtn.SetActive(true);
        }
    }

    public void RequestPermission()
    {
        if (NativeCamera.RequestPermission() == NativeCamera.Permission.Granted && NativeGallery.RequestPermission() == NativeGallery.Permission.Granted)
        {
            PermissionsCanvas.SetActive(false);
            InvokeCallback();
        }
        else
        {
            
            CancelBtnText.text = CloseText;
#if UNITY_IOS
            MessageText.text = DeniedMessageios;
#elif UNITY_ANDROID
            MessageText.text = DeniedMessage;
#endif
            CancelBtnText.text = CloseText;
            AcceptBtn.SetActive(false);
        }
    }

    private void InvokeCallback()
    {
        switch (ControlOrigin)
        {
            case FromWhere.Search:
                //CallbackSearch.Invoke();
                StartCoroutine(WaitForMAinSection());
                
                break;
            case FromWhere.Profile:
                //FindObjectOfType<ProfileImage>().TakeImageFromCamera();
                CallbackProfile.Invoke();
                break;
        }
    }

    IEnumerator WaitForMAinSection()
    {
        FindObjectOfType<BarraDeNavegacion>().ActivateSection(2);
        yield return new WaitUntil(() =>  FindObjectOfType<MainSectionController>() != null );
        if (FindObjectOfType<FichaMezcales>() != null)
        {
            Destroy(FindObjectOfType<FichaMezcales>().gameObject); 
        }
        FindObjectOfType<MainSectionController>().ComesFromCamera(true);
    }
}
