﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

/// <summary>
/// Manages 
/// </summary>
public class PlayerDataCtrl : MonoBehaviour
{
    public string FileName;
    public static PlayerDataCtrl DataCtrl;
    public bool HasSavedData;
    
    string DataFilePath;
    BinaryFormatter Binary;
    PlayerData playerData;

    private void Awake()
    {
        if (DataCtrl == null) 
        {
            DataCtrl = this;
        }
        Binary = new BinaryFormatter();
    }

    // Start is called before the first frame update
    void Start()
    {
        playerData = new PlayerData();
        DataFilePath = Application.persistentDataPath + "/" + FileName;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private bool PlayerDataFileExist() 
    {
        return File.Exists(DataFilePath);
    }

    private void LoadData() 
    {
        FileStream fs = new FileStream(DataFilePath, FileMode.Open);
        playerData = (PlayerData)Binary.Deserialize(fs);
        Debug.Log("mail: " + playerData.Mail + " pw: " + playerData.Password);
        fs.Close();
    }

    /// <summary>
    /// Chacks if theres any player data saved
    /// </summary>
    /// <returns></returns>
    public bool CheckIfThereIsPlayerData() 
    {
        HasSavedData = false;
        if (PlayerDataFileExist()) 
        {
            LoadData();
            if(playerData.Mail != "" && playerData.Password != "") 
            {
                HasSavedData = true;
                return true;
            }
            else 
            {
                return false;
            }
        }
        else 
        {
            return false;
        }
    } 


    public void SavePlayerData(string mail, string pw) 
    {
        FileStream fs = new FileStream(DataFilePath, FileMode.Create);
        playerData.Mail = mail;
        playerData.Password = pw;
        Binary.Serialize(fs, playerData);
        fs.Close();
    }


    public PlayerData GetPlayerData() 
    {
        return playerData;
    }
}
