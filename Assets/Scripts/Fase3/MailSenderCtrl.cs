﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Net.Mail;
using System.Net.Mime;
using UnityEngine;
using UnityEngine.UI;
public class MailSenderCtrl : MonoBehaviour
{
    public delegate void SentCorrect();
    public static MailSenderCtrl mailSender;
    public string ImagePath;
    public string Subject;
    public string Body;
    public string DestinationMail;
    public InputField SugestedMezcal;
    public GameObject Loading;
    SentCorrect sentcorrect;
    // Start is called before the first frame update
    private void Awake()
    {
        if (mailSender == null)
            mailSender = this;
    }

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnSendMAilWithoutImage()
    {
        BarraDeNavegacion.instance.loadingSpinner.SetActive(true);
        SendMail(Subject, Body + " " + SugestedMezcal.text, "", DestinationMail);
    }

    public void OnSendMAilWithImage()
    {
        BarraDeNavegacion.instance.loadingSpinner.SetActive(true);
        SendMail(Subject, Body, ImagePath, DestinationMail);
    }

    private void SendMail(string subject, string body, string filePath, string destinationMail)
    {
        try
        {
            
            int smtpPort = 587;
            MailMessage mail = new MailMessage();
            mail.From = new MailAddress("culturamezcalmx@gmail.com", "CulturaMezcal App");
            mail.To.Add(new MailAddress(destinationMail));
            mail.Subject = subject.Trim();
            mail.Body = body;
            //mail.Headers.Add("X-SMTPAPI", xsmtpapiJSON);
            Debug.Log("Image path: " + filePath);
            if (!string.IsNullOrEmpty(filePath))
            {
                Attachment attachment = new Attachment(filePath);
                attachment.ContentDisposition.Inline = true;
                attachment.ContentDisposition.DispositionType = DispositionTypeNames.Inline;
                attachment.ContentId = "myimagecid";
                mail.Attachments.Add(attachment);
                attachment.ContentType.MediaType = "image/jpg";
                attachment.ContentType.Name = Path.GetFileName(filePath);
            }
            SmtpClient smtpServer = new SmtpClient("smtp.sendgrid.net");
            smtpServer.Port = smtpPort;
            string smtpUsername = "apikey"; //sendgrid username
            string smtpPassword = "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";
            smtpServer.Credentials = new System.Net.NetworkCredential(smtpUsername, smtpPassword);
            smtpServer.EnableSsl = true;
            bool smtpEnabledSsl = false;
            if (smtpPort != 25)
            {
                smtpEnabledSsl = true;
            }
            smtpServer.EnableSsl = smtpEnabledSsl;
            smtpServer.UseDefaultCredentials = false;
            //client.Credentials = credentials;
            smtpServer.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
            smtpServer.Send(mail);
            
            MessageBox.instance.ShowMessage("Correo enviado.", 0, SetactiveLoading);
            //
        }
        catch (Exception e)
        {
            Debug.Log(e);
            MessageBox.instance.ShowMessage("El correo no pudo ser enviado.", 0, SetactiveLoading);
        }
    }

    public void SetactiveLoading()
    {
        BarraDeNavegacion.instance.loadingSpinner.SetActive(false);
    }
}
