﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

/// <summary>
/// Player  data container for Log in by printer finger
/// </summary>
[Serializable]
public class PlayerData
{
    public string Mail;
    public string Password;
}
