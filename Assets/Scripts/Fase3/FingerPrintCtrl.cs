﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
#if UNITY_IOS
using DeadMosquito.IosGoodies;
using JetBrains.Annotations;
#endif

#if UNITY_ANDROID
using DeadMosquito.AndroidGoodies;
using JetBrains.Annotations;
#endif
//implementation "com.android.support:support-v4:+"
public class FingerPrintCtrl : MonoBehaviour
{
    public Toggle CanUseFingerPrintToggle;
    public InputField MailInput;
    public InputField PasswordInput;
    public GameObject CanUseFingerPrintGO;
    public GameObject FingerPrintPanelGO;
    public Color Normal;
    public Color Error;
    public Text Instructions;
    public static FingerPrintCtrl fingerPrint;
    public bool CanUseFingerPrint;
    public bool UserWantToUseFingerPrint;
    private void Awake()
    {
        if (fingerPrint == null)
            fingerPrint = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        CanUseFingerPrint = false;
        
         #if UNITY_IOS
        #if !DISABLE_BIOMETRIC_AUTH
                CanUseFingerPrint = IGLocalAuthentication.IsLocalAuthenticationAvailable;
        #endif
        #elif UNITY_ANDROID
                CanUseFingerPrint = AGFingerprintScanner.HasFingerprintHardware && AGFingerprintScanner.HasEnrolledFingerprint;
        #endif
         

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void EnableUseFingerPrintCapability() 
    {
        CanUseFingerPrintGO.SetActive(CanUseFingerPrint);
    }

    public void SetCanUseFingerPrint() 
    {
        UserWantToUseFingerPrint = CanUseFingerPrintToggle.isOn;
        if(!UserWantToUseFingerPrint)
        {
            PasswordInput.text = "";
            MailInput.interactable = true;
            PasswordInput.interactable = true;
        }
    }

    public void SetupFingerPrintValidation() 
    {
        EnableUseFingerPrintCapability();
        if (CanUseFingerPrint) 
        {
            if (PlayerDataCtrl.DataCtrl.CheckIfThereIsPlayerData()) 
            {
                PlayerData auxPlayerData = PlayerDataCtrl.DataCtrl.GetPlayerData();
                MailInput.text = auxPlayerData.Mail;
                PasswordInput.text = auxPlayerData.Password;
                MailInput.interactable = false;
                PasswordInput.interactable = false;
                CanUseFingerPrintToggle.isOn = true;
                UserWantToUseFingerPrint = true;
#if UNITY_IOS
                                ValidateIOS();
                #endif
                #if UNITY_ANDROID
                                ValidateAndroid();
                #endif
            }
            else 
            {
                MailInput.text = "";
                PasswordInput.text = "";
                MailInput.interactable = true;
                PasswordInput.interactable = true;
                CanUseFingerPrintToggle.isOn = false;
                UserWantToUseFingerPrint = false;
            }
        }
        else 
        {
            FingerPrintPanelGO.SetActive(false);
        }
    }



    
      #if UNITY_IOS
        private void ValidateIOS()
        {
    #if !DISABLE_BIOMETRIC_AUTH
            if (IGLocalAuthentication.IsLocalAuthenticationAvailable)
            {
                const IGLocalAuthentication.Policy policy = IGLocalAuthentication.Policy.DeviceOwnerAuthenticationWithBiometrics;
                IGLocalAuthentication.AuthenticateWithBiometrics("Por favor, confirma tu identidad", policy,
                    () => { IniciarSesion.iniciar.Iniciar(); },
                    error => Debug.Log("Authentication failed: " + error));
            }
            else
            {
                Debug.Log("Device does not support biometric authentication.");
            }
    #endif
        }
    #endif

    #if UNITY_ANDROID
        [UsedImplicitly]
        public void ValidateAndroid() 
        {
            Instructions.text = "Toque el sensor de huella para iniciar sesión";

            AGFingerprintScanner.Authenticate(
                    () =>
                    {
                        FingerPrintPanelGO.SetActive(false);
                        IniciarSesion.iniciar.Iniciar();
                        Debug.Log("Fingerprint authentication sucessful");
                    },
                    warning =>
                    {
                        Instructions.text = "Error de lectura, inténtalo de nuevo";
                        Instructions.color = Error;
                        Debug.Log("Fingerprint authentication failed with warning: " + warning);
                    },
                    error =>
                    {
                        OnCancelAuth();
                        Debug.Log("Fingerprint authentication failed with error: " + error);
                    });
            Instructions.color = Normal;
            FingerPrintPanelGO.SetActive(true);
        }

        public void OnCancelAuth()
        {
            Debug.Log("Attempting to cancel fingerprint auth...");
            FingerPrintPanelGO.SetActive(false);
            AGFingerprintScanner.Cancel();

        }
    #endif
     
}

