using UnityEngine;

public class WebViewManager : Singleton<WebViewManager>
{
    [SerializeField] GameObject WebViewPrefab;
    public void OpenLink(string url)
    {
        GameObject webViewObj = Instantiate(WebViewPrefab);
        webViewObj.GetComponent<WebViewController>().OpenLink(url);
    }
}
