﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WebViewAux : MonoBehaviour
{
    public string url;

    public void OpenLink()
    {
        WebViewManager.Instance.OpenLink(url);
    }

   public void OnClickBtn() 
   {
        WebViewManager.Instance.OpenLink("https://culturamezcal.com/horeca ");
   }
   
   public void OpenLinkExternal()
   {
       Application.OpenURL(url);
   }
}
