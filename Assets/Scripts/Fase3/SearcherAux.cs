﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SearcherAux : MonoBehaviour
{
   
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnSearchFromNotFound()
    {
        BarraDeNavegacion.instance.ActivateSection(3);
        MainSectionController.instance.ComesFromCamera(false);
        BarraDeNavegacion.instance.ActivateSection(3);
        MainSectionController.instance.ComesFromCamera(false);
    }

    public void OnSearchBtn() 
    {
        BarraDeNavegacion.instance.ActivateSection(3);
        StartCoroutine(WaitMainMenu());
    }

    IEnumerator WaitMainMenu()
    {
        //yield return new WaitForSeconds(1.5f);
        yield return new WaitWhile(() => GameObject.FindObjectOfType<MainSectionController>() == null);
        Debug.Log("OnCorutine");
        MainSectionController.instance.ComesFromCamera(false);
        //GameObject.FindObjectOfType<MainSectionController>().GetComponent<MainSectionController>().ComesFromCamera(false);
   
    }


}
