﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using System.Collections.Generic;

public class CameraTestMicrosoft : MonoBehaviour {

	// Use this for initialization
	void Start ()
    {
        
    }
	
	public void GetImageFromCamera()
    {
        AndroidCamera.Instance.OnImagePicked += OnImagePicked;
        AndroidCamera.Instance.GetImageFromCamera();
    }

    private void OnImagePicked(AndroidImagePickResult result)
    {
        Debug.Log("OnImagePicked");
        if (result.IsSucceeded)
        {
            print("Succeeded, path: " + result.ImagePath);

            byte[] imageBytes = System.IO.File.ReadAllBytes(result.ImagePath);

#if DEBUG
            System.IO.File.WriteAllBytes(Application.persistentDataPath + "/foto.jpg", imageBytes);
            print("image saved to:" + Application.persistentDataPath + "/foto.jpg");
#endif

            StartCoroutine(SendImageToOCR(imageBytes));
        }
        else
        {
            //AN_PoupsProxy.showMessage("Image Pick Rsult", "Failed");
        }
        AndroidCamera.Instance.OnImagePicked -= OnImagePicked;
    }

    private IEnumerator SendImageToOCR(byte[] imageBytes)
    {
        Dictionary<string, string> headersDictionary = new Dictionary<string, string>();
        headersDictionary.Add("Content-Type", "application/octet-stream");
        headersDictionary.Add("Ocp-Apim-Subscription-Key", "14b23ee5ff6f45e2928ed15a9212f40a");

        string url = "https://api.projectoxford.ai/vision/v1.0/ocr?es&true";

        WWW w = new WWW(url, imageBytes, headersDictionary);
        print("sending photo...");
        yield return w;
        if (!string.IsNullOrEmpty(w.error))
        {
            print(w.error);
        }
        else
        {
            print(w.text);
        }
    }

}
