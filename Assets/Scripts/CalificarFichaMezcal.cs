﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
public class CalificarFichaMezcal : MonoBehaviour {
	public Sprite Magueylleno;
	public Sprite Magueyvacio;
	public GameObject Mensaje;
	public GameObject Ficha;
	public GameObject Perfil;

    private void Start()
    {
        Perfil = GameObject.FindGameObjectWithTag("Monito");        // relacion con boton del Menu de Perfil
    }
    public void Calificar()
    {
        if (PlayerPrefs.HasKey("UserID"))
        {
            limpiar();                                                  // asignar mageys en Blanco 
            foreach (Transform item in gameObject.transform.parent)
            {                                                           // Acceder a todos los hijos del objeto en comun que engloba a todos las calificaciones del 1 al 5
                item.GetComponent<Image>().sprite = Magueylleno;        // asignar Mageylleno 
                if (item == gameObject.transform)
                {

                    CulturaMezcalJsons.Calificar ca = new CulturaMezcalJsons.Calificar();
                    ca.UserID = Ficha.transform.GetComponent<FichaMezcales>().us.UserID;
                    ca.ProductID = Ficha.transform.GetComponent<FichaMezcales>().us.ProductID;
                    ca.Calificacion = Int32.Parse(item.name);
                    string json = JsonUtility.ToJson(ca);
                    WSClient.instance.StartWebService("user/eval", json, Enviado, (r) => MessageBox.instance.ShowMessage("Hubo un problema de conexión."));
                    break;                                              // si el Item corresponde con el Tocado romper Ciclo
                }
            }
        }
        else
        {

            GameObject.FindObjectOfType<LoginPerfil>().Iniciar();     // Si no cuena con Id no tiene  Perfil por lo tanto Mostrar menu de LoginUsuario
        }
		
	}
	void limpiar(){
		foreach (Transform item in gameObject.transform.parent) {	// acceder a todos las calificaciones por medio del padre del boton clickeado
			item.GetComponent<Image> ().sprite = Magueyvacio;		// Asignar MagueyVacio a todos los elementos
		}
	}
	void Enviado(WSResponse response){
        string result = response.responseTxt;
		CulturaMezcalJsons.Exito ex = new CulturaMezcalJsons.Exito();
		ex=JsonUtility.FromJson<CulturaMezcalJsons.Exito>(result);
		if (!ex.Success) {
            MessageBox.instance.ShowMessage(" Problemas para conectar con el Servidor, Intente mas Tarde");
		}
	}
}
