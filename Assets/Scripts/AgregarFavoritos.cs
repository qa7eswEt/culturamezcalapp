﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class AgregarFavoritos : MonoBehaviour
{
    public Sprite CorazonVacio;
    public Sprite Corazonlleno;
    public CulturaMezcalJsons.UsuarioID us;

    private void Start()
    {
        us = new CulturaMezcalJsons.UsuarioID();
    }

    public void AddFavoritos()
    {
        string id = PlayerPrefs.GetString("UserID");
        us.UserID = id;
        us.ProductID = transform.parent.name;
        string json = JsonUtility.ToJson(us);
        if (transform.GetComponent<Image>().sprite == CorazonVacio)
        {
                                                                   // Mandar Json para añadir este mezcal a ala lista
            WSClient.instance.StartWebService("user/favorite/add", json, agregar, (r) => MessageBox.instance.ShowMessage("Hubo un problema de conexión."));
        }
        else
        {                                                                                               // si es diferente entonces Quitar este Mezcal de la lista de Favoritos

            WSClient.instance.StartWebService("user/favorite/remove", json, quitar, (r) => MessageBox.instance.ShowMessage("Hubo un problema de conexión."));
        }

    }
    void agregar(WSResponse response)
    {
        string result = response.responseTxt;
        CulturaMezcalJsons.Exito ex = new CulturaMezcalJsons.Exito();
        ex = JsonUtility.FromJson<CulturaMezcalJsons.Exito>(result);            // si la respuesta del Js es True 
        if (ex.Success)
        {
            transform.GetComponent<Image>().sprite = Corazonlleno;          // proceder a Cambiar imagen de Corazon LLeno
        }
    }
    // Metodo Para verificar Quitar de lista de Favoritos
    void quitar(WSResponse response)
    {
        string result = response.responseTxt;
        CulturaMezcalJsons.Exito ex = new CulturaMezcalJsons.Exito();       // si la Respuesta es True
        ex = JsonUtility.FromJson<CulturaMezcalJsons.Exito>(result);
        if (ex.Success)
        {
            transform.GetComponent<Image>().sprite = CorazonVacio;          // Cambiar a Imagen de Corazon Vacio
        }
    }
}
