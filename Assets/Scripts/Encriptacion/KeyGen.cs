﻿using UnityEngine;
using System.Collections;

public class KeyGen : MonoBehaviour
{
    private static System.Random r0;

    void Start()
    {
        Debug.Log(GenKey());
    }

    /// <summary>
    /// Genera dinamicamente la llave de encriptacion
    /// </summary>
    /// <returns></returns>
    public static byte[] GenKey()
    {
        r0 = new System.Random(3544174);
        System.Random r1 = new System.Random(r0.Next());
        byte[] key = new byte[32];
        r1.NextBytes(key);
        return key;
    }
}
