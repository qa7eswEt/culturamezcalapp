﻿using UnityEngine;
using System.Collections;
using System.Security.Cryptography;
using System.Text;
using System.IO;

public class Cryptor : MonoBehaviour
{
    public static System.Random r0;

    public void Start()
    {

    }

    /// <summary>
    /// Recibe la respuesta de un webservice, limpia, desencripta y regresa el json como string.
    /// </summary>
    /// <param name="webServiceResponce">Json a desencriptar</param>
    /// <returns></returns>
    public static string DecryptWSString(string webServiceResponce)
    {
        if (string.IsNullOrEmpty(webServiceResponce))
        {
            Debug.LogError("Se intento desencriptar string null o empty");
            return "";
        }
        Debug.Log("Desencriptando: " + webServiceResponce);
        EncryptedWS wsJson = JsonUtility.FromJson<EncryptedWS>(webServiceResponce);
        string result = Decrypt(wsJson.a.Replace(@"\", ""), wsJson.b.Replace(@"\", ""));
        Debug.Log("Resultado: " + result);
        return result;
    }

    /// <summary>
    /// Recibe el json a encriptar como string, crea un vector, lo acomoda en un json y regresa el string encriptado
    /// </summary>
    /// <param name="webServiceString">Json a encriptar</param>
    /// <returns></returns>
    public static string EncryptWSString(string webServiceString)
    {
        byte[] vector = new byte[16];
        System.Random random = new System.Random();
        random.NextBytes(vector);

        EncryptedWS post = new EncryptedWS();
        post.a = Encrypt(webServiceString, vector);
        post.b = System.Convert.ToBase64String(vector);
        return JsonUtility.ToJson(post);
    }

    /// <summary>
    /// Encripta un string
    /// </summary>
    /// <param name="unencrypted">Mensaje a encriptar</param>
    /// <param name="vector">Vector a usar</param>
    /// <returns></returns>
    private static string Encrypt(string unencrypted, byte[] vector)
    {
        UTF8Encoding encoder = new UTF8Encoding();
        return System.Convert.ToBase64String(EncryptBytes(encoder.GetBytes(unencrypted), vector));
    }

    /// <summary>
    /// Desencripta un string
    /// </summary>
    /// <param name="encrypted">Mensaje a desencriptar</param>
    /// <param name="vector">Vector a usar</param>
    /// <returns></returns>
    private static string Decrypt(string encrypted, string vector)
    {
        UTF8Encoding encoder = new UTF8Encoding();
        byte[] buffer = System.Convert.FromBase64String(encrypted);
        byte[] vectorBytes = System.Convert.FromBase64String(vector);
        return encoder.GetString(DecryptBytes(buffer, vectorBytes));
    }

    /// <summary>
    /// Encripta un arreglo de bytes
    /// </summary>
    /// <param name="buffer">Arreglo de bytes a encriptar</param>
    /// <param name="vector">Vector a usar</param>
    /// <returns></returns>
    private static byte[] EncryptBytes(byte[] buffer, byte[] vector)
    {
        RijndaelManaged rm = new RijndaelManaged();
        rm.Mode = CipherMode.CFB;
        rm.Padding = PaddingMode.PKCS7;
        ICryptoTransform encryptor = rm.CreateEncryptor(KeyGen.GenKey(), vector);
        return Transform(buffer, encryptor);
    }

    /// <summary>
    /// Desencripta un arreglo de bytes
    /// </summary>
    /// <param name="buffer">Arreglo de bytes a desencriptar</param>
    /// <param name="vector">Vector a usar</param>
    /// <returns></returns>
    private static byte[] DecryptBytes(byte[] buffer, byte[] vector)
    {
        RijndaelManaged rm = new RijndaelManaged();
        rm.Mode = CipherMode.CFB;
        rm.Padding = PaddingMode.None;
        ICryptoTransform decryptor = rm.CreateDecryptor(KeyGen.GenKey(), vector);
        return Transform(buffer, decryptor);
    }

    /// <summary>
    /// Transforma un arreglo de bytes con el transform cryptografico pasado
    /// </summary>
    /// <param name="buffer"></param>
    /// <param name="transform"></param>
    /// <returns></returns>
    protected static byte[] Transform(byte[] buffer, ICryptoTransform transform)
    {
        MemoryStream stream = new MemoryStream();
        using (CryptoStream cs = new CryptoStream(stream, transform, CryptoStreamMode.Write))
        {
            cs.Write(buffer, 0, buffer.Length);
        }
        return stream.ToArray();
    }
}

/// <summary>
/// Json para enviar mensajes encriptados
/// </summary>
public class EncryptedWS
{
    /// <summary>
    /// Base64 encrypted JSON string
    /// </summary>
    public string a = "";
    /// <summary>
    /// Base64 VI
    /// </summary>
    public string b = "";
}
