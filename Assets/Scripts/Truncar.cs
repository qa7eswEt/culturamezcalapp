﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Truncar : MonoBehaviour {

    public void Trunca( int carracteres)
    {
        if (gameObject.GetComponent<Text>().text.Length > carracteres)
        {
            string substring = gameObject.GetComponent<Text>().text.Substring(0, carracteres);
            substring += "...";
            gameObject.GetComponent<Text>().text= substring;
        }
    }
}
