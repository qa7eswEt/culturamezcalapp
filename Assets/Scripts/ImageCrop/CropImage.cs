﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CropImage : MonoBehaviour
{
    public Vector2 referenceResolution;
    public Texture2D testTexture;
    public RectTransform mask;
    public RawImage rawImage;
    public RectTransform rawImageContainer;
    public ScrollRect scroll;
    public Camera screenshotCamera;
    //public Camera mainCamera;
    public Canvas screenShotCanvas;
    public Canvas cropCanvas;
    //public GameObject uiScrollCanvas;

    public delegate void CropFinished(byte[] croppedImage);
    public CropFinished OnCropFinished;

    void Start()
    {
        //StartCrop(testTexture);
    }

	public void StartCrop(Texture2D texture)
    {
        gameObject.SetActive(true);
        scroll.transform.SetParent(cropCanvas.transform);
        scroll.transform.localScale = new Vector3(1, 1, 1);
        scroll.GetComponent<RectTransform>().offsetMax = Vector2.zero;
        scroll.GetComponent<RectTransform>().offsetMin = Vector2.zero;
        scroll.transform.SetSiblingIndex(1);

        rawImage.texture = texture;

        Debug.Log(texture.width + "x" + texture.height);
        if(texture.width > texture.height)
        {
            rawImage.transform.localRotation = Quaternion.Euler(0, 0, -90);
            rawImage.GetComponent<RectTransform>().pivot = new Vector2(0, 0.5f);
            rawImage.GetComponent<RectTransform>().anchoredPosition = Vector2.zero;
        }

        rawImage.SetNativeSize();
        if (rawImage.rectTransform.sizeDelta.x > referenceResolution.x &&
            rawImage.rectTransform.sizeDelta.y > referenceResolution.y)
        {
            rawImageContainer.sizeDelta = rawImage.rectTransform.sizeDelta;
        }
        else
        {
            rawImageContainer.sizeDelta = new Vector2(referenceResolution.x, referenceResolution.y);
        }
        rawImage.enabled = true;
    }

    public void CropButton()
    {
        scroll.transform.SetParent(mask.transform.parent);
        scroll.transform.localScale = new Vector3(1, 1, 1);
        scroll.GetComponent<RectTransform>().offsetMax = Vector2.zero;
        scroll.GetComponent<RectTransform>().offsetMin = Vector2.zero;

        //mainCamera.enabled = false;
        //screenshotCamera.enabled = true;
        //uiScrollCanvas.SetActive(false);
        ////cropCanvas.gameObject.SetActive(false);

        RenderTexture tempRT = new RenderTexture(screenshotCamera.pixelWidth, screenshotCamera.pixelHeight, 0, RenderTextureFormat.Default);
        screenshotCamera.targetTexture = tempRT;
        screenshotCamera.Render();

        RenderTexture.active = tempRT;
        Texture2D virtualPhoto =
            new Texture2D(Mathf.CeilToInt(mask.rect.width * screenShotCanvas.scaleFactor),
            Mathf.CeilToInt(mask.rect.height * screenShotCanvas.scaleFactor), 
            TextureFormat.RGB24, false);

#if !UNITY_ANDROID || UNITY_EDITOR
        virtualPhoto.ReadPixels(
        new Rect(screenshotCamera.WorldToScreenPoint(mask.position).x,
        screenshotCamera.pixelHeight - screenshotCamera.WorldToScreenPoint(mask.position).y,
        mask.rect.width * screenShotCanvas.scaleFactor,
        mask.rect.height * screenShotCanvas.scaleFactor),
        0, 0);
#else
        virtualPhoto.ReadPixels(
        new Rect(screenshotCamera.WorldToScreenPoint(mask.position).x,
        screenshotCamera.WorldToScreenPoint(mask.position).y - mask.rect.height * cropCanvas.scaleFactor,
        mask.rect.width * cropCanvas.scaleFactor,
        mask.rect.height * cropCanvas.scaleFactor),
        0, 0);
#endif

        RenderTexture.active = null;
        screenshotCamera.targetTexture = null;
        Destroy(tempRT);

#if DEBUG
        print(Application.persistentDataPath + "/test.jpg");
        System.IO.File.WriteAllBytes(Application.persistentDataPath + "/test.jpg", virtualPhoto.EncodeToJPG(100));
#endif
        OnCropFinished(virtualPhoto.EncodeToJPG(100));
        
        gameObject.SetActive(false);
    }

    public void CancelCropButton()
    {
        if (OnCropFinished != null) OnCropFinished(null);
        gameObject.SetActive(false);
    }
}
