﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PinchToZoom : MonoBehaviour
{
    public float maximumScale = 2;
    public float minimumScale = 1;

    public Graphic objectToScale;
    public ScrollRect scroll;
    public float scrollSpeed;

    private float pixelDensity;

	// Use this for initialization
	void Start ()
    {
        pixelDensity = Screen.height;
	}
	
	// Update is called once per frame
	void Update ()
    {
		if(Input.touchCount == 2)
        {
            scroll.horizontal = false;
            scroll.vertical = false;
            Touch firstTouch = Input.GetTouch(0);
            Touch secondTouch = Input.GetTouch(1);

            Vector2 firstTouchPrevPos = firstTouch.position - firstTouch.deltaPosition;
            Vector2 secondTouchPrevPos = secondTouch.position - secondTouch.deltaPosition;

            float prevDelta = (firstTouchPrevPos - secondTouchPrevPos).magnitude;
            float currDelta = (firstTouch.position - secondTouch.position).magnitude;

            float scaleFactor = prevDelta - currDelta;
            Vector3 scaleVector = new Vector3((-scaleFactor / pixelDensity) * scrollSpeed, (-scaleFactor / pixelDensity) * scrollSpeed, 0);
            Vector3 resultVector = scaleVector + objectToScale.transform.localScale;
            if (resultVector.x <= minimumScale || resultVector.y <= minimumScale)
            {
                objectToScale.transform.localScale = new Vector3(minimumScale, minimumScale, objectToScale.transform.localScale.z);
            }
            else if(resultVector.x >= maximumScale || resultVector.y >= maximumScale)
            {
                objectToScale.transform.localScale = new Vector3(maximumScale, maximumScale, objectToScale.transform.localScale.z);
            }
            else
            {
                objectToScale.transform.localScale = resultVector;
            }
        }
        else
        {
            scroll.horizontal = true;
            scroll.vertical = true;
        }
	}
}
