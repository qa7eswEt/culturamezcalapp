﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BarraDeNavegacion : MonoBehaviour
{
    public string conoceSceneName;
    public string mainSceneName;
    public GameObject loadingSpinner;
    public ButtonSelectedSprite[] buttons;          // imagenes prendidas para las secciones 

    public static BarraDeNavegacion instance;

    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
            GameObject.DontDestroyOnLoad(gameObject);
        }
        else
        {
            GameObject.Destroy(gameObject);
        }
    }

    public void ActivateSection(int canvasIndex)
    {
        Debug.Log("ActivateSection: " + canvasIndex);
        StartCoroutine(LoadSceneForSection(canvasIndex));
    }

    private IEnumerator LoadSceneForSection(int canvasIndex)
    {
        loadingSpinner.SetActive(true);
        if(canvasIndex == 1)
        {
            if (SceneManager.GetActiveScene().name != conoceSceneName)
            {
                TurnButtonOn(canvasIndex);
                var scene = SceneManager.LoadSceneAsync(conoceSceneName);
                yield return new WaitUntil(() => scene.isDone);
            }
        }
        else
        {
            if(SceneManager.GetActiveScene().name != mainSceneName)
            {
                var scene = SceneManager.LoadSceneAsync(mainSceneName);
                yield return new WaitUntil(() => scene.isDone);
            }
            if (canvasIndex == 4)
            {
                ActivateUserProfile();
            }
            else if(canvasIndex == 2)
            {
                TutorialDeCamara.instance.content.SetActive(true);
                Debug.Log("Camera tutorial");
                TurnButtonOn(canvasIndex);
            }
            else
            {
                Transform.FindObjectOfType<MainSectionController>().ActivateSection(canvasIndex);
                if(canvasIndex != 3) 
                {
                    TurnButtonOn(canvasIndex);
                }
            }
            
        }

        loadingSpinner.SetActive(false);
    }

    public void ActivateUserProfile()
    {
        Transform.FindObjectOfType<MainSectionController>().ActivateUserProfile();
    }

    public void TurnButtonOn(int canvasIndex)
    {
        foreach (ButtonSelectedSprite button in buttons)
        {
            button.ChangeSpriteState(false);                                        // aplicar sprite correpondiente
        }
        if (buttons[canvasIndex] != null) buttons[canvasIndex].ChangeSpriteState(true);
    }


}
