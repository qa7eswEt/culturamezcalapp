﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
public class DatosMezcalLista : MonoBehaviour {

    public Text Nombre;
    public GameObject Calificacion;
    public WebImage Foto;
    public Text Agave;
    public Button Url;
    public Sprite Magueylleno;
    public GameObject Ficha;
    public void Iniciar(CulturaMezcalJsons.ResultadosTag res)       // Datos devueltos de la busquedad por Tag 
    {
        Agave.text = res.Agave;
        Nombre.text = res.Nombre;
        foreach (Transform item in Calificacion.transform)  
        {   // Estrellas del Mezcal Cal, Promedio
            int k = Int32.Parse(item.name);
            if (k <= res.Calificacion)
            {
                item.GetComponent<Image>().sprite = Magueylleno; 
            }
        }
        Foto.LoadImage(res.Foto);
        gameObject.name = res.ID;

    }

    public void VerFicha()                                          // el boton puede crear mas Fichas
    {
        GameObject fichaM = (GameObject)GameObject.Instantiate(Ficha, GetLastParent());
        fichaM.GetComponent<FichaMezcales>().Iniciar(gameObject.name);
    }

    private Transform GetLastParent()
    {
        Transform currentParent = transform.parent;
        while (currentParent.parent != null)
        {
            currentParent = currentParent.parent;
        }
        return currentParent;
    }

}
