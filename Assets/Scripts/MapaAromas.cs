﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;

public class MapaAromas : MonoBehaviour {
	public Sprite encendido;
	public Sprite Apagado;
	// Use this for initialization
	public RectTransform[] union;
	public GameObject[] Lineas;
	List<GameObject> Estrella = new List<GameObject>();
    public Text Resultado;
    public Text RSensacion;



	public void llamar(){
		CeroRepetidos ();
		var padre = transform.parent.transform.parent;  // Componenetes
		for (int i = 0; i < padre.childCount; i++) {
			if (transform.parent == padre.GetChild(i)) {
			if(i < padre.childCount){
					int j = i;
					if (i == 12){								// reacomodo a posicion inicial
					    i=-1;
						j = 12;
					}
					if (i == 0) {							// reacomodo a posicion final
						i = 0;
						j=13;
					}
					Vector3 posA = transform.localPosition;          // Vectores iniciales
                    Vector3 posB = transform.position;
					Vector3 posC = transform.position;

              
                    foreach (Transform item in padre.GetChild(i+1)) {
						if (item.transform.GetComponent<Image> ().sprite == encendido) {	// si esta encendido la siguente categoria 
							posB= item.transform.localPosition;
						}
					}
						foreach (Transform item2 in padre.GetChild(j-1)) {
							if (item2.transform.GetComponent<Image> ().sprite == encendido) { // si esta encendido la anterior Categoria
							posC= item2.transform.localPosition;
						}
				}
					if (posB != transform.position) {		// si la posicion cambio mover las lineas a nueva posicion 
						CreateLine (union[0], posA, posB);
					}
					if (posC != transform.position) {
						CreateLine (union[1], posA, posC); // cambio de categoria anterior 
					}
					break; // salir del ciclo si no encontro coincidencias
				}}
		}
		Resultados (padre);
	}
	public void CreateLine(RectTransform loRectTransform, Vector2 loPosA, Vector2 loPosB) // Movimiento de un Rect , hacia la posicion a o B
	{
		float lnDistance = Vector3.Distance(loPosA, loPosB);
		Vector3 loMiddle = Vector3.Lerp(loPosA, loPosB, 0.5f);

		Vector3 loDirection = loPosB - loPosA;
		loDirection.Normalize();

		Quaternion loRotation = Quaternion.LookRotation(loDirection, Vector3.up);
		loRotation = Quaternion.Inverse(loRotation);
		Vector3 loNewRotation = loRotation.eulerAngles;
		loNewRotation.z = -loNewRotation.z;
		loNewRotation.x = 0.0f;
		loNewRotation.y = 0.0f;
		loRectTransform.rotation = Quaternion.Euler(loNewRotation);
		loRectTransform.sizeDelta = new Vector2(Math.Abs(lnDistance), 5.0f);
		loRectTransform.localPosition = loMiddle;

	}
	public void CeroRepetidos(){
		foreach (RectTransform item in transform.parent) {				// Prender y apagar solo un Boton de cada categoria
			item.gameObject.GetComponent<Image> ().sprite = Apagado;
			if (gameObject.name==item.name) {
				item.gameObject.GetComponent<Image> ().sprite = encendido;
			}
		}
	}
	public void Resultados(Transform result){
        
		Estrella.Clear ();
        foreach (var item in Estrella)
        {
            Debug.Log(item);
        }
		foreach (Transform item in result) {
			foreach (Transform item2 in item) {
				if (item2.GetComponent<Image> ().sprite == encendido) {
					Estrella.Add (item2.gameObject);
				}
			}
		}
		for (int i = 0; i < Estrella.Count; i++) {
			for (int j = 0; j < Estrella.Count; j++) {
				int contador = Int32.Parse (Estrella [i].name);
				int extra = Int32.Parse (Estrella [j].name);
				if (contador > extra) {							// Ordenar de Mayor a Menor
					GameObject temp = Estrella[i]; 
					GameObject temp2 =Estrella [j];
					Estrella [i] = temp2;
					Estrella [j]= temp;
				}
			}
		}

        Resultado.GetComponent<Text> ().text = "";
		for (int i = 0; i < Estrella.Count; i++) {
			if (i >= 3) {										// asignar solo los prtimeros 3
				break;
			}
            Resultado.GetComponent<Text>().text+=Estrella[i].transform.parent.name+" (" +Estrella [i].name+")"+" * "; // plasmar resultados en cadena de Texto


		}


	}
		



	public void sensaciones (){             // recopilar informacion de Sensaciones
                                            // verificar que solo exixta uno a la vez
        InterSensacion();
        controlsensacion();
	}

   public  void controlsensacion()
    {
        RSensacion.text = "";
        foreach (Transform item in gameObject.transform.parent)
        {
            if (item.GetComponent<Image>().sprite == encendido)
            {
                RSensacion.text += item.name +"*";
            }
        }
       
    }
    public void cargar(GameObject item)
    {
        item.GetComponent<Image>().sprite = encendido;
    }
    void InterSensacion()
    {
        if (transform.GetComponent<Image>().sprite == encendido)
        {
            transform.GetComponent<Image>().sprite = Apagado;              
        }
        else
        {
            transform.GetComponent<Image>().sprite = encendido;
        }
    }

	public void Cero(){
		var padre = transform.parent.transform.parent;  // Componenetes Este Metodo Solo se encuentra en el Primer Boton de Objeto Tierra
		foreach (Transform item in padre) {				
			foreach ( Transform item2 in item) {
				item2.GetComponent<Image> ().sprite = Apagado;		// Apagamos todos los Botones con la imagen Apagado
			}
			foreach (GameObject item3 in Lineas) {
				item3.transform.position = new Vector3 (-1000, 0, 0);	// Posicionamos todas las lineas a 0 para que no sean Vistas
			}
		}
		Resultados (padre);											// Reactivamos los resultados con todo en 0 
	}
}
