﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EscribirOpinion : MonoBehaviour {
	public GameObject AreaTexto;
	public InputField comentario;
	public GameObject Mensaje;
	public GameObject Ficha;
	public GameObject FichaProducto;
    private GameObject Perfil;

	// Use this for initialization
	public void desplegar(){		                        // mover abajo 
        Ficha.GetComponent<ContetZise>().EcenderContent();
        if (AreaTexto.activeSelf) {			            	// si el Mapa de Degustacion esta Activo
			AreaTexto.SetActive (false);	            	// desactivar 
		} else {								        	// cuando no este activo 
			AreaTexto.SetActive (true);			        	// Activar Mapa degustacion
		}
        Ficha.GetComponent<ContetZise>().ApagarContent();
    }
	public void fichaCompleta(){ 			            	// Medodo para most rar la Ficha completa de cada producto
		if (AreaTexto.activeSelf) {				
			AreaTexto.SetActive (false);	            	//Activar Mapa degustacion   >>! el metodo "Desplegar" se repite con una expecion al ser dos objetos 
		} else {
			AreaTexto.SetActive (true);			           	// De lo contrartio realizar inversa , mover el objeto hacia la posicion principal e ocultar Mapa de Degustacion
		}
	}
    private void Start()
    {
        //Perfil = GameObject.FindGameObjectWithTag("Monito");
    }


	public void Fijar(GameObject obj){                       // Mueve la ficha en poscion que el panel escribir opinion se vicible en el contect y el rango del scroll
		obj.gameObject.GetComponent<RectTransform>().localPosition = new Vector2(obj.transform.localPosition.x,(-AreaTexto.GetComponent<RectTransform>().localPosition.y));
		desplegar ();                                           // Desplegar panel para escribir
	}

	public void Enviar()                // Enviar comentario / opinion
    {
        if (PlayerPrefs.HasKey("UserID"))
        {
            CulturaMezcalJsons.Comentarios com = new CulturaMezcalJsons.Comentarios();
            com.UserID = PlayerPrefs.GetString("UserID");
            com.ProductID = Ficha.transform.GetComponent<FichaMezcales>().us.ProductID;
            com.Comentario = comentario.text;

            string json = JsonUtility.ToJson(com);

            WSClient.instance.StartWebService("user/comentario", json, Enviado, (r) => MessageBox.instance.ShowMessage("Hubo un problema de conexión."));
        }
        else{
            //Perfil.transform.GetComponent<LoginPerfil>().Iniciar();         // si no cuenta con userI d es por que debe inciar seccion primero
            GameObject.FindObjectOfType<LoginPerfil>().Iniciar();
        }
	}
	// Metodo para Verificar si se Mando el comentario 
	void Enviado(WSResponse response){
        string result = response.responseTxt;
		CulturaMezcalJsons.Exito ex = new CulturaMezcalJsons.Exito();
		ex=JsonUtility.FromJson<CulturaMezcalJsons.Exito>(result);
		if (ex.Success) {
            comentario.text = "";
            FichaProducto.GetComponent<FichaProducto>().vaciar();
            Ficha.transform.GetComponent<FichaMezcales>().Recargar();
        }
        else {
            MessageBox.instance.ShowMessage("Problemas para conectar con el Servidor, Intente mas Tarde ");
		}
	
	}

	// Proceso de Recarga de Comentarios para mostrar el comentario nuevo recien Enviado




}
