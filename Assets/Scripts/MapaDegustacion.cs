﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class MapaDegustacion : MonoBehaviour {

	public GameObject Ficha;
	public GameObject Tierra;
	public GameObject Fermentos;
	public GameObject Madera;
	public GameObject Humo;
	public GameObject Espesies;
	public GameObject Caramelo;
	public GameObject Frutal;
	public GameObject Alcohol;
	public GameObject Citrico;
	public GameObject Floral;
	public GameObject Hierba;
	public GameObject Mineral;
	public GameObject Humedad;
	public GameObject[] sensaciones;
	public Sprite Encendido;
	public GameObject Perfil;
	CulturaMezcalJsons.EnviarMapa Em = new CulturaMezcalJsons.EnviarMapa ();
	CulturaMezcalJsons.UsuarioID us = new CulturaMezcalJsons.UsuarioID ();


	public void Inicio(string ProductId){
		us.UserID = PlayerPrefs.GetString("UserID");
        us.ProductID = ProductId;
		string json = JsonUtility.ToJson(us);
		WSClient.instance.StartWebService("user/degustacion", json, Cargar, (r) => MessageBox.instance.ShowMessage("Hubo un problema de conexión."));
	}
	public void Cargar(WSResponse response) {
        string result = response.responseTxt;

		if(result!=	"No Content"){
            Debug.Log(result);
		CulturaMezcalJsons.MapaPersonal md = new CulturaMezcalJsons.MapaPersonal ();
		md =JsonUtility.FromJson<CulturaMezcalJsons.MapaPersonal>(result);
		Rellenar (Alcohol, md.MapaDegustacion.Alcohol);
		Rellenar (Tierra, md.MapaDegustacion.Tierra);
		Rellenar (Fermentos, md.MapaDegustacion.Fermentos);
		Rellenar (Madera, md.MapaDegustacion.Madera);
		Rellenar (Humo, md.MapaDegustacion.Humo);
		Rellenar (Espesies, md.MapaDegustacion.Especias);
		Rellenar (Caramelo, md.MapaDegustacion.Caramelo);
		Rellenar (Frutal, md.MapaDegustacion.Frutal);
		Rellenar (Citrico, md.MapaDegustacion.Citrico);
		Rellenar (Floral, md.MapaDegustacion.Floral);
		Rellenar (Hierba, md.MapaDegustacion.Hierba);
		Rellenar (Mineral, md.MapaDegustacion.Mineral);
		Rellenar (Humedad, md.MapaDegustacion.Humedad);
		sensacion(sensaciones [0],md.MapaSensacion.Aspero);
		sensacion(sensaciones [1],md.MapaSensacion.Calor);
		sensacion(sensaciones [2],md.MapaSensacion.Fresco);
		sensacion(sensaciones [3],md.MapaSensacion.Picante);
		sensacion(sensaciones [4],md.MapaSensacion.Untuoso);
		}
	}
	void Rellenar(GameObject sabor , float valor){
		if (valor > 0) {
			foreach (Transform item in sabor.transform) {
				if (item.name == valor.ToString ()) {
					item.GetComponent<MapaAromas> ().llamar ();
				}
			}
		}
	}
    private void Start()
    {
        Perfil = GameObject.FindGameObjectWithTag("Monito");
    }
    int valor=0;
	int valor2=0;
	public void Enviar()
    {
		if (PlayerPrefs.HasKey("UserID")) {
			Em.UserID = us.UserID;
			Em.ProductID = us.ProductID;
			Em.Mineral =revisar (Mineral);
			Em.Caramelo = revisar (Caramelo);
			Em.Humo = revisar (Humo);
			Em.Humedad =revisar (Humedad);
			Em.Fermentos = revisar (Fermentos);
			Em.Alcohol =revisar (Alcohol);
			Em.Citrico =	revisar (Citrico);
			Em.Especias = revisar (Espesies);
			Em.Floral = revisar (Floral);
			Em.Frutal =	revisar (Frutal);
			Em.Hierba =	revisar (Hierba);
			Em.Tierra =	revisar (Tierra);
			Em.Madera = revisar (Madera);
			Em.Aspero = VerSensacion (sensaciones [0]);
			Em.Calor = VerSensacion (sensaciones [1]);
			Em.Fresco = VerSensacion (sensaciones [2]);
			Em.Picante = VerSensacion (sensaciones [3]);
			Em.Untuoso = VerSensacion (sensaciones [4]);
			string json = JsonUtility.ToJson (Em);	
	


			WSClient.instance.StartWebService ("user/mapa", json, Envidado, (r) => MessageBox.instance.ShowMessage("Hubo un problema de conexión."));  
		}else {

            GameObject.FindObjectOfType<LoginPerfil>().Iniciar ();
	
		}
	}
	void Envidado(WSResponse response){
        string result = response.responseTxt;
		CulturaMezcalJsons.Exito ex = new CulturaMezcalJsons.Exito();
		ex=JsonUtility.FromJson<CulturaMezcalJsons.Exito>(result);
		if (ex.Success) {

            Ficha.GetComponent<FichaMezcales>().Recargar();

        }
	}


	public int revisar(GameObject sabor){
		
		foreach (Transform item in sabor.transform) {
			if (item.GetComponent<Image> ().sprite == Encendido) {
				valor = Int32.Parse (item.name);
                break;
            }
            else{
                valor = 0;
            }
		}
        
		return valor;
	}
	void sensacion(GameObject sensacion , int valor){
		
		if (valor > 0) {
            sensacion.GetComponent<MapaAromas>().cargar(sensacion);
            sensacion.GetComponent<MapaAromas>().controlsensacion();
		}

			
	}
	public int VerSensacion(GameObject sensacion){
		valor2 = 0;
		if(sensacion.transform.GetComponent<Image>().sprite == Encendido) {
		valor2 = 1;
		}
		return valor2;
	
	}

	// Update is called once per frame
	void Update () {
		
	}
}
