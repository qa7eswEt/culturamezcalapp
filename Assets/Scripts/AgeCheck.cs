﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;
#if UNITY_IOS
using DeadMosquito.IosGoodies;
#endif
public class AgeCheck : MonoBehaviour
{
    public GameObject warningPanel;
    public Text dateText;

    private static bool sameSession = false;
    DateTime ChosenDate;
    long chosenDate;
    bool noPreguntar = false;

	// Use this for initialization
	void Start ()
    {
	    if(PlayerPrefs.HasKey("MayorDeEdad") || Application.isEditor || sameSession)
        {
            gameObject.SetActive(false);
        }
        else
        {
            long epochTicks = new DateTime(1970, 1, 1).Ticks;
            chosenDate = ((DateTime.UtcNow.Ticks - epochTicks) / TimeSpan.TicksPerSecond);
            ChosenDate = DateTime.Today;
        }
	}

    public void CheckDate()
    {
        float width = Screen.width / 2;
        float height = Screen.width / 14;
        Rect drawRect = new Rect((Screen.width - width) / 2, height, width, height);
#if UNITY_ANDROID
    NativePicker.Instance.ShowDatePicker(toScreenRect(drawRect), GotDate, null);
#elif UNITY_IOS
        var today = DateTime.Today;
        IGDateTimePicker.ShowDatePicker(today.Year, today.Month, today.Day,
            SetDateOnUI,
            CancelPickDate);
#endif
    }

    public void CancelPickDate()
    {
        ChosenDate = new DateTime();
    }

    public void SetDateOnUI(DateTime date)
    {
        ChosenDate = date;
        dateText.text = date.ToString("dd/MM/yyyy");
    }

    public void GotDate(long date)
    {
        chosenDate = date;
        dateText.text = NativePicker.ConvertToDateTime(date).ToShortDateString();
    }

    public static DateTime UnixTimeStampToDateTime(double unixTimeStamp)
    {
        // Unix timestamp is seconds past epoch
        System.DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
        dtDateTime = dtDateTime.AddSeconds(unixTimeStamp).ToLocalTime();
        return dtDateTime;
    }

    public void Enter()
    {
        bool isAdult = false;

#if UNITY_ANDROID
        long epochTicks = new DateTime(1970, 1, 1).Ticks;
        long todayMinusEighteen = ((DateTime.UtcNow.AddYears(-21).Ticks - epochTicks) / TimeSpan.TicksPerSecond);
        print("Date: " + chosenDate);
        isAdult = chosenDate  > todayMinusEighteen;
#elif UNITY_IOS
        if(DateTime.Compare(ChosenDate, new DateTime()) != 0)
        {
            var today = DateTime.Today;
            var aux = today - ChosenDate;
            int year = DateTime.MinValue.Add(aux).Year - 1;
            isAdult =  year < 21;
        }
        else
        {
        isAdult = false;
        }


#endif



        if (isAdult)
        {
            warningPanel.SetActive(true);
            print("no eres mayor de edad");
        }
        else
        {
            print("eres mayor de edad");
            sameSession = true;
            if (noPreguntar)
            {
                PlayerPrefs.SetInt("MayorDeEdad", 1);
            }
            gameObject.SetActive(false);
        }
    }

    public void SetNoPreguntar(bool preguntar)
    {
        noPreguntar = preguntar;
    }

    Rect toScreenRect(Rect rect)
    {
        Vector2 lt = new Vector2(rect.x, rect.y);
        Vector2 br = lt + new Vector2(rect.width, rect.height);

        lt = GUIUtility.GUIToScreenPoint(lt);
        br = GUIUtility.GUIToScreenPoint(br);

        return new Rect(lt.x, lt.y, br.x - lt.x, br.y - lt.y);
    }

    // Update is called once per frame
    void Update () {
		
	}
}
