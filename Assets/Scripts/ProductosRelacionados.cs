﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class ProductosRelacionados : MonoBehaviour {
	public GameObject[] Producto;
	public Sprite Magueylleno;
	public Sprite MagueyVacio;
	public GameObject Ficha;
	// Use this for initialization
	void Start () {

	}

	
	// Update is called once per frame
	void Update () {
		
	}
	// Productos Relacionados
	public void Relacionados(string result){
		CulturaMezcalJsons.FichaMezcal fm = new CulturaMezcalJsons.FichaMezcal ();
		fm=JsonUtility.FromJson<CulturaMezcalJsons.FichaMezcal>(result);

			llenar (fm, Producto);

	}

	// Llenar los 3 Objetos con los primero 3
	void llenar (CulturaMezcalJsons.FichaMezcal ficha, GameObject[] Pr)
	{
        for (int i = 0; i < ficha.ProductosRelacionados.Length; i++)
        {

            Pr[i].transform.GetChild(1).GetComponent<Text>().text = ficha.ProductosRelacionados[i].Nombre;

            Pr[i].transform.GetChild(2).GetComponent<Text>().text = ficha.ProductosRelacionados[i].Agave;
            int c = 0;
            foreach (Transform item2 in Pr[i].transform.GetChild(3))
            {
                c++;
                int j = ficha.ProductosRelacionados[i].Calificacion;
                if (c <= j)
                {
                    item2.GetComponent<Image>().sprite = Magueylleno;
                }
                else
                {
                    item2.GetComponent<Image>().sprite = MagueyVacio;
                }
            }

            for (int j = 0; j < ficha.Etiquetas.Length; j++)
            {
                if (ficha.Etiquetas[j].tag == "estado")
                {
                    Pr[i].transform.GetChild(4).GetComponent<Text>().text = ficha.Etiquetas[j].val;
                    Pr[i].transform.GetChild(4).name = ficha.Etiquetas[j].tag;
                    break;
                }
            }
            
            Pr[i].name = ficha.ProductosRelacionados[i].ProductID;
            Debug.Log(i + " Nombre " + ficha.ProductosRelacionados[i].Nombre + " agave: " + ficha.ProductosRelacionados[i].Agave + " url: " + ficha.ProductosRelacionados[i].Foto);
            //StartCoroutine(llenarFoto(Pr[i].transform.GetChild(0).gameObject, ficha.ProductosRelacionados[i].Foto));
            Pr[i].transform.GetChild(0).GetComponent<WebImage>().LoadImage(ficha.ProductosRelacionados[i].Foto);
        }
        /*
         * if (ficha.ProductosRelacionados.Length > 2)
        {
            
        }else 
         */
        if (ficha.ProductosRelacionados.Length == 2)
        {
            Pr[2].gameObject.SetActive(false);
        }
        else if (ficha.ProductosRelacionados.Length == 1)
        {
            Pr[2].gameObject.SetActive(false);
            Pr[1].gameObject.SetActive(false);
        }
        else if (ficha.ProductosRelacionados.Length < 1)
        {
            Pr[2].gameObject.SetActive(false);
            Pr[1].gameObject.SetActive(false);
            Pr[0].gameObject.SetActive(false);
        }
    }

}
