﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class FloatingScroll : MonoBehaviour
{
    [Tooltip("Determina la distancia maxima en y para saber si se considera dentro de rango.")]
    public float maxDistance = 10.0f;

    [Tooltip("Lugar desde donde determinamos la distancia.")]
    public Transform from;

    [Tooltip("Curva que determina el alpha dependiendo de la distancia de nuestro elemnto.")]
    public AnimationCurve alphaCurve = AnimationCurve.EaseInOut(0.0f, 0.0f, 1.0f, 1.0f);

    [Tooltip("GameObjects que se usaran para determinar quien es el mas cercano a nuestro elemento.")]
    private CanvasGroup[] goElements;

    /// <summary>
    /// 
    /// </summary>
    private void Awake()
    {
        goElements = GetComponentsInChildren<CanvasGroup>();
    }

    /// <summary>
    /// 
    /// </summary>
    public void Update()
    {
        //<< Determinamos cual de los elementos es el mas cercano.
        float lnAlpha = 0.0f;
        float lnDistance = 0.0f;

        foreach (CanvasGroup loCanvasGroupElement in goElements)
        {
            //<< Obtenemos la distancia.
            lnDistance = Vector3.Distance(loCanvasGroupElement.transform.position, from.position);
            lnAlpha = 1.0f - (lnDistance / maxDistance);

            //<< Asignamos el nuevo alfa.
            loCanvasGroupElement.alpha = alphaCurve.Evaluate(lnAlpha);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    private void OnDrawGizmos()
    {
        //<< Dibujamos las lineas que determinaran el rango de entrada del elemento.
        Gizmos.color = Color.cyan;
        Gizmos.DrawWireSphere(from.position, maxDistance);
    }
}
