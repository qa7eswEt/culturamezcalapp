﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class UnityEventBool : UnityEvent<bool> { }

public class VisibilityCheck : MonoBehaviour
{
    public UnityEventBool onVisibility;
    public UnityEventBool onVisibilityInvert;
    public UnityEvent onEnable;
    public UnityEvent onDisable;

    private void OnDisable()
    {
        if (onVisibility != null) onVisibility.Invoke(false);
        if (onVisibilityInvert != null) onVisibilityInvert.Invoke(true);
        if (onDisable != null) onDisable.Invoke();
    }

    private void OnEnable()
    {
        if (onEnable != null) onEnable.Invoke();
        if (onVisibility != null) onVisibility.Invoke(true);
        if (onVisibilityInvert != null) onVisibilityInvert.Invoke(false);
    }
}
