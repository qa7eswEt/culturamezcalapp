﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathAnimation : MonoBehaviour
{
    [Tooltip("Son los rectTransforms que crearán nuestra linea.")]
    public RectTransform[] elements;

    [Tooltip("Densidad de puntos por puntos.")]
    public int pointDensity = 20;

    [Tooltip("Indica los puntos deben iniciar en zero para despues agregar la posicion.")]
    public bool pointsStartOnZero = true;

    /// <summary>
    /// Curva de bezier.
    /// </summary>
    private Bezier goBezier = new Bezier();

    // Use this for initialization
    void Awake()
    {
        Bezier.CreateCurve(elements, pointDensity, goBezier, pointsStartOnZero);
    }

    private void Start()
    {

    }

    /// <summary>
    /// UpdateAnimation
    /// </summary>
    /// <param name="lnDelta"></param>
    public void UpdateAnimation(float lnDelta)
    {
        //<< Sacamos la posicion actual con base en la distancia.
        float lnNewDistance = goBezier.gnTotalDistance * lnDelta;
        Vector2 loPosition = Vector2.zero;
        Bezier.GetCurrentPosition(ref loPosition, goBezier, lnNewDistance);
        transform.position = Vector2.Lerp(transform.position, loPosition, lnDelta);
    }

    /// <summary>
    /// UpdateAnimation
    /// </summary>
    /// <param name="lnDelta"></param>
    public void UpdateAnimation(float lnDelta, ref Vector2 loPosition)
    {
        //<< Sacamos la posicion actual con base en la distancia.
        float lnNewDistance = goBezier.gnTotalDistance * lnDelta;
        Bezier.GetCurrentPosition(ref loPosition, goBezier, lnNewDistance);
    }

#if UNITY_EDITOR
    /// <summary>
    /// Para dibujar los gizmos
    /// </summary>
    void OnDrawGizmos()
    {
        if (!enabled) return;
        RectTransform loRectTransformA = null;
        RectTransform loRectTransformB = null;
        Gizmos.color = Color.red;
        int lnCount = elements.Length - 1;
        for (int i = 0; i < lnCount; i++)
        {
            loRectTransformA = elements[i];
            loRectTransformB = elements[i + 1];
            if (loRectTransformA == null) continue;
            if (loRectTransformB == null) continue;
            Gizmos.DrawLine(loRectTransformA.position, loRectTransformB.position);
        }

        //<< Chcamos que estén completos los elementos para hacer la curva.
        lnCount = elements.Length;
        for (int i = 0; i < lnCount; i++) { if (elements[i] == null) return; }

        Vector2[] loPoints = Bezier.GetStaticPoints(elements, pointDensity, false);
        Bezier.OnDrawGizmos(loPoints, Color.yellow);
        Bezier.OnDrawGizmos(goBezier.goPoints, Color.green);
    }
#endif
}
