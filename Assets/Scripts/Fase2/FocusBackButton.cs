﻿using UnityEngine;
using UnityEngine.Events;

public class FocusBackButton : MonoBehaviour
{
    [SerializeField] FocusZoom _focusZoom;
    [SerializeField] UnityEvent _onRoot;
    [SerializeField] UnityEvent _onNonRoot;

    public void ZoomCheck() {
        if (_focusZoom.Deep == 0) _onRoot?.Invoke();
        else _onNonRoot.Invoke();
    }

}
