﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Bezier
{
    /// <summary>
    /// Puntos de bezier.
    /// </summary>
    public Vector2[] goPoints = new Vector2[0];

    /// <summary>
    /// Distancias entre puntos.
    /// </summary>
    public float[] goDistances = new float[0];

    /// <summary>
    /// Sumatoria de distancias.
    /// </summary>
    private float[] goCheckPoints = new float[0];

    /// <summary>
    /// Distancia total de nuestra curva.
    /// </summary>
    public float gnTotalDistance = 0.0f;

    /// <summary>
    /// Distancias estaticas.
    /// </summary>
    private static Bezier goStaticBezier = new Bezier();

    /// <summary>
    /// Regresa los puntos estaticos de esta clase.
    /// </summary>
    /// <returns></returns>
    public static Vector2[] GetStaticPoints(RectTransform[] loControlPoints, int lnOutputSegmentCount, bool lbPointsStartOnZero)
    {
        //<< Obtenemos los puntos de la curva.
        Bezier.GetBezierApproximation(loControlPoints, lnOutputSegmentCount);

        //<< Establecemos los puntos con inicio en el origen.
        if (lbPointsStartOnZero) SetStartZero(ref goStaticBezier.goPoints);

        return goStaticBezier.goPoints;
    }

    /// <summary>
    /// Establece que los puntos inicien en zero.
    /// </summary>
    public static void SetStartZero(ref Vector2[] loPoints)
    {
        if (loPoints.Length == 0) return;//<< Si no hay puntos no hay manera.

        //<< Sacamos el primer punto para restar la posicion a todos.
        Vector2 loPoint = loPoints[0];

        int lnCount = loPoints.Length;
        for (int i = 0; i < lnCount; i++)
        {
            loPoints[i].x -= loPoint.x;
            loPoints[i].y -= loPoint.y;
        }
    }

    /// <summary>
    /// Crea los puntos en las variables dadas.
    /// </summary>
    public static void CreateCurve(RectTransform[] loControlPoints, int lnPointDensity, Bezier loBezier, bool lbPointsStartOnZero)
    {
        //<< Obtenemos los puntos de bezier.
        GetBezierApproximation(loControlPoints, lnPointDensity, ref loBezier.goPoints);

        //<< Sacamos la distancia entre los puntos.
        loBezier.gnTotalDistance = GetDistances(ref loBezier.goPoints, ref loBezier.goDistances);

        //<< Sacamos las distancias de nuestros elementos incremental.
        int lnCount = loBezier.goPoints.Length;
        loBezier.goCheckPoints = new float[lnCount];
        for (int i = 0; i < lnCount; i++)
        {
            for (int j = 0; j < i; j++) loBezier.goCheckPoints[i] += loBezier.goDistances[j];
        }

        //<< Establecemos los puntos con inicio en el origen.
        if (lbPointsStartOnZero) SetStartZero(ref loBezier.goPoints);
    }

    /// <summary>
    /// Crea los puntos en las variables dadas.
    /// </summary>
    public static void CreateCurve(RectTransform[] loControlPoints, int lnPointDensity, ref Vector2[] loPoints, ref float[] loDistances, ref float lnTotalDistance, bool lbStartPointsZero)
    {
        //<< Obtenemos los puntos de bezier.
        GetBezierApproximation(loControlPoints, lnPointDensity, ref loPoints);

        //<< Sacamos la distancia entre los puntos.
        lnTotalDistance = GetDistances(ref loPoints, ref loDistances);

        //<< Establecemos los puntos con inicio en el origen.
        if (lbStartPointsZero) SetStartZero(ref loPoints);
    }

    /// <summary>
    /// Crea los elementos en curva basados en los puntos de control.
    /// </summary>
    public static void CreateElements<T>(Transform loParent, RectTransform[] loControlPoints, int lnPointDensity, float lnDistanceBetweenDots, IList<T> loElements, T loElement, ref Vector2[] loPoints) where T : Component
    {
        if (loControlPoints == null || loControlPoints.Length == 0) return;

        //<< Obtenemos los puntos de bezier.
        GetBezierApproximation(loControlPoints, lnPointDensity, ref loPoints);

        //<< Sacamos la distancia entre los puntos.
        float lnTotalDistance = GetDistances(ref loPoints, ref goStaticBezier.goDistances);

        //<< Obtenemos la cantidad de puntos que debemos crear con base en la distancia entre los puntos y la total.
        int lnDots = 0;
        if (lnTotalDistance > lnDistanceBetweenDots) lnDots = Mathf.FloorToInt(lnTotalDistance / lnDistanceBetweenDots);

        //<< Determinamos cada cuanto aparece un elemento.
        float lnOffset = 0.0f;
        if (lnDots > 0) lnOffset = lnTotalDistance / lnDots;

        //<< Sacamos las distancias de nuestros elementos.
        int lnCount = loPoints.Length;
        float[] loCheckPoints = new float[lnCount];
        for (int i = 0; i < lnCount; i++)
        {
            for (int j = 0; j < i; j++) loCheckPoints[i] += goStaticBezier.goDistances[j];
        }

        //<< Ya que tenemos la distancia entre los puntos determinamos la posicion de los elementos, creamos nuestros prefabs.
        Vector2 loPosition = new Vector3();
        Quaternion loRotation = new Quaternion();

        //<< Creamos nuevos elementos.
        for (int i = 0; i < lnDots; i++)
        {
            float lnCurrentDistance = i * lnOffset;
            Bezier.GetCurrentPosition(ref loPosition, loPoints, goStaticBezier.goDistances, loCheckPoints, lnCurrentDistance);

            //<< Si no hay suficientes creamos.
            if (i >= loElements.Count) loElements.Add(GameObject.Instantiate<T>(loElement, loParent, false));

            //<< Asignamos elemento.
            T loRectTransform = loElements[i];
            loRectTransform.transform.position = loPosition;
            loRectTransform.transform.localRotation = loRotation;
            loRectTransform.name = "Dot (" + i + ")";
        }
    }

    /// <summary>
    /// Asigna las rotaciones y posiciones de los puntos.
    /// </summary>
    /// <param name="loPosition"></param>
    /// <param name="loPoints"></param>
    /// <param name="lnDistance"></param>
    /// <param name="lbIsForward"></param>
    public static void GetCurrentPosition(ref Vector2 loPosition, Vector2[] loPoints, float[] loDistances, float[] loCheckPoints, float lnDistance)
    {
        int lnCount = 0;
        float lnDelta = 0.0f;
        lnCount = loPoints.Length - 1;
        Vector3 loEuler = Vector3.zero;
        Vector3 loRelativePos = Vector3.zero;
        for (int i = 0; i < lnCount; i++)
        {
            if (lnDistance >= loCheckPoints[i + 1]) continue;//<< Si nuestra distancia aún es mayor 

            //<< Posicion.
            lnDelta = (lnDistance - loCheckPoints[i]) / loDistances[i];
            loPosition = Vector2.Lerp(loPoints[i], loPoints[i + 1], lnDelta);

            //<< Direccion.
            loRelativePos = loPoints[i + 1] - loPosition;
            loRelativePos.z = loRelativePos.y;
            loRelativePos.y = 0.0f;

            return;
        }

        loPosition = loPoints[lnCount];
    }

    /// <summary>
    /// Asigna las rotaciones y posiciones de los puntos.
    /// </summary>
    /// <param name="loPosition"></param>
    /// <param name="loPoints"></param>
    /// <param name="lnDistance"></param>
    /// <param name="lbIsForward"></param>
    public static void GetCurrentPosition(ref Vector2 loPosition, Bezier loBezier, float lnDistance)
    {
        GetCurrentPosition(ref loPosition, loBezier.goPoints, loBezier.goDistances, loBezier.goCheckPoints, lnDistance);
    }

    /// <summary>
    /// Obtiene la distancia total y la distancia entre cada punto.
    /// </summary>
    /// <param name="loPoints"></param>
    /// <param name="loDistances"></param>
    /// <returns></returns>
    private static float GetDistances(ref Vector2[] loPoints, ref float[] loDistances)
    {
        if (loPoints == null) return 0.0f;

        //<< Sacamos la distancia total y determinamos la distancia entre prefab y prefab.
        float lnTotalDistance = 0.0f;
        int lnCount = loPoints.Length - 1;
        if (loDistances == null || loDistances.Length < lnCount) loDistances = new float[lnCount];
        float lnDistance = 0.0f;
        for (int i = 0; i < lnCount; i++)
        {
            lnDistance = Vector3.Distance(loPoints[i], loPoints[i + 1]);
            loDistances[i] = lnDistance;
            lnTotalDistance += lnDistance;
        }
        return lnTotalDistance;
    }

    /// <summary>
    /// Obtiene la curva de bezier pasando las posiciones a los puntos, esto con base en los puntos de control.
    /// </summary>
    /// <param name="loControlPoints"></param>
    /// <param name="lnOutputSegmentCount"></param>
    /// <param name="loPoints"></param>
    public static void GetBezierApproximation(RectTransform[] loControlPoints, int lnOutputSegmentCount, ref Vector2[] loPoints)
    {
        if (loControlPoints == null || loControlPoints.Length == 0) return;
        if (loPoints == null || loPoints.Length != lnOutputSegmentCount) loPoints = new Vector2[lnOutputSegmentCount];

        float lnDelta = 0.0f;
        float lnStep = 1.0f / (lnOutputSegmentCount - 1.0f);
        for (int i = 0; i < lnOutputSegmentCount; i++)
        {
            GetBezierPoint(lnDelta, loControlPoints, 0, loControlPoints.Length, ref loPoints[i]);
            lnDelta += lnStep;
        }
    }

    /// <summary>
    /// Obtiene la curva de bezier pasando las posiciones a los puntos, esto con base en los puntos de control.
    /// </summary>
    /// <param name="loControlPoints"></param>
    /// <param name="lnOutputSegmentCount"></param>
    /// <param name="loPoints"></param>
    public static void GetBezierApproximation(RectTransform[] loControlPoints, int lnOutputSegmentCount)
    {
        GetBezierApproximation(loControlPoints, lnOutputSegmentCount, ref goStaticBezier.goPoints);
    }

    /// <summary>
    /// Saca la posicion del punto con base a los puntos de control.
    /// </summary>
    /// <param name="lnDelta"></param>
    /// <param name="loControlPoints"></param>
    /// <param name="lnIndex"></param>
    /// <param name="lnCount"></param>
    /// <param name="loPoint">Punto que tendrá la nueva posicion con base en los puntos de control.</param>
    private static void GetBezierPoint(float lnDelta, RectTransform[] loControlPoints, int lnIndex, int lnCount, ref Vector2 loPoint)
    {
        if (lnCount == 1) { loPoint = loControlPoints[lnIndex].position; return; }
        lnCount -= 1;
        Vector2 loP0 = Vector2.zero;
        Vector2 loP1 = Vector2.zero;
        GetBezierPoint(lnDelta, loControlPoints, lnIndex, lnCount, ref loP0);
        lnIndex++;
        GetBezierPoint(lnDelta, loControlPoints, lnIndex, lnCount, ref loP1);

        loPoint.x = (1.0f - lnDelta) * loP0.x + lnDelta * loP1.x;
        loPoint.y = (1.0f - lnDelta) * loP0.y + lnDelta * loP1.y;
    }

    /// <summary>
    /// Dibuja los puntos usando gizmos, solo debe llamarse en onDrawGizmo.
    /// </summary>
    /// <param name="loPoints"></param>
    /// <param name="loColor"></param>
    public static void OnDrawGizmos(Vector2[] loPoints, Color loColor)
    {
        Gizmos.color = loColor;
        int lnCount = loPoints.Length - 1;
        Vector3 loA = Vector3.zero;
        Vector3 loB = Vector3.zero;
        for (int i = 0; i < lnCount; i++)
        {
            loA = loPoints[i];
            loB = loPoints[i + 1];
            Gizmos.DrawLine(loA, loB);
        }
    }
}
