﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(CanvasGroup))]
public class FocusZoom : SimpleAnimation
{
    /// <summary>
    /// Elemento que controlará el regresar
    /// </summary>
    private static FocusZoom goRoot = null;

    /// <summary>
    /// Escala que se aplicará para cuando deseemos la salida del elemento.
    /// </summary>
    private static float gnScaleOut = 120.0f;

    /// <summary>
    /// Escala que se aplicará para cuando deseemos la salida del elemento.
    /// </summary>
    private static Vector3 goScaleOut = new Vector3(gnScaleOut, gnScaleOut, 1.0f);

    [Tooltip("Indica si las curvas tomaran los valores de la plantilla.")]
    public bool useTemplate = true;

    [Tooltip("Curva para la animacion de entrada.")]
    public AnimationCurve curveEnter = AnimationCurve.EaseInOut(0.0f, 0.0f, 1.0f, 1.0f);

    [Tooltip("Curva para la animacion de salida.")]
    public AnimationCurve curveExit = AnimationCurve.EaseInOut(0.0f, 0.0f, 1.0f, 1.0f);

    [Tooltip("Curva para la animacion del alpha de salida.")]
    public AnimationCurve curveAlphaExit = AnimationCurve.EaseInOut(0.0f, 0.0f, 1.0f, 1.0f);

    [Tooltip("Posicion de default de nuestro RectTransform.")]
    public RectTransform rectTransform;

    [Tooltip("Scrollrect para detener el input y establecer posiciones.")]
    public ScrollRect scrollRect;

    [Tooltip("Nuestro canvas group para realizar el fadeout.")]
    public CanvasGroup canvasGroup;

    [Tooltip("Escala final a la que llega nuestro elemento.")]
    public Vector3 scaleEnd = Vector3.one;

    [Tooltip("Hijos que tiene que usar el alpha y escala tambien.")]
    public CanvasGroup[] alphaChilds;

    /// <summary>
    /// Padre del elemento.
    /// </summary>
    private FocusZoom goParent;

    /// <summary>
    /// Hijo del elemento.
    /// </summary>
    private FocusZoom goChild;

    /// <summary>
    /// Estado A de nuestro elemento.
    /// </summary>
    private AnimationState goAnimationStateToRunA = null;

    /// <summary>
    /// Estado B de nuestro elemento.
    /// </summary>
    private AnimationState goAnimationStateToRunB = null;

    /// <summary>
    /// Estado A de nuestro elemento.
    /// </summary>
    private AnimationState[] goAnimationStatesA = new AnimationState[2] { new AnimationState(), new AnimationState() };

    /// <summary>
    /// Estado B de nuestro elemento.
    /// </summary>
    private AnimationState[] goAnimationStatesB = new AnimationState[2] { new AnimationState(), new AnimationState() };

    /// <summary>
    /// Posicion donde se detectó el click.
    /// </summary>
    private Vector3 goClickPosition = Vector3.zero;

    /// <summary>
    /// Posicion a donde entrará el elemento.
    /// </summary>
    private Vector3 goEnterPosition = Vector3.zero;

    /// <summary>
    /// Indica la profundidad del zoom es decir cuantas veces se ha hecho la animacion sobre el elemento, maximo son 2, digase indice 0 y 1.
    /// </summary>
    private int gnDeep = 0;

    public int Deep => gnDeep;

    /// <summary>
    /// Clase que nos da el estado del elemento.
    /// </summary>
    private class AnimationState
    {
        /// <summary>
        /// Posicion del elemento. Nota: Si Esta animandose la entrada este valor tomará el del primer elemento RecTransform.
        /// </summary>
        public Vector3 goPosition = Vector3.zero;

        /// <summary>
        /// Escala del elemento. Nota: Si Esta animandose la entrada este valor tomará el valor de 1.0f.
        /// </summary>
        public Vector3 goScale = Vector3.one;

        /// <summary>
        /// Alfa de nuestro canvas group.
        /// </summary>
        public float gnAlpha = 1.0f;

        /// <summary>
        /// Curva de animacion.
        /// </summary>
        public AnimationCurve goCurve = null;
    }

    /// <summary>
    /// 
    /// </summary>
    protected override void Awake()
    {
        base.Awake();

        goEnterPosition = -(rectTransform.position - transform.position);
        onDelta.AddListener(UpdateAnimation);
        onA.AddListener(OnA);
        onB.AddListener(OnB);
    }

    /// <summary>
    /// 
    /// </summary>
    private void Start()
    {
        if (useTemplate) FocusZoomTemplate.SetTemplateValues(this);
    }

    /// <summary>
    /// Se llama cuando termina de entrar.
    /// </summary>
    private void OnA()
    {
        SetInteractableAndMovementType(true);

        //<< Si tenemos hijo sabemos que somos el padre del ultimo elemento en profundindad por ello quitamos al hijo y reducimos profundidad.
        if (goChild != null) { gnDeep--; goChild = null; return; }

        //<< Si no tenemos hijo sabemos que somos el ultimo elemento en profundindad por ello desactivamos y reiniciamos estados.
        gameObject.SetActive(false);
        goParent = null;
        gnDeep = 0;
    }

    /// <summary>
    /// Se llama cuando termina de salir.
    /// </summary>
    private void OnB()
    {
        SetInteractableAndMovementType(true);

        //<< Si no tenemos hijo y estamos animando en direccion de A a B sabemos que estamos entrando.
        if (goChild == null) return;

        //<< Desactivamos nuestro panel si es que salimos por que somos padre de otro elemento.
        gameObject.SetActive(false);
    }

    private void SetInteractableAndMovementType(bool lbActive)
    {
        //<< Activamos la interactibilidad por que ya terminamos.
        canvasGroup.interactable = lbActive;

        //<< Establecemos que el scrollrect en elastic de nuevo.
        if (lbActive) scrollRect.movementType = ScrollRect.MovementType.Elastic;
        else scrollRect.movementType = ScrollRect.MovementType.Unrestricted;
    }

    /// <summary>
    /// Establece los estados para la animacion para cuando haremos zoom in.
    /// </summary>
    private void SetAnimationStateEnterScaleIn()
    {
        AnimationState loAnimationStateA = goAnimationStatesA[gnDeep];
        AnimationState loAnimationStateB = goAnimationStatesB[gnDeep];

        //<< Establecemos estado A
        loAnimationStateA.goPosition = transform.position;
        loAnimationStateA.goScale = Vector3.zero;
        loAnimationStateA.gnAlpha = 0.0f;
        loAnimationStateA.goCurve = curveEnter;

        //<< Establecemos estado B
        loAnimationStateB.goPosition = goEnterPosition;
        loAnimationStateB.goScale = scaleEnd;
        loAnimationStateB.gnAlpha = 1.0f;

        //<< Establecemos la curva que usaremos para la animacion de entrada.
        SetCurveEnter();

        //<< Establecemos los estados para correr.
        SetStatesToRun();
    }

    /// <summary>
    /// Establece los estados para la animacion para cuando haremos zoom out.
    /// </summary>
    private void SetAnimationStateExitScaleOut()
    {
        AnimationState loAnimationStateA = goAnimationStatesA[gnDeep];
        AnimationState loAnimationStateB = goAnimationStatesB[gnDeep];

        //<< Establecemos estado A
        loAnimationStateA.goPosition = scrollRect.content.position;
        loAnimationStateA.goScale = scrollRect.content.localScale;
        loAnimationStateA.gnAlpha = canvasGroup.alpha;
        loAnimationStateA.goCurve = curveExit;

        //<< Establecemos estado B
        Vector3 loPosition = transform.position + (goClickPosition - scrollRect.content.position);
        loPosition.Scale(goScaleOut);
        loAnimationStateB.goPosition = -loPosition;
        loAnimationStateB.goScale = goScaleOut;
        loAnimationStateB.gnAlpha = 0.0f;

        //<< Establecemos la curva que usaremos para la animacion de salida.
        SetCurveExit();

        //<< Establecemos los estados para correr.
        SetStatesToRun();
    }

    /// <summary>
    /// Establece que estados se usaran para correr la animacion.
    /// </summary>
    /// <param name="loAnimationStateA"></param>
    /// <param name="loAnimationStateB"></param>
    private void SetStatesToRun()
    {
        //<< Establecemos los estados para correr.
        goAnimationStateToRunA = goAnimationStatesA[gnDeep];
        goAnimationStateToRunB = goAnimationStatesB[gnDeep];
    }

    /// <summary>
    /// Establece que estados se usaran para correr la animacion.
    /// </summary>
    /// <param name="loAnimationStateA"></param>
    /// <param name="loAnimationStateB"></param>
    private void SetStatesToRunLast()
    {
        int lnDeep = gnDeep > 0 ? gnDeep - 1 : gnDeep;

        //<< Establecemos los estados para correr.
        goAnimationStateToRunA = goAnimationStatesA[lnDeep];
        goAnimationStateToRunB = goAnimationStatesB[lnDeep];

        curve = goAnimationStateToRunA.goCurve;
    }

    /// <summary>
    /// Establece la curva de entrada.
    /// </summary>
    private void SetCurveEnter()
    {
        curve = curveEnter;
    }

    /// <summary>
    /// Establece la curva de salida.
    /// </summary>
    private void SetCurveExit()
    {
        curve = curveExit;
    }

    /// <summary>
    /// Recibe el indice del elemento al cual deseamos hacer zoom.
    /// </summary>
    /// <param name="lnChildIndex"></param>
    public void ZoomTo(FocusZoom loFocusZoom)
    {
        //<< Asignamos el root.
        if (goParent == null) goRoot = this;

        //<< Asignamos nuestro hijo.
        goChild = loFocusZoom;

        //<< Indicamos que no estamos interactivos y con scroll unrestricted.
        SetInteractableAndMovementType(false);
        goChild.SetInteractableAndMovementType(false);

        //<< Activamos el elemento focus zoom por si está desactivado.
        goChild.gameObject.SetActive(true);

        //<< Almacenamos donde se realizó el click.
        goClickPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);

        //<< Asignamos de que padre venimos.
        goChild.goParent = this;

        //<< Establecemos a donde entraremos los estados para la entrada.
        goChild.SetAnimationStateEnterScaleIn();

        //<< Establecemos el estado de salida.
        SetAnimationStateExitScaleOut();

        //<< Indicamos que entramos al nivel e incrementamos la profundidad para no modificar los datos de la animacion.
        goChild.gnDeep++;
        gnDeep++;

        //<< Indicamos que comenzamos la animacion.
        SetAsA();
        AnimateToB();

        goChild.SetAsA();
        goChild.AnimateToB();
    }

    /// <summary>
    /// Recibe el indice del elemento al cual deseamos hacer zoom.
    /// </summary>
    /// <param name="lnChildIndex"></param>
    public void ZoomToSimple(FocusZoom loFocusZoom)
    {
        ////<< Asignamos el root.
        //if (goParent == null) goRoot = this;

        ////<< Asignamos nuestro hijo.
        //goChild = loFocusZoom;

        ////<< Indicamos que no estamos interactivos y con scroll unrestricted.
        //SetInteractableAndMovementType(false);
        //goChild.SetInteractableAndMovementType(false);

        ////<< Activamos el elemento focus zoom por si está desactivado.
        //goChild.gameObject.SetActive(true);

        ////<< Almacenamos donde se realizó el click.
        //goClickPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);

        ////<< Asignamos de que padre venimos.
        //goChild.goParent = this;

        ////<< Establecemos a donde entraremos los estados para la entrada.
        //goChild.SetAnimationStateEnterScaleIn();

        ////<< Establecemos el estado de salida.
        //SetAnimationStateExitScaleOut();

        ////<< Indicamos que entramos al nivel e incrementamos la profundidad para no modificar los datos de la animacion.
        //goChild.gnDeep++;
        //gnDeep++;

        ////<< Indicamos que comenzamos la animacion.
        //SetAsA();
        //AnimateToB();

        //goChild.SetAsA();
        //goChild.AnimateToB();
    }

    /// <summary>
    /// Esta funcion se conecta con simple animation para actualizar la animacion.
    /// </summary>
    /// <param name="lnDelta"></param>
    private void UpdateAnimation(float lnDelta)
    {
        //<< Actulizamos la posicion del elemento.
        scrollRect.content.position = Vector3.Lerp(goAnimationStateToRunA.goPosition, goAnimationStateToRunB.goPosition, lnDelta);

        //<< Actulizamos la escala del elemento.
        scrollRect.content.localScale = Vector3.Lerp(goAnimationStateToRunA.goScale, goAnimationStateToRunB.goScale, lnDelta);

        //<< Si es animacion de salida usamos alpha
        float lnAlpha = 1.0f;
        Vector3 loScaleChilds = Vector3.zero;
        if (goChild != null)
        {
            lnAlpha = Mathf.Lerp(goAnimationStateToRunA.gnAlpha, goAnimationStateToRunB.gnAlpha, curveAlphaExit.Evaluate(lnDelta));

            loScaleChilds = Vector3.Lerp(Vector3.one, Vector3.zero, lnDelta);
        }
        else loScaleChilds = Vector3.Lerp(Vector3.zero, Vector3.one, lnDelta);

        //<< Si es animacion de salida usamos la 
        if (goChild != null) canvasGroup.alpha = lnAlpha;

        //<< Si tenemos hijos a los cuales deseamos aplicar la animacion les pasamos los datos.
        foreach (CanvasGroup loCanvasGroup in alphaChilds)
        {
            loCanvasGroup.transform.localScale = loScaleChilds;
            loCanvasGroup.alpha = lnAlpha;
        }
    }

    /// <summary>
    /// Realiza la animacion para ir al padre actual.
    /// </summary>
    public void GoToParent()
    {
        if (goParent == null) return;

        //<< Activamos el elemento focus zoom por si está desactivado.
        goParent.gameObject.SetActive(true);
        goParent.SetStatesToRunLast();
        goParent.SetAsB();
        goParent.AnimateToA();

        //<< Establecemos nuestro estado B para regresar desde donde estamos.
        goAnimationStatesB[0].goPosition = scrollRect.content.position;
        goAnimationStatesB[0].goScale = scrollRect.content.localScale;
        SetStatesToRunLast();
        SetAsB();
        AnimateToA();
    }

    /// <summary>
    /// Esta funcion ayuda a que subamos de padre desde el un solo lugar.
    /// </summary>
    public void GoToParentFromRoot()
    {
        if (goRoot == null) return;//<< No hay Root.

        //<< Obtenemos el ultimo hijo.
        FocusZoom loLastElement = goRoot;
        while (true)
        {
            if (loLastElement.goChild == null) break;

            //<< Establecemos que el ultimo elemento es el hijo y checamos si es realmente el ultimo elemento.
            loLastElement = loLastElement.goChild;
        }

        //<< Solo regresamos por que el ultimo elemento es el root.
        if (loLastElement == goRoot) return;

        // Si se está animando solo regresamos.
        if (loLastElement.IsAnimating()) return;
        // //<< Interrumpir animación.
        // if (loLastElement.IsAnimating())
        // {
        //     gnDelta = gnDirection > 0 ? 1.0f : 0.0f;
        //     UpdateAnimationChilds(gnDelta);
        //     UpdateAnimation(gnDelta);
        //     onDelta?.Invoke(curve.Evaluate(gnDelta));
        //     if (gnDirection == 1.0f)
        //     {
        //         OnB();
        //         onB?.Invoke();
        //     }
        //     else
        //     {
        //         OnA();
        //         onA?.Invoke();
        //     }
        //     gnDirection = 0.0f;
        // }

        //<< Realizamos la animacion para ir al padre.
        loLastElement.GoToParent();
    }
}