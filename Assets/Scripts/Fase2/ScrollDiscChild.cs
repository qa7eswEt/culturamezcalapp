﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ScrollDiscChild : MonoBehaviour, IDragHandler, IPointerDownHandler
{
    public ScrollDisc scrollDisc;
    private Button button;

    /// <summary>
    /// 
    /// </summary>
    private void Awake()
    {
        button = GetComponent<Button>();
    }

    /// <summary>
    /// CLick al elemento
    /// </summary>
    /// <param name="loPointerEventData"></param>
    public void OnPointerDown(PointerEventData loPointerEventData)
    {
        button.interactable = true;
        scrollDisc.OnPointerDown(loPointerEventData);
    }

    /// <summary>
    /// Drag de nuestro elemento.
    /// </summary>
    /// <param name="loPointerEventData"></param>
    public void OnDrag(PointerEventData loPointerEventData)
    {
        if (loPointerEventData.clickTime > 1) button.interactable = false;
        scrollDisc.OnDrag(loPointerEventData);
    }
}
