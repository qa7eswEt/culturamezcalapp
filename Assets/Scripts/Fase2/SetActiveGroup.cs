﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetActiveGroup : MonoBehaviour
{
    [Tooltip("Elementos a activar o desactivar")]
    public GameObject[] elements;

    /// <summary>
    /// Activa o desactiva la lista de elementos.
    /// </summary>
    /// <param name="lbActive"></param>
    public void SetActive(bool lbActive)
    {
        foreach (GameObject loGameObject in elements) loGameObject.SetActive(lbActive);
    }
}
