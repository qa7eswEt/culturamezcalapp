﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FocusZoomTemplate : FocusZoom
{
    /// <summary>
    /// Curva para la animacion de entrada.
    /// </summary>
    private static AnimationCurve goCurveEnter = AnimationCurve.EaseInOut(0.0f, 0.0f, 1.0f, 1.0f);

    /// <summary>
    /// Curva para la animacion de salida.
    /// </summary>
    private static AnimationCurve goCurveExit = AnimationCurve.EaseInOut(0.0f, 0.0f, 1.0f, 1.0f);

    /// <summary>
    /// Curva para la animacion del alpha de salida.
    /// </summary>
    private static AnimationCurve goCurveAlphaExit = AnimationCurve.EaseInOut(0.0f, 0.0f, 1.0f, 1.0f);

    /// <summary>
    /// Tiempo para el zoom.
    /// </summary>
    private static float gnTime = 0.0f;

    /// <summary>
    /// Establecemos los datos de la plantilla
    /// </summary>
    protected override void Awake()
    {
        useTemplate = false;
        goCurveEnter = curveEnter;
        goCurveExit = curveExit;
        goCurveAlphaExit = curveAlphaExit;
        gnTime = time;
    }

    /// <summary>
    /// Establece los valores de la plantilla.
    /// </summary>
    /// <param name="loFocusZoom"></param>
    public static void SetTemplateValues(FocusZoom loFocusZoom)
    {
        loFocusZoom.curveEnter = goCurveEnter;
        loFocusZoom.curveExit = goCurveExit;
        loFocusZoom.curveAlphaExit = goCurveAlphaExit;
        loFocusZoom.SetTime(gnTime);
    }
}
