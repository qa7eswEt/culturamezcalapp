﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Graphic))]
public class ColorAnimation : MonoBehaviour, IAnimation
{
    [Tooltip("Estado inicial del alfa")]
    public Color a = Color.white;
    [Tooltip("Estado final del alfa")]
    public Color b = Color.white;

    /// <summary>
    /// Nuestro canvas group para usar el alpha.
    /// </summary>
    private Graphic goGraphic = null;

    /// <summary>
    /// 
    /// </summary>
    private void Awake()
    {
        goGraphic = GetComponent<Graphic>();
    }

    /// <summary>
    /// Se llama atraves de SimpleAnimation para actualizar el valor del alpha.
    /// </summary>
    public void UpdateAnimation(float lnDelta)
    {
        goGraphic.color = Color.Lerp(a, b, lnDelta);
    }
}
