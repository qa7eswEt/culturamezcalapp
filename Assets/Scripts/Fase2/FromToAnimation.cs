﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FromToAnimation : MonoBehaviour, IAnimation
{
    [Tooltip("Posicion A")]
    public RectTransform a;

    [Tooltip("Posicion B")]
    public RectTransform b;

    /// <summary>
    /// Actualiza la animacion con base en el delta.
    /// </summary>
    /// <param name="lnDelta"></param>
    public void UpdateAnimation(float lnDelta)
    {
        transform.position = Vector3.Lerp(a.position, b.position, lnDelta);
    }
}
