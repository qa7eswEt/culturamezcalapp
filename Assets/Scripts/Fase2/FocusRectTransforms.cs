﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FocusRectTransforms : SimpleAnimation
{
    [Tooltip("Canvas para que nos de los valores necesarios para la animacion.")]
    public Canvas canvas;

    [Tooltip("ScrollRect que contendrá los rects que tendrán el foco.")]
    public ScrollRect scrollrect;

    [Tooltip("Elementos que tendrán el foco.")]
    public PathAnimation[] goFocusElements;

    /// <summary>
    /// Para almacenar el offset de nuestra posicion y el elemento seleccionado.
    /// </summary>
    private Vector2 goOffset = Vector2.zero;

    /// <summary>
    /// Elemento seleccionado para animar.
    /// </summary>
    private PathAnimation goSelectedElement = null;

    /// <summary>
    /// 
    /// </summary>
    protected override void Awake()
    {
        base.Awake();
        onB.AddListener(OnB);
        onDelta.AddListener(UpdateAnimation);
    }

    private void OnB()
    {
        goSelectedElement = null;
        scrollrect.inertia = true;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="lnDelta"></param>
    private void UpdateAnimation(float lnDelta)
    {
        //<< Si no hay nada seleccionado solo regresamos.
        if (goSelectedElement == null) return;

        //<< Si tenemos seleccionado actualizamos la posicion.
        Vector2 loPosition = Vector2.zero;
        goSelectedElement.UpdateAnimation(lnDelta, ref loPosition);

        //<< Agregamos el offset de la curva para establecer la posicion correcta.
        loPosition += goOffset;

        //<< Establecemos la nueva posicion del recttransform.
        scrollrect.content.transform.position = Vector2.Lerp((Vector2)scrollrect.content.transform.position, -loPosition, lnDelta);
    }

    /// <summary>
    /// Se llama para hacer foco a un elemento al darle click.
    /// </summary>
    /// <param name="lnFocusElementIndex"></param>
    public void OnClick(int lnFocusElementIndex)
    {
#if DEBUG
        if (lnFocusElementIndex >= goFocusElements.Length) { Debug.Log("[FocusRectTransforms]: El indice del elemento dado supera la lista."); return; }
#endif

        // Checamos que ningun elemento se esté animando.
        if (goSelectedElement != null) return;

        scrollrect.inertia = false;

        //<< Establecemos el elemento seleccionado.
        goSelectedElement = goFocusElements[lnFocusElementIndex];

        //<< Obtenemos el offset de nuestro elemento.
        goOffset = goSelectedElement.transform.position - scrollrect.content.position;

        //<< Establecemos el elemento como A.
        SetAsA();

        //<< Animamos a la posicion B que es el rectransform seleccionado.
        AnimateToB();
    }
}
