﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InverseFlag : MonoBehaviour
{
    public UnityEventBool onFlag;

    public Toggle[] toggles;

    public void Set(bool lbFlag)
    {
        if (onFlag != null) onFlag.Invoke(!lbFlag);
    }

    public void ToggleSetIsOn(bool isOn)
    {
        Toggle toggle = GetComponent<Toggle>();
        toggle.isOn = !isOn;
        toggle.isOn = isOn;
    }

    public void CheckToggles()
    {
        bool lastToggleState = toggles[0].isOn;
        foreach(Toggle toggle in toggles)
        {
            if(toggle.isOn != lastToggleState)
            {
                return;
            }
        }
        //ToggleSetIsOn(lastToggleState);
    }
}
