﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Script que se encarga de mostrar o esconder los globos de tutorial en la seccion de Conoce solamente antes de que le de click por primera vez a algun nodo o texto verde
/// </summary>
public class TutorialNodos : MonoBehaviour
{
    public enum TipoDeTutorial {Circulo, Texto};
    public TipoDeTutorial tipoDeTutorial = TipoDeTutorial.Circulo;

    public delegate void StatusRefresh();
    public static event StatusRefresh InvokeRefresh;

    private const string KEY = "KeyTutorialConoce_";

    // Use this for initialization
    void Start ()
    {
        CheckStatus();
        TutorialNodos.InvokeRefresh += CheckStatus;
	}

    void CheckStatus()
    {
        string currentKey = KEY + System.Enum.GetName(typeof(TipoDeTutorial), tipoDeTutorial);
        if (PlayerPrefs.HasKey(currentKey))
        {
            gameObject.SetActive(false);
        }
    }
	
	public void TurnOffTutorial()
    {
        string currentKey = KEY + System.Enum.GetName(typeof(TipoDeTutorial), tipoDeTutorial);
        PlayerPrefs.SetInt(currentKey, 1);
        gameObject.SetActive(false);
        TutorialNodos.InvokeRefresh();
    }

    void OnDestroy()
    {
        TutorialNodos.InvokeRefresh -= CheckStatus;
    }
}
