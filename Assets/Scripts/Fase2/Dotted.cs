﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dotted : MonoBehaviour
{
    [Tooltip("Son los rectTransforms que crearán nuestra linea.")]
    public RectTransform[] elements;

    [Tooltip("Densidad de puntos por puntos.")]
    public int pointDensity = 20;

    [Tooltip("Indica la distancia entre los elementos en pixeles.")]
    public float distance = 10.0f;

    [Tooltip("Pedazo de nuestra linea.")]
    public RectTransform dot;

    [Tooltip("Indica que debemos crear los puntos en el start.")]
    public bool createOnAwake = true;

    /// <summary>
    /// Elementos que conforman las lineas.
    /// </summary>
    private List<RectTransform> goElements = new List<RectTransform>();

    /// <summary>
    /// Puntos de bezier.
    /// </summary>
    private Vector2[] goPoints = null;

    /// <summary>
    /// 
    /// </summary>
    private void Awake()
    {
        if (createOnAwake) Setup();
    }

    private void Start()
    {

    }

    /// <summary>
    /// 
    /// </summary>
    public void Setup()
    {
        Bezier.CreateElements(transform, elements, pointDensity, distance, goElements, dot, ref goPoints);
    }

#if UNITY_EDITOR
    /// <summary>
    /// Para dibujar los gizmos
    /// </summary>
    void OnDrawGizmos()
    {
        if (!enabled) return;
        RectTransform loRectTransformA = null;
        RectTransform loRectTransformB = null;
        Gizmos.color = Color.magenta;
        int lnCount = elements.Length - 1;
        for (int i = 0; i < lnCount; i++)
        {
            loRectTransformA = elements[i];
            loRectTransformB = elements[i + 1];
            if (loRectTransformA == null) continue;
            if (loRectTransformB == null) continue;
            Gizmos.DrawLine(loRectTransformA.position, loRectTransformB.position);
        }
        
        //<< Chcamos que estén completos los elementos para hacer la curva.
        lnCount = elements.Length;
        for (int i = 0; i < lnCount; i++) { if (elements[i] == null) return; }

        Gizmos.color = Color.cyan;
        Bezier.GetBezierApproximation(elements, pointDensity, ref goPoints);
        lnCount = goPoints.Length - 1;
        for (int i = 0; i < lnCount; i++) Gizmos.DrawLine(goPoints[i], goPoints[i + 1]);
    }
#endif
}
