﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class ImageFullScreen : MonoBehaviour
{
    [Tooltip("Padre de la imagen para sacar sus dimensiones.")]
    public RectTransform parent;

    [Tooltip("Imagen a la cual se asignará el sprite y se acomodará con respecto al padre.")]
    public Image image;

    [Tooltip("Evento que se llama cuando se asigna el sprite.")]
    public UnityEvent onSetSprite;

    /// <summary>
    /// Establece el sprite en la imagen y despues establece las imagenes 
    /// </summary>
    /// <param name="loSprite"></param>
    public void SetSprite(Sprite loSprite)
    {
        //<< Establecemos el sprite en la imagen.
        image.overrideSprite = loSprite;

        //<< Establecemos el tamaño del rectangulo como viene la fuente.
        image.rectTransform.sizeDelta = new Vector2(loSprite.texture.width, loSprite.texture.height);

        //<< Ya que tenemos el tamaño asignado al rectangulo, ahora escalamos para encajarlo en la pantalla.
        float lnScaleWidth = parent.rect.width / loSprite.texture.width;
        float lnScaleHeight = parent.rect.height / loSprite.texture.height;

        //<< Determinamos que escala usaremos si tenemos el ancho mayor o el alto.
        float lnFinalScale = 0.0f;
        if (loSprite.texture.width > loSprite.texture.height) lnFinalScale = lnScaleWidth;
        else lnFinalScale = lnScaleHeight;

        //<< Asignamos la nueva escala a nuestro elemento para que encaje en la pantalla.
        image.transform.localScale = new Vector3(lnFinalScale, lnFinalScale, 1.0f);

        //<< Llamamos el evento de establecer sprite.
        if (onSetSprite != null) onSetSprite.Invoke();
    }

    /// <summary>
    /// Establece el sprite en la imagen y despues establece las imagenes 
    /// </summary>
    /// <param name="loSprite"></param>
    public void SetImage(Image loImage)
    {
        SetSprite(loImage.sprite);
    }
}
