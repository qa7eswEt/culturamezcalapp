﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class HorizontalMatch : MonoBehaviour
{
    [Tooltip("Determina la distancia maxima en y para saber si se considera dentro de rango.")]
    public float range = 10.0f;

    [Tooltip("Elemento que determina desde donde checaremos la distancia.")]
    public Transform from;

    [Tooltip("GameObjects que se usaran para determinar quien es el mas cercano a nuestro elemento.")]
    public HorizontalMatchElement[] elements;

    [Tooltip("Evento que se llama cuando si hay match.")]
    public UnityEvent onMatch;

    [Tooltip("Evento que se llama cuando no hay match.")]
    public UnityEvent onNoMatch;

    /// <summary>
    /// 
    /// </summary>
    public void Check()
    {
        //<< Determinamos cual de los elementos es el mas cercano.
        float lnMinDistance = float.MaxValue;
        float lnDistance = 0.0f;

        HorizontalMatchElement loCloser = null;
        foreach (HorizontalMatchElement loHorizontalMatchElement in elements)
        {
            //<< Obtenemos la distancia.
            lnDistance = Vector3.Distance(loHorizontalMatchElement.transform.position, from.transform.position);

            //<< Checamos si somo el mas cercano
            if (lnDistance > lnMinDistance) continue;

            //<< Ya que somos el mas cercano lo establecemos como tal.
            loCloser = loHorizontalMatchElement;
            lnMinDistance = lnDistance;
        }

        //<< Checamos que tengamos al mas cercano y que esté dentro de rango.
        if (loCloser == null) { OnNoMatch(); return; }
        float lnDiferenceY = Mathf.Abs(from.transform.position.y - loCloser.transform.position.y);
        if (lnDiferenceY > range) { OnNoMatch(); return; }
        loCloser.OnMatch();
        if (onMatch != null) onMatch.Invoke();
    }

    /// <summary>
    /// Se llama cuando no hay match.
    /// </summary>
    private void OnNoMatch()
    {
        if (onNoMatch != null) onNoMatch.Invoke();
    }

    /// <summary>
    /// 
    /// </summary>
    private void OnDrawGizmos()
    {
        if (from == null) return;

        //<< Sacamos las posiciones de las lineas.
        Vector3 loTop = from.transform.position;
        loTop.y += range;

        Vector3 loBottom = from.transform.position;
        loBottom.y -= range;

        //<< Determinamos las posiciones de las lineas.
        Vector3 loTopLeft = loTop;
        Vector3 loTopRight = loTop;
        loTopLeft.x -= 3.0f;
        loTopRight.x += 3.0f;

        Vector3 loBottomLeft = loBottom;
        Vector3 loBottomRight = loBottom;
        loBottomLeft.x -= 3.0f;
        loBottomRight.x += 3.0f;

        //<< Dibujamos las lineas que determinaran el rango de entrada del elemento.
        Gizmos.color = Color.cyan;
        Gizmos.DrawLine(loTopLeft, loTopRight);
        Gizmos.DrawLine(loBottomLeft, loBottomRight);

        //<< Dibujamos las lineas del from a los to's
        Gizmos.color = Color.magenta;
        foreach (HorizontalMatchElement loHorizontalMatchElement in elements)
        {
            if (loHorizontalMatchElement == null) continue;
            Gizmos.DrawLine(from.transform.position, loHorizontalMatchElement.transform.position);
        }
    }
}
