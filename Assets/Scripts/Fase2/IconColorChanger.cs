﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class IconColorChanger : MonoBehaviour
{

    public Color ActiveIconColor;
    public Color InactiveIconColor;

    public void SetActiveColor()
    {
        GetComponent<Image>().color = ActiveIconColor;
    }

    public void SetInactiveColor()
    {
        GetComponent<Image>().color = InactiveIconColor;
    }
}
