﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;

public class SingleEvent : MonoBehaviour
{
    [Tooltip("Solo informacion del evento, no se usa para nada mas.")]
    public string info = "EventX";

    [Tooltip("Evento que se llama.")]
    public UnityEvent callEvent;

    // Update is called once per frame
    public void CallEvent()
    {
        if (callEvent != null) callEvent.Invoke();
    }
}
