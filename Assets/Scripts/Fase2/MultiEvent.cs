﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using System.Collections.Generic;

public class MultiEvent : MonoBehaviour
{
    [Tooltip("Evento en comun que se llama cuando alguno de los otros se llama.")]
    public UnityEvent commonEvent;

    [Tooltip("Es el indice del evento actual a ser llamado.")]
    public int eventIndex = 0;

    [Tooltip("Lista de eventos que sirven para ser llamados dependiendo del indice establecido en el valor eventIndex.")]
    public UnityEvent[] theEvents;

    /// <summary>
    /// Realiza la llamada del indice de evento.
    /// </summary>
    public void CallEvent()
    {
        if (theEvents.Length == 0) return;
        if (eventIndex >= theEvents.Length) { Debug.Log("[MultiEvent]: Indice fuera de rango - " + eventIndex.ToString() + " GameObject: " + gameObject.name); return; }
        commonEvent.Invoke();
        theEvents[eventIndex].Invoke();
    }

    /// <summary>
    /// Realiza la llamada del indice de evento.
    /// </summary>
    public void CallEvent(int lnIndex)
    {
        if (theEvents.Length == 0) return;
        if (lnIndex >= theEvents.Length) { Debug.Log("[MultiEvent]: Indice fuera de rango - " + lnIndex.ToString()); return; }
        commonEvent.Invoke();
        theEvents[lnIndex].Invoke();
    }

    /// <summary>
    /// Establece el indice del evento.
    /// </summary>
    /// <param name="lnIndex"></param>
    public void SetEventIndex(int lnIndex)
    {
        eventIndex = lnIndex;
    }
}
