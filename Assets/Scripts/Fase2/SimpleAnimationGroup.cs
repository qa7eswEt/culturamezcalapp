﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleAnimationGroup : SimpleAnimation
{
    [Tooltip("Porcentaje de Tiempo entre cada elemento para que sean animaciones defasadas."), Range(0.0f, 1.0f)]
    public float perecentTimeOverlap = 0.0f;

    [Tooltip("Todos los elementos que generan la escalera de animaciones.")]
    public SimpleAnimation[] simpleAnimations;

    /// <summary>
    /// Indice del elemento que se está reproduciendo.
    /// </summary>
    private int gnIndex = 0;

    /// <summary>
    /// Tiempo que esperamos para llamar la siguiente animacion.
    /// </summary>
    private float gnTimeWait = 0.0f;

    /// <summary>
    /// Coroutine.
    /// </summary>
    private IEnumerator goCoroutine = null;

    /// <summary>
    /// 
    /// </summary>
    protected override void Awake()
    {
        //<< Establecemos el tiempo de la animacion por si no hay elementos.
        SetTime(time);

        //<< Si no hay elementos solo regresamos.
        if (simpleAnimations.Length == 0) return;

        //<< Determinamos el tiempo entre animaciones.
        float lnTimeBetween = time / simpleAnimations.Length;

        //<< Obtenemos el tiempo de sobreposicion de los elementos.
        float lnTimeOverlap = lnTimeBetween * perecentTimeOverlap;

        //<< Obtenemos el tiempo que debemos esperar entre animaciones.
        gnTimeWait = lnTimeBetween - lnTimeOverlap;

        //<< Establecemos el nuevo tiempo de la animacion completa.
        SetTime(time - lnTimeOverlap);

        //<< Establecemos los tiempos de las animaciones.
        foreach (SimpleAnimation loSimpleAnimation in simpleAnimations) loSimpleAnimation.SetTime(lnTimeBetween);
    }

    /// <summary>
    /// Establece a todos los simple animations en estado A
    /// </summary>
    public void SetActive(bool lbActive)
    {
        foreach (SimpleAnimation loSimpleAnimation in simpleAnimations) loSimpleAnimation.gameObject.SetActive(lbActive);
    }

    /// <summary>
    /// Establece a todos los simple animations en estado A
    /// </summary>
    public override void SetAsA()
    {
        foreach (SimpleAnimation loSimpleAnimation in simpleAnimations) loSimpleAnimation.SetAsA();
    }

    /// <summary>
    /// Establece a todos los simple animations en estado B
    /// </summary>
    public override void SetAsB()
    {
        foreach (SimpleAnimation loSimpleAnimation in simpleAnimations) loSimpleAnimation.SetAsB();
    }

    /// <summary>
    /// Detiene todo el flujo.
    /// </summary>
    public override void Stop()
    {
        base.Stop();

        //<< Detenemos la corrouitna primero.
        if (goCoroutine != null) StopCoroutine(goCoroutine);

        //<< Detenemos todos los elmentos.
        foreach (SimpleAnimation loSimpleAnimation in simpleAnimations) loSimpleAnimation.Stop();
    }

    /// <summary>
    /// Comienza la animacion.
    /// </summary>
    public override void AnimateToB()
    {
        //<< Detenemos cualquer animacion anterior.
        Stop();

        //<< Establecemos todos nuestros elementos al estado A.
        SetActive(true);
        SetAsA();

        //<< Iniciamos el proceso de animacion.
        goCoroutine = StartCoroutine(gnTimeWait, simpleAnimations);
        StartCoroutine(goCoroutine);

        base.AnimateToB();
    }

    /// <summary>
    /// Coroutina para iniciar las animaciones.
    /// </summary>
    /// <returns></returns>
    private static IEnumerator StartCoroutine(float lnTimeWait, SimpleAnimation[] loSimpleAnimations)
    {
        WaitForSeconds loWaitForSeconds = new WaitForSeconds(lnTimeWait);
        foreach (SimpleAnimation loSimpleAnimation in loSimpleAnimations)
        {
            loSimpleAnimation.AnimateToB();
            yield return loWaitForSeconds;
        }

        yield break;
    }
}
