﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class UnityEventDelta : UnityEvent<float> { }

public class SimpleAnimation : MonoBehaviour
{
    [Tooltip("Tiempo para realizar la animacion.")]
    public float time = 0.5f;

    [Tooltip("Curva para la animacion.")]
    public AnimationCurve curve = AnimationCurve.EaseInOut(0.0f, 0.0f, 1.0f, 1.0f);

    [Tooltip("Evento de cuando se actualiza el delta.")]
    public UnityEventDelta onDelta;

    [Tooltip("Evento de cuando se llega al estado A.")]
    public UnityEvent onA;

    [Tooltip("Evento de cuando se llega al estado B.")]
    public UnityEvent onB;

    /// <summary>
    /// Indica la direccion de la animacion.
    /// </summary>
    protected float gnDirection = 0.0f;

    /// <summary>
    /// Delta de la animacion.
    /// </summary>
    protected float gnDelta = 1.0f;

    /// <summary>
    /// Velocidad de la animacion.
    /// </summary>
    protected float gnSpeed = 0.0f;

    /// <summary>
    /// Para actualizar las animaciones automaticas que comparten gameobject.
    /// </summary>
    private IAnimation[] goIAnimations = new IAnimation[0];

    /// <summary>
    /// 
    /// </summary>
    protected virtual void Awake()
    {
        //<< Establecemos el tiempo de la animacion.
        SetTime(time);

        //<< Obtenemos los elementos a conectar automaticamente.
        goIAnimations = GetComponents<IAnimation>();
    }

    /// <summary>
    /// Establece el tiempo de la animacion.
    /// </summary>
    /// <param name="lnTime"></param>
    public void SetTime(float lnTime)
    {
        gnSpeed = 1.0f / lnTime;
    }

    /// <summary>
    /// 
    /// </summary>
    protected virtual void Update()
    {
        //<< Si no estamos animando solo regresamos.
        if (gnDirection == 0.0f) return;

        //<< Avanzamos el delta con respecto a el tiempo y velocidad.
        gnDelta += Time.deltaTime * gnSpeed * gnDirection;

        //<< Clamp Value.
        if (gnDelta > 1.0f) gnDelta = 1.0f;
        else if (gnDelta < 0.0f) gnDelta = 0.0f;

        //<< Avisamos de la actualizacion.
        UpdateAnimationChilds(gnDelta);
        UpdateAnimation(gnDelta);
        if (onDelta != null) onDelta.Invoke(curve.Evaluate(gnDelta));

        //<< Avisamos del termino si cumplimos.
        if ((gnDirection == 1.0f && gnDelta >= 1.0f))
        {
            OnB();
            if (onB != null) onB.Invoke();
            gnDirection = 0.0f;
        }
        else if (gnDirection == -1.0f && gnDelta <= 0.0f)
        {
            OnA();
            if (onA != null) onA.Invoke();
            gnDirection = 0.0f;
        }
    }

    /// <summary>
    /// Actualiza las animaciones automaticas.
    /// </summary>
    /// <param name="lnDelta"></param>
    protected void UpdateAnimationChilds(float lnDelta)
    {
        foreach (IAnimation loIAnimation in goIAnimations) loIAnimation.UpdateAnimation(lnDelta);
    }

    /// <summary>
    /// Actualiza las animaciones automaticas.
    /// </summary>
    /// <param name="lnDelta"></param>
    protected virtual void UpdateAnimation(float lnDelta)
    {
    }

    /// <summary>
    /// Se llama cuando se llega al estado A.
    /// </summary>
    protected virtual void OnA()
    {
    }

    /// <summary>
    /// Se llama cuando se llega al estado B.
    /// </summary>
    protected virtual void OnB()
    {
    }

    /// <summary>
    /// Detenemos la animacion.
    /// </summary>
    public virtual void Stop()
    {
        gnDirection = 0.0f;
    }

    /// <summary>
    /// Anima la barra para que se agrande.
    /// </summary>
    public virtual void AnimateToB()
    {
        if (gnDelta >= 1.0f) return;
        if (gnDirection == 1.0f) return;
        gnDirection = 1.0f;
    }

    /// <summary>
    /// Anima la barra para que se reduzca.
    /// </summary>
    public virtual void AnimateToA()
    {
        if (gnDelta <= 0.0f) return;
        if (gnDirection == -1.0f) return;
        gnDirection = -1.0f;
    }

    /// <summary>
    /// Establece nuestro elemento como si estuviera reducido.
    /// </summary>
    public virtual void SetAsA()
    {
        gnDelta = 0.0f;
        gnDirection = 0.0f;
        if (onDelta != null) onDelta.Invoke(curve.Evaluate(gnDelta));
    }

    /// <summary>
    /// Establece nuestro elemento como si estuviera reducido.
    /// </summary>
    public virtual void SetAsB()
    {
        gnDelta = 1.0f;
        gnDirection = 0.0f;
        if (onDelta != null) onDelta.Invoke(curve.Evaluate(gnDelta));
    }

    /// <summary>
    /// Indica si estamos animando el elemento.
    /// </summary>
    /// <returns></returns>
    public bool IsAnimating()
    {
        if (gnDirection == 0.0f) return false;
        return true;
    }

    /// <summary>
    /// Indica si se está animando hacia A
    /// </summary>
    /// <returns></returns>
    public virtual bool IsAnimatingToA()
    {
        if (gnDirection == -1.0f) return true;
        return false;
    }

    /// <summary>
    /// Indica si se está animando hacia B
    /// </summary>
    /// <returns></returns>
    public virtual bool IsAnimatingToB()
    {
        if (gnDirection == 1.0f) return true;
        return false;
    }
}