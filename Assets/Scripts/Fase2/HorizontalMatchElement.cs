﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class HorizontalMatchElement : MonoBehaviour
{
    public UnityEvent onMatch; 

    /// <summary>
    /// Se llama cuando hay un match en el horizontal.
    /// </summary>
    public void OnMatch()
    {
        if (onMatch != null) onMatch.Invoke();
    }
}
