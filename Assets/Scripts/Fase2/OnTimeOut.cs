﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

/// <summary>
/// Cñase que realiza un contador de segundos para acciones posteriores
/// Creado por Robot 2017.
/// </summary>

public class OnTimeOut : MonoBehaviour
{
    [Tooltip("Segundos a esperar.")]
    public float seconds = 0;

    [Tooltip("Se llama cuando se termina el tiempo indicado.")]
    public UnityEvent onTimeOut;

    /// <summary>
    /// Tiempo almacenado.
    /// </summary>
    private float gnFlaggedTime = 0;

    /// <summary>
    /// 
    /// </summary>
    private void Update()
    {
        //<< Si el tiempo bandera es cero sabemos que no hay evento activo.
        if (gnFlaggedTime == 0) return;

        //<< Aún no pasa el tiempo establecido.
        if (Time.time < gnFlaggedTime) return;

        //<< Timeout del evento.
        OnTimeOutEvent();
        if (onTimeOut != null) onTimeOut.Invoke();
        gnFlaggedTime = 0;
    }

    /// <summary>
    /// Se llama para esperar el tiempo.
    /// </summary>
    public void CallEvent()
    {
        gnFlaggedTime = seconds + Time.time;
    }

    /// <summary>
    /// Se llama cuando termina el tiempo.
    /// </summary>
    protected virtual void OnTimeOutEvent()
    {

    }

    /// <summary>
    /// Se llama para esperar el tiempo dado.
    /// </summary>
    /// <param name="lnSeconds"></param>
    public void CallEvent(float lnSeconds)
    {
        gnFlaggedTime = lnSeconds + Time.time;
    }

    /// <summary>
    /// Detiene el evento.
    /// </summary>
    public virtual void Stop()
    {
        gnFlaggedTime = 0.0f;
    }
}
