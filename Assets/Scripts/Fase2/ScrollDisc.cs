﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.Events;

public class ScrollDisc : MonoBehaviour, IDragHandler, IPointerDownHandler
{
    [Tooltip("Angulo minimo de rotacion.")]
    public float minAngle = 110.0f;

    [Tooltip("Angulo maximo de rotacion.")]
    public float maxAngle = 276.0f;

    [Tooltip("Canvas para determinar que tipo de render mode tiene.")]
    public Canvas canvas;

    [Tooltip("Evento que se llama cada ves que se mueve el disco.")]
    public UnityEvent onScrollChanged;

    [Tooltip("RectTransform al cual se le aplicará la rotacion.")]
    public RectTransform rectTransform;

    /// <summary>
    /// Angulo guardado para rotar el disco.
    /// </summary>
    private float gnSavedAngle = 0.0f;

    /// <summary>
    /// Angulo guardado para rotar el disco.
    /// </summary>
    private float gnCurrentAngle = 0.0f;

    /// <summary>
    /// Drag de nuestro elemento.
    /// </summary>
    /// <param name="loPointerEventData"></param>
    public void OnDrag(PointerEventData loPointerEventData)
    {
        OnDragIn(loPointerEventData.position);
    }

    /// <summary>
    /// Drag interno.
    /// </summary>
    /// <param name="loPositon"></param>
    private void OnDragIn(Vector2 loPositon)
    {
        Vector3 loPointerPosition = GetTruePoint(loPositon);

        float lnAngle = CalculateAngle(loPointerPosition, rectTransform.position);
        float lnCurrentAngle = gnCurrentAngle + lnAngle - gnSavedAngle;

        //<< Checamos que esté dentro del angulo establecido.
        if (lnCurrentAngle < minAngle) lnCurrentAngle = minAngle;
        else if (lnCurrentAngle > maxAngle) lnCurrentAngle = maxAngle;

        //<< Establecemos el angulo ya que pasamos los filtros.
        gnSavedAngle = lnAngle;
        gnCurrentAngle = lnCurrentAngle;
        rectTransform.rotation = Quaternion.Euler(0.0f, 0.0f, gnCurrentAngle);

        //<< Avisamos que se movio el disco.
        if (onScrollChanged != null) onScrollChanged.Invoke();
    }

    /// <summary>
    /// Evento on pointer down.
    /// </summary>
    /// <param name="loPointerEventData"></param>
    public void OnPointerDown(PointerEventData loPointerEventData)
    {
        OnPointerDownIn(loPointerEventData.pressPosition);
    }

    /// <summary>
    /// OnPointerDown Interno.
    /// </summary>
    /// <param name="loPosition"></param>
    public void OnPointerDownIn(Vector2 loPosition)
    {
        Vector3 loPointerPosition = GetTruePoint(loPosition);
        gnSavedAngle = CalculateAngle(loPointerPosition, rectTransform.transform.position);//<< Guardamos el angulo.
    }

    /// <summary>
    /// Calculate the angle based on a point.
    /// </summary>
    /// <param name="loPoint">Point based on mouse position.</param>
    /// <returns>Angle from 0 to 360.</returns>
    float CalculateAngle(Vector3 loPoint, Vector3 loCenter)
    {
        Vector2 loPosition = new Vector2((loCenter.x - loPoint.x), (loCenter.y - loPoint.y));
        loPosition.Normalize();
        float lnAngle = (float)(Math.Atan2(loPosition.y, loPosition.x) * 180.0f / Math.PI);
        if (lnAngle < 0.0f) lnAngle = 360.0f + lnAngle;
        return lnAngle;
    }

    /// <summary>
    /// Obtiene el punto con base en el render mode del canvas.
    /// </summary>
    /// <param name="loPointerEventData"></param>
    /// <returns></returns>
    private Vector3 GetTruePoint(Vector2 loPositon)
    {
        Vector3 loPointerPosition = loPositon;
        switch (canvas.renderMode)
        {
            case RenderMode.ScreenSpaceOverlay: break;
            case RenderMode.ScreenSpaceCamera: loPointerPosition = canvas.worldCamera.ScreenToWorldPoint(loPointerPosition); break;
            case RenderMode.WorldSpace: break;
            default: break;
        }
        return loPointerPosition;
    }
}
