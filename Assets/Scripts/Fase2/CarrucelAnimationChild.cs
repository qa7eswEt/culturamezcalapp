﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class CarrucelAnimationChild : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    private CarrucelAnimation goCarrucelAnimation;

    /// <summary>
    /// 
    /// </summary>
    private void Awake()
    {
        goCarrucelAnimation = GetComponentInParent<CarrucelAnimation>();
    }

    /// <summary>
    /// PointerDown
    /// </summary>
    /// <param name="loPointerEventData"></param>
    public void OnPointerDown(PointerEventData loPointerEventData)
    {
        goCarrucelAnimation.OnPointerDown(loPointerEventData);
    }

    /// <summary>
    /// PointerUp
    /// </summary>
    /// <param name="loPointerEventData"></param>
    public void OnPointerUp(PointerEventData loPointerEventData)
    {
        goCarrucelAnimation.OnPointerUp(loPointerEventData);
    }
}
