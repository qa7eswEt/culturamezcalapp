﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScaleAnimation : MonoBehaviour, IAnimation
{
    /// <summary>
    /// Escala inicial de nuestro elemento.
    /// </summary>
    public Vector3 a = Vector3.one;

    /// <summary>
    /// Escala final de nuestro elemento.
    /// </summary>
    public Vector3 b = Vector3.one;

    /// <summary>
    /// Funcion que se llama atraves de simple animation para actualizar la escala.
    /// </summary>
    public void UpdateAnimation(float lnDelta)
    {
        transform.localScale = Vector3.Lerp(a, b, lnDelta);
    }
}
