﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Video;

public class VideoPlayerEvents : MonoBehaviour
{
    [Tooltip("Ignora el fin del video y llama el evento en el start.")]
    public bool onStart = false;

    [Tooltip("Evento que se llama cuando termina el video.")]
    public UnityEvent onLoopPointReached;

    public GameObject renderTexture;

    /// <summary>
    /// Nuestro videoplayer para conectar los eventos.
    /// </summary>
    private VideoPlayer goVideoPlayer = null;

    /// <summary>
    /// 
    /// </summary>
    void Awake()
    {
        //<< Establecemos el evento de cuando se "termina" la reproduccion.
        if (onStart) return;

        //<< Obtenemos el componente para conectar los eventos.
        goVideoPlayer = GetComponent<VideoPlayer>();

        //<< Nosregistramos al evento.
        goVideoPlayer.loopPointReached += OnLoopPointReached;

        //nos registramos al evento para cuando termina de preparase el video
        goVideoPlayer.prepareCompleted += OnPrepareCompleted;
        goVideoPlayer.Prepare();
    }

    private void OnPrepareCompleted(UnityEngine.Video.VideoPlayer vPlayer)
    {
        //goVideoPlayer.Play();
        //renderTexture.SetActive(true);
        StartCoroutine(PlayWithDelay());
    }

    private IEnumerator PlayWithDelay()
    {
        goVideoPlayer.Play();
        yield return new WaitForSeconds(0.2f);
        renderTexture.SetActive(true);
    }

    private void Start()
    {
        if (!onStart) return;
        if (onLoopPointReached != null) onLoopPointReached.Invoke();
    }

    private void Update()
    {
        if(Input.GetMouseButtonDown(0))
        {
            if (onLoopPointReached != null) onLoopPointReached.Invoke();
        }
    }

    /// <summary>
    /// Evento que manda a llamar al evento... yep pero en editor se puede llamar sin usar un 
    /// </summary>
    /// <param name="loVideoPlayer"></param>
    private void OnLoopPointReached(VideoPlayer loVideoPlayer)
    {
        if (onLoopPointReached != null) onLoopPointReached.Invoke();
    }

    public void OnClickLink()
    {
        WebViewManager.Instance.OpenLink("https://culturamezcal.com/compra.html");
    }
}
