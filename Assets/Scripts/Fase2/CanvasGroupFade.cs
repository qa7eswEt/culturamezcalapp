﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CanvasGroup))]
public class CanvasGroupFade : MonoBehaviour, IAnimation
{
    [Tooltip("Estado inicial del alfa")]
    public float a = 0.0f;
    [Tooltip("Estado final del alfa")]
    public float b = 1.0f;

    /// <summary>
    /// Nuestro canvas group para usar el alpha.
    /// </summary>
    CanvasGroup goCanvasGroup = null;

    /// <summary>
    /// 
    /// </summary>
    private void Awake()
    {
        goCanvasGroup = GetComponent<CanvasGroup>();
    }

    /// <summary>
    /// Se llama atraves de SimpleAnimation para actualizar el valor del alpha.
    /// </summary>
    public void UpdateAnimation(float lnDelta)
    {
        goCanvasGroup.alpha = Mathf.Lerp(a, b, lnDelta);
    }
}
