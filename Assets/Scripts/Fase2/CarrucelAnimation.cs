﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

[RequireComponent(typeof(ScrollRect))]
public class CarrucelAnimation : SimpleAnimation, IPointerDownHandler, IPointerUpHandler
{
    /// <summary>
    /// Distancia requerida para ser tomado el gesto como swipe.
    /// </summary>
    private const float gnRequiredDistance = 0.0005f;

    /// <summary>
    /// Velocidad requerida para ser tomado el gesto como swipe.
    /// </summary>
    private const float gnRequiredSpeed = 0.005f;

    /// <summary>
    /// Velocidad que se va incrementando frame a frame.
    /// </summary>
    private const float gnSpeedIncrement = 0.15f;

    [Tooltip("Indica la primer pagina seleccionada.")]
    public int firstSelection = 0;

    [Tooltip("Padre que contiene las Paginas de nuestro carrucel.")]
    public RectTransform parentOfPages;

    [Tooltip("Paginas de nuestro carrucel.")]
    public RectTransform[] pages;

    [Tooltip("Padre que contiene las puntos, botones de las paginas.")]
    public RectTransform parentOfDots;

    [Tooltip("Punto que servirá de referencia para crear los puntos.")]
    public Toggle toggleDummy;

    /// <summary>
    /// Nuestro scrollrect para sacar el contenido
    /// </summary>
    private ScrollRect goScrollRect = null;

    /// <summary>
    /// Tiempo en el que se registró el primer click.
    /// </summary>
    private float gnClickInitialTime = 0.0f;

    /// <summary>
    /// Almacenamos en punto inicial del drag en pantalla.
    /// </summary>
    private Vector3 goClickInitialPositionScreen = Vector3.zero;

    /// <summary>
    /// Almacenamos en punto inicial del drag.
    /// </summary>
    private Vector3 goClickInitialPosition = Vector3.zero;

    /// <summary>
    /// Elemento seleccionado para animar.
    /// </summary>
    private RectTransform goSelectedElement = null;

    /// <summary>
    /// Donde comienza la posicion del contenido.
    /// </summary>
    private Vector3 goFrom;

    /// <summary>
    /// Donde termina la posicion del contenido.
    /// </summary>
    private Vector3 goTo;

    /// <summary>
    /// Indice actual.
    /// </summary>
    private int gnIndex = -1;

    /// <summary>
    /// 
    /// </summary>
    private Toggle[] goToggles;

    /// <summary>
    /// 
    /// </summary>
    protected override void Awake()
    {
        base.Awake();
        goScrollRect = GetComponent<ScrollRect>();

        //<< Creamos tantos puntos como elementos.
        int lnCount = pages.Length;
        goToggles = new Toggle[lnCount];
        for (int i = 0; i < lnCount; i++) goToggles[i] = Instantiate<Toggle>(toggleDummy, parentOfDots, false);

        //<< Desactivamos todos los toggles.
        foreach (Toggle loToggle in goToggles)
        {
            loToggle.interactable = false;
            loToggle.isOn = false;
        }

        //<< Desactivamos el dummy para que no se vea
        toggleDummy.gameObject.SetActive(false);
    }

    /// <summary>
    /// 
    /// </summary>
    private void Start()
    {
        GoToPageNoAnimation(firstSelection);
    }

    /// <summary>
    /// Desactiva todos los toggles y activa el correcto.
    /// </summary>
    /// <param name="lnIndex"></param>
    private void SetSelectedElement(int lnIndex)
    {
        //<< Establecemos nuestro indice actual.
        gnIndex = lnIndex;

        //<< Establecemos el elemento seleccionado.
        goSelectedElement = pages[gnIndex];

        //<< Desactivamos todos los toggles.
        foreach (Toggle loToggle in goToggles) loToggle.isOn = false;

        //<< Activamos el toggle del indice.
        goToggles[gnIndex].isOn = true;
    }

    /// <summary>
    /// Para checar el inicio del drag.
    /// </summary>
    /// <param name="loPointerEventData"></param>
    public void OnPointerDown(PointerEventData loPointerEventData)
    {
        if (IsAnimating()) return;

        gnClickInitialTime = Time.time;
        goClickInitialPositionScreen = loPointerEventData.position;
        Vector3 loPosition = loPointerEventData.position;
        loPosition.z = Camera.main.nearClipPlane;
        goClickInitialPosition = Camera.main.ScreenToWorldPoint(loPosition);
    }

    /// <summary>
    /// Para checar el fin del drag.
    /// </summary>
    /// <param name="loPointerEventData"></param>
    public void OnPointerUp(PointerEventData loPointerEventData)
    {
        if (IsAnimating()) return;

        Vector3 loPosition = loPointerEventData.position;
        loPosition.z = Camera.main.nearClipPlane;
        Vector3 loWorldPosition = Camera.main.ScreenToWorldPoint(loPosition);

        //<< Determinamos el tiempo del click.
        float lnTime = Time.time - gnClickInitialTime;
        float lnDistance = Vector3.Distance(loWorldPosition, goClickInitialPosition);
        float lnSpeed = lnDistance / lnTime;

        //Debug.Log("Speed: " + lnSpeed + " Disatance: " + lnDistance);

        //<< Checamos si cumplimos los requerimientos (velocidad y distancia) para que se considere como swipe.
        if (lnDistance < gnRequiredDistance) return;
        if (lnSpeed < gnRequiredSpeed) return;

        //<< Asignamos la velocidad que obtuvimos.
        gnSpeed = lnSpeed;
        Stop();

        //<< Determinamos la direccion del swipe.
        if (loPointerEventData.position.x < goClickInitialPositionScreen.x) GoToPage(gnIndex + 1);
        else GoToPage(gnIndex - 1);
    }

    /// <summary>
    /// LateUpdate
    /// </summary>
    private void LateUpdate()
    {
        if (goSelectedElement == null) return;
        if (IsAnimating()) { gnSpeed += gnSpeedIncrement; return; }

        parentOfPages.transform.position = GetToPosition() + (goScrollRect.content.position - goScrollRect.transform.position);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="lnDelta"></param>
    protected override void UpdateAnimation(float lnDelta)
    {
        //<< Si no hay nada seleccionado solo regresamos.
        if (goSelectedElement == null) return;

        //<< Establecemos la nueva posicion del recttransform.
        parentOfPages.position = Vector3.Lerp(goFrom, goTo, lnDelta);
    }

    /// <summary>
    /// Se llama para hacer foco al elemento del indice seleccionado.
    /// </summary>
    /// <param name="lnIndex"></param>
    public void GoToPage(int lnIndex)
    {
        //<< Evitamos que pase del indice.
        if (lnIndex >= pages.Length) return;
        if (lnIndex < 0) return;

        //<< Checamos que no se esté animando.
        if (IsAnimating()) return;

        //<< Desactivamos la inercia para que al terminar la animacion no se siga.
        goScrollRect.inertia = false;

        //<< Establecemos el elemento seleccionado.
        SetSelectedElement(lnIndex);

        //<< Obtenemos el offset de nuestro elemento para hacer la animacion.
        goFrom = parentOfPages.position;
        goTo = GetToPosition();

        //<< Establecemos el elemento como A.
        SetAsA();

        //<< Animamos a la posicion B que es el rectransform seleccionado.
        AnimateToB();
    }

    /// <summary>
    /// Establece la posicion a la pagina seleccionada.
    /// </summary>
    /// <param name="lnIndex"></param>
    public void GoToPageNoAnimation(int lnIndex)
    {
        //<< Evitamos que pase del indice.
        if (lnIndex >= pages.Length) return;
        if (lnIndex < 0) return;

        //<< Checamos que no se esté animando.
        if (IsAnimating()) return;

        //<< Establecemos el elemento seleccionado.
        SetSelectedElement(lnIndex);

        //<< Establecemos la posicion para que el elemento sea el primero seleccionado.
        parentOfPages.position = GetToPosition();
    }

    /// <summary>
    /// Obtiene la posicion del elemento seleccionado con base al offset del elemento.
    /// </summary>
    /// <returns></returns>
    private Vector3 GetToPosition()
    {
        if (goSelectedElement == null) return Vector3.zero;
        return goScrollRect.transform.position - (goSelectedElement.position - parentOfPages.position);
    }
}
