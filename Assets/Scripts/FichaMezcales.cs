﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using CulturaMezcalJsons;
public class FichaMezcales: MonoBehaviour
{
    public delegate void WillCloseDelegate();
    public WillCloseDelegate OnClose;
	public Text Titulo;
	public Text Titulo2;
	public GameObject Calificacion;
	public GameObject Calificar;
	public Sprite Magueylleno;
	public Text Agave;
	public Texture2D Textur2D;
	public WebImage Foto;
	public Text Precio;
	public Text AcercaAgave;
	public Text AcercaRegion;
    public Text AcercaMaguey;
    public Text AcercaEstado;
    public Text AcercaMaestro;
	public Text AcercaGrados;
	public Text AcercaDe;
	public Sprite Corazonlleno;
	public Sprite CorazonVacio;
	public GameObject Favoritos;
	public Text EstadisticaCant;
	public GameObject Categoria1;
	public GameObject Categoria2;
	public GameObject Perfil;
	public GameObject Opinions;
	public GameObject relacionados;
	public string URLTienda;
	public GameObject BackFichaProducto;
	List<string> estadisticas = new List<string> ();
	List<string> estadisticas2 = new List<string> ();
	FichaMezcal fm = new FichaMezcal ();
	//public GameObject Contect;
    public MapaDegustacion Mapa;
    public GameObject Panel;
    public Text ano;
    public Text litros;
    public Text origen;
    public Text maduracion;
    public Text terruno;
    public Text conocimiento;
    public Text molido;
    public Text agua;
    public Text fermentacion;
    public Text destilador;
    public Text destilaciones;
    public Text ajuste;
    public Text abocado;
    public GameObject AcercaDeGO;
    public VerticalLayoutGroup ScrollContent;
    public ContentSizeFitter InfoText;
    public VerticalLayoutGroup AboutText;
    public GameObject WebViewPrefab;
    // Use this for initialization

    public GameObject AcercaAgaveGO;
    public GameObject AcercaRegionGO;
    public GameObject AcercaMagueyGO;
    public GameObject AcercaEstadoGO;
    public GameObject AcercaMaestroGO;
    public GameObject AcercaGradosGO;
    public GameObject anoGO;
    public GameObject litrosGO;
    public GameObject origenGO;
    public GameObject maduracionGO;
    public GameObject terrunoGO;
    public GameObject conocimientoGO;
    public GameObject molidoGO;
    public GameObject aguaGO;
    public GameObject fermentacionGO;
    public GameObject destiladorGO;
    public GameObject destilacionesGO;
    public GameObject ajusteGO;
    public GameObject abocadoGO;
    public GameObject acercadeDescGO;
    public GameObject MoreInfo;
    public GameObject AboutInfo;
    public UsuarioID us = new UsuarioID ();	
	public void LoginUserID(){

        if (PlayerPrefs.HasKey("UserID")) {
            string IDuser = PlayerPrefs.GetString("UserID");
            UserID(IDuser);
		} else {
			string json = JsonUtility.ToJson(us);			// Crear json con valores de la instancia declarados publicamente, para el cambio acceder a instancia y Realizar Cambios
			WSClient.instance.StartWebService("mezcal", json, SearchSuccess, (r) => MessageBox.instance.ShowMessage("Hubo un problema de conexión."));  // Obtner Ficha del Servidor

		}
	}

    void Start()
    {
        Perfil = GameObject.FindGameObjectWithTag("Monito");
        GetComponent<RectTransform>().offsetMax = Vector2.zero;
        GetComponent<RectTransform>().offsetMin = Vector2.zero;
        transform.localScale = new Vector3(1,1,1);
        RectTransform contentRect = GetComponent<RectTransform>();
        contentRect.anchoredPosition = new Vector2(contentRect.rect.width, 0);
    }

    public void Hide()
    {
        if (OnClose != null) OnClose();

        if (gameObject.transform.root.name == "PerfilUsuario"&&transform.root.childCount==2)
        {
            
            if (gameObject.transform.root.name == "PerfilUsuario")
            {
                gameObject.transform.root.GetComponent<PerfilUsuario>().menuDespegable.SetActive(true);
            }

        }
        StopAllCoroutines();
        GetComponent<SideAnimation>().OnAnimationEnded += SelfDestroy;
        GetComponent<SideAnimation>().Hide();
    }

    public void SelfDestroy()
    {
        GetComponent<SideAnimation>().OnAnimationEnded -= SelfDestroy;
        GameObject.Destroy(gameObject);
    }

	void UserID(string result)
    {
		if (result != null) {
			us.UserID = result;
			//us.UserID = "91a40f8b3a09fb41a6f9c52afe8ba8881e9f18d6";
            Mapa.Inicio(us.ProductID);
            string json = JsonUtility.ToJson(us);           // Crear json con valores de la instancia declarados publicamente, para el cambio acceder a instancia y Realizar Cambios

            WSClient.instance.StartWebService("mezcal", json, SearchSuccess, (r) => MessageBox.instance.ShowMessage("Hubo un problema de conexión."));  // Obtner Ficha del Servidor

        }
	
	}// Instacia global para Todos los Valores de FichaMezcal ( Usuario y IdProducto

	public void Iniciar(string ProductoID)
    {
        us.ProductID = ProductoID;
        Panel.transform.position = new Vector3(Panel.transform.position.x,0,0);
        IniciarFicha();
  
    }
    public void IniciarAnim()
    {
        //  GetComponent<SideAnimation>().OnAnimationEnded += IniciarFicha;
        GetComponent<SideAnimation>().Show();
        ResetSize();
        //Canvas.ForceUpdateCanvasaes()
        //LayoutRebuilder.ForceRebuildLayoutImmediate(AcercaDeGO.GetComponent<RectTransform>());
    }

    public void ResetSize()
    {
        LayoutRebuilder.ForceRebuildLayoutImmediate(AboutInfo.GetComponent<RectTransform>());
        LayoutRebuilder.ForceRebuildLayoutImmediate(MoreInfo.GetComponent<RectTransform>());
        LayoutRebuilder.ForceRebuildLayoutImmediate(AcercaDeGO.GetComponent<RectTransform>());
    }

    private void IniciarFicha()
    {
       
        Perfil = GameObject.Find("Monito");
        LoginUserID();

    }
    public void Recargar()
    {
        string json = JsonUtility.ToJson(us);
        WSClient.instance.StartWebService("mezcal", json, ReSuccess, (r) => MessageBox.instance.ShowMessage("Hubo un problema de conexión."));
    }
    public void ReSuccess(WSResponse response)
    {
        string result = response.responseTxt;
        relacionados.transform.GetComponent<ProductosRelacionados>().Relacionados(result);
        fm = JsonUtility.FromJson<FichaMezcal>(result);
        Opinions.transform.GetComponent<FichaProducto>().Inicio();
        estadis();
    }
    public void SearchSuccess(WSResponse response)
    {
        string result = response.responseTxt;
        fm = JsonUtility.FromJson<FichaMezcal>(result);
        Foto.LoadImage(fm.Foto);

        Opinions.transform.GetComponent<FichaProducto> ().Inicio ();
		relacionados.transform.GetComponent<ProductosRelacionados> ().Relacionados(result);
       
        
		Titulo.text = fm.Nombre;						// Nombre del Mezcal
		Titulo2.text = fm.Nombre;		// Etiqueta Superior de Titulo
		foreach (Transform item in Calificacion.transform) {	// Estrellas del Mezcal Cal, Promedio
			int k = Int32.Parse (item.name);					
			if (k <= fm.CalificacionPromedio) {
				item.GetComponent<Image> ().sprite = Magueylleno; // asignar Sprite lleno
			}
		}
		Agave.text = fm.Maguey+ " / "+fm.Agave;                                  // Agave del Mezcal
        if(fm.Etiquetas.Length > 0)
            Agave.name = fm.Etiquetas[0].tag;
        else
            Agave.name = "";
        URLTienda =fm.TiendaURL;
		Precio.text = string.Format("€{0}",fm.Precio);      // Precio en Formato decimal y simbolo Euro

        if (string.IsNullOrEmpty(fm.Agave))
            AcercaAgaveGO.SetActive(false);
        else
            AcercaAgave.text=fm.Agave;                          // Agave en Acerca De

        if (string.IsNullOrEmpty(fm.Maguey))
            AcercaMagueyGO.SetActive(false);
        else
            AcercaMaguey.text = fm.Maguey;

        if (string.IsNullOrEmpty(fm.Estado))
            AcercaEstadoGO.SetActive(false);
        else
            AcercaEstado.text = fm.Estado;

        if (string.IsNullOrEmpty(fm.Region))
            AcercaRegionGO.SetActive(false);
        else
            AcercaRegion.text = fm.Region;

        if (string.IsNullOrEmpty(fm.Maestro))
            AcercaMaestroGO.SetActive(false);
        else
            AcercaMaestro.text = fm.Maestro;                    // Maestro en Acerc a De

        if (string.IsNullOrEmpty(fm.Alcohol))
            AcercaGradosGO.SetActive(false);
        else
                AcercaGrados.text = fm.Alcohol+"°";                     // Alcohol en Acerca De

        if (string.IsNullOrEmpty(fm.AcercaDe))
            acercadeDescGO.SetActive(false);
        else
            AcercaDe.text = fm.AcercaDe;                        // Descripcion de Acerca de

        if (fm.Year == 0)
            anoGO.SetActive(false);
        else
            ano.text = fm.Year.ToString();                      // Año en Acerca de

        if (string.IsNullOrEmpty(fm.Molido))
            molidoGO.SetActive(false);
        else
            molido.text = fm.Molido;                            // Molido en ficha Desplegable

        if (string.IsNullOrEmpty(fm.Origen))
            origenGO.SetActive(false);
        else
            origen.text = fm.Origen;                                // origen en Ficha deplgable

        if (string.IsNullOrEmpty(fm.Maduracion))
            maduracionGO.SetActive(false);
        else
            maduracion.text = fm.Maduracion +" Años";     // maduracion en Ficha desplegable 

        if (string.IsNullOrEmpty(fm.Terruno))
            terrunoGO.SetActive(false);
        else
            terruno.text = fm.Terruno;                              // terruño en Ficha desplegable 

        if (string.IsNullOrEmpty(fm.Cocimiento))
            conocimientoGO.SetActive(false);
        else
            conocimiento.text = fm.Cocimiento;                      // cocimiento en ficha Desplegabe

        if (string.IsNullOrEmpty(fm.Molido))
            molidoGO.SetActive(false);
        else
            molido.text = fm.Molido;                                // molido en ficha desplegable 

        if (string.IsNullOrEmpty(fm.Fermentacion))
            fermentacionGO.SetActive(false);
        else
            fermentacion.text = fm.Fermentacion;                    // fementacion en ficha desplegable 

        if (string.IsNullOrEmpty(fm.Destilador))
            destiladorGO.SetActive(false);
        else
            destilador.text = fm.Destilador;

        if (string.IsNullOrEmpty(fm.Destilaciones))
            destilacionesGO.SetActive(false);
        else
            destilaciones.text = fm.Destilaciones;

        if (string.IsNullOrEmpty(fm.Ajuste))
            ajusteGO.SetActive(false);
        else
            ajuste.text = fm.Ajuste;

        if (string.IsNullOrEmpty(fm.Abocado))
            abocadoGO.SetActive(false);
        else
            abocado.text = fm.Abocado;

        if (string.IsNullOrEmpty(fm.Agua))
            aguaGO.SetActive(false);
        else
        agua.text = fm.Agua;

        if (fm.Litros == 0)
            litrosGO.SetActive(false);
        else
            litros.text = fm.Litros.ToString();

        if (fm.Favorito)
        {
            Favoritos.transform.GetComponent<Image>().sprite = Corazonlleno;
        }
        else
        {
            Favoritos.transform.GetComponent<Image>().sprite = CorazonVacio;
        }
        foreach (Transform item in Calificar.transform)
        {
            int k = Int32.Parse(item.name);                     // Puntacion del Usuario 
            if (k <= fm.CalificacionUsuario)
            {
                item.GetComponent<Image>().sprite = Magueylleno;
            }
        }

        estadis();

        //Apagar Contetsize


        LayoutRebuilder.ForceRebuildLayoutImmediate(AcercaDeGO.GetComponent<RectTransform>());
        gameObject.GetComponent<FichaMezcales>().IniciarAnim();


    }
    public void estadis()
    {
        EstadisticaCant.text = fm.NumeroDeEstadisticas + " degustaciones";
        estadisticas.Add("Alcohol," + fm.MapaGeneral.MapaDegustacion.Alcohol);
        estadisticas.Add("Caramelo," + fm.MapaGeneral.MapaDegustacion.Caramelo);
        estadisticas.Add("Citrico," + fm.MapaGeneral.MapaDegustacion.Citrico);
        estadisticas.Add("Especias," + fm.MapaGeneral.MapaDegustacion.Especias);
        estadisticas.Add("Fermentos," + fm.MapaGeneral.MapaDegustacion.Fermentos);
        estadisticas.Add("Floral," + fm.MapaGeneral.MapaDegustacion.Floral);              // Lista de los 13 degustaciones 
        estadisticas.Add("Frutal," + fm.MapaGeneral.MapaDegustacion.Frutal);
        estadisticas.Add("Hierba," + fm.MapaGeneral.MapaDegustacion.Hierba);
        estadisticas.Add("Humedad," + fm.MapaGeneral.MapaDegustacion.Humedad);
        estadisticas.Add("Humo," + fm.MapaGeneral.MapaDegustacion.Humo);
        estadisticas.Add("Madera," + fm.MapaGeneral.MapaDegustacion.Madera);
        estadisticas.Add("Mineral," + fm.MapaGeneral.MapaDegustacion.Mineral);
        estadisticas.Add("Tierra," + fm.MapaGeneral.MapaDegustacion.Tierra);
        estadisticas2.Add("Aspero," + fm.MapaGeneral.MapaSensacion.Aspero);
        estadisticas2.Add("Calor," + fm.MapaGeneral.MapaSensacion.Calor);
        estadisticas2.Add("Fresco," + fm.MapaGeneral.MapaSensacion.Fresco);
        estadisticas2.Add("Picante," + fm.MapaGeneral.MapaSensacion.Picante);
        estadisticas2.Add("Untuoso," + fm.MapaGeneral.MapaSensacion.Untuoso);

        for (int i = 0; i < estadisticas.Count; i++)
        {
            char delimiter = ',';
            string[] substrings = estadisticas[i].Split(delimiter);
            for (int j = 0; j < estadisticas.Count; j++)
            {
                string[] substring2 = estadisticas[j].Split(delimiter);
                double k = Double.Parse(substrings[1]);
                double m = Double.Parse(substring2[1]);
                if (k > m)
                {
                    string temp = estadisticas[i];
                    estadisticas[i] = estadisticas[j];
                    estadisticas[j] = temp;
                }
            }
        }
        for (int i = 0; i < estadisticas2.Count; i++)
        {
            char delimiter = ',';
            string[] substrings = estadisticas2[i].Split(delimiter);
            for (int j = 0; j < estadisticas2.Count; j++)
            {
                string[] substring2 = estadisticas2[j].Split(delimiter);
                double k = Double.Parse(substrings[1]);
                double m = Double.Parse(substring2[1]);
                if (k > m)
                {
                    string temp = estadisticas2[i];
                    estadisticas2[i] = estadisticas2[j];
                    estadisticas2[j] = temp;
                }
            }
        }
        int a = 0;
        foreach (Transform pun in Categoria1.transform)
        {

            char delimiter = ',';
            string[] substrings = estadisticas[a].Split(delimiter);
            int k = (int)Double.Parse(substrings[1]);
            pun.GetComponent<Text>().text = substrings[0];
            pun.GetChild(0).GetComponent<Text>().text = substrings[1];
            pun.GetChild(0).transform.GetChild(0).GetComponent<RectTransform>().sizeDelta = new Vector2((k * 25) * 3, 30);
            a++;
        }

        int b = 0;
        foreach (Transform pun in Categoria2.transform)
        {

            char delimiter = ',';
            string[] substrings = estadisticas2[b].Split(delimiter);
            int k = (int)Double.Parse(substrings[1]);
            pun.GetComponent<Text>().text = substrings[0];                                                          // Graficas de Estadisticas
            pun.GetChild(0).GetComponent<Text>().text = substrings[1] + "%";
            pun.GetChild(0).transform.GetChild(0).GetComponent<RectTransform>().sizeDelta = new Vector2((k) * 3, 30);
            b++;
        }
    }

	// Boton Corazon , Agrega a lista de Favoritos este Mezcal en el Servidor
	public void AddFavoritos()
    {
		if (PlayerPrefs.HasKey("UserID"))
        {
			if (Favoritos.GetComponent<Image> ().sprite == CorazonVacio) {			// Si coniene el Corazon Vacio 
				string json = JsonUtility.ToJson (us);															// Mandar Json para añadir este mezcal a ala lista
				WSClient.instance.StartWebService ("user/favorite/add", json, agregar, (r) => MessageBox.instance.ShowMessage("Hubo un problema de conexión."));
			} else {																								// si es diferente entonces Quitar este Mezcal de la lista de Favoritos
				string json = JsonUtility.ToJson (us);
				WSClient.instance.StartWebService ("user/favorite/remove", json, quitar, (r) => MessageBox.instance.ShowMessage("Hubo un problema de conexión."));
			}
		} else {
            GameObject.FindObjectOfType<LoginPerfil>().Iniciar ();
		}
	}
	// Metodop Para verificar Resultados de Cambio
	void agregar(WSResponse response){
        string result = response.responseTxt;
		CulturaMezcalJsons.Exito ex = new CulturaMezcalJsons.Exito ();
		ex=JsonUtility.FromJson<CulturaMezcalJsons.Exito>(result);			// si la respuesta del Js es True 
		if (ex.Success) {
			Favoritos.GetComponent<Image> ().sprite = Corazonlleno;			// proceder a Cambiar imagen de Corazon LLeno
		}
	}
	// Metodop Para verificar Quitar de lista de Favoritos
	void quitar(WSResponse response){
        string result = response.responseTxt;
		CulturaMezcalJsons.Exito ex = new CulturaMezcalJsons.Exito ();		// si la Respuesta es True
		ex=JsonUtility.FromJson<CulturaMezcalJsons.Exito>(result);			
		if (ex.Success) {
			Favoritos.GetComponent<Image> ().sprite = CorazonVacio;			// Cambiar a Imagen de Corazon Vacio
		}
	}

	public void Comprar()
    {
        //Application.OpenURL(URLTienda);
        //FindObjectOfType<WebViewController>().GetComponent<WebViewController>().OpenLink(URLTienda);
        GameObject auzx = Instantiate(WebViewPrefab);
        auzx.GetComponent<WebViewController>().OpenLink(URLTienda);
        //WebViewController.instance.OpenLink("http://pre-culturamezcal.ddns.net/compra.html");
    }

   
    public void OnShareBtn() 
    {
        FBController.instance.Share(fm.Nombre, fm.TiendaURL);
    }
}
