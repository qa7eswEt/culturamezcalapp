﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
#if UNITY_ANDROID
using DeadMosquito.AndroidGoodies.Internal;
using DeadMosquito.AndroidGoodies;
using JetBrains.Annotations;
#endif 
#if UNITY_IOS
using DeadMosquito.IosGoodies;
using DeadMosquito;
#endif
public class PhotoPick : MonoBehaviour
{
    public UnityEngine.UI.RawImage testImage;

    public enum PhotoPickSource {Camera, Gallery};

    public delegate void PhotoPickCallback(Texture2D photo);
    private static PhotoPickCallback currentCallback;

	// Use this for initialization
	void Start ()
    {
		
	}

    public void GalleryTest()
    {
        PickPhoto(PhotoPickSource.Gallery, TestCallback);
    }

    public void PhotoTest()
    {
        PickPhoto(PhotoPickSource.Camera, TestCallback);
    }

    public void TestCallback(Texture2D photo)
    {
        testImage.texture = photo;
    }
	
    public static void PickPhoto(PhotoPickSource source, PhotoPickCallback photoPickCallback)
    {
        currentCallback = photoPickCallback;
        switch(source)
        {
            case PhotoPickSource.Camera:
#if UNITY_ANDROID
                if (PermissionsManager.IsPermissionGranted(AN_Permission.CAMERA) && PermissionsManager.IsPermissionGranted(AN_Permission.READ_EXTERNAL_STORAGE))
                {
                    Debug.Log("WRITE_EXTERNAL_STORAGE Granted");
                    AndroidCamera.Instance.OnImagePicked += AndroidOnImagePicked;
                    AndroidCamera.Instance.GetImageFromCamera();
                }
                else
                {
                    Debug.Log("WRITE_EXTERNAL_STORAGE Denied");
                    PermissionsManager.ActionPermissionsRequestCompleted += PermissionCallbackCamera;
                    PermissionsManager.Instance.RequestPermissions(AN_Permission.CAMERA, AN_Permission.READ_EXTERNAL_STORAGE);
                }
#elif UNITY_IOS
                IOSCamera.OnImagePicked += IOSOnImagePicked;
                IOSCamera.Instance.PickImage(ISN_ImageSource.Camera);
#endif
                break;
            case PhotoPickSource.Gallery:
#if UNITY_ANDROID
                AndroidCamera.Instance.OnImagePicked += AndroidOnImagePicked;
                AndroidCamera.Instance.GetImageFromGallery();

#elif UNITY_IOS
                IOSCamera.OnImagePicked += IOSOnImagePicked;
                IOSCamera.Instance.PickImage(ISN_ImageSource.Album);
#endif
                break;
        }
    }

    static void PermissionCallbackCamera(AN_GrantPermissionsResult res)
    {
        Debug.Log("HandleActionPermissionsRequestCompleted");
        PermissionsManager.ActionPermissionsRequestCompleted -= PermissionCallbackCamera;
        if (res.IsSucceeded)
        {
            AndroidCamera.Instance.OnImagePicked += AndroidOnImagePicked;
            AndroidCamera.Instance.GetImageFromCamera();

            foreach (KeyValuePair<AN_Permission, AN_PermissionState> pair in res.RequestedPermissionsState)
            {
                Debug.Log(System.Enum.GetName(typeof(AN_Permission), pair.Key) + " / " + pair.Value.ToString());
            }
        }
        else
        {
            Debug.Log("Permission not granted");
        }

    }

    private static void AndroidOnImagePicked(AndroidImagePickResult result)
    {
        if(result.IsSucceeded)
        {
            currentCallback(result.Image);
        }
        AndroidCamera.Instance.OnImagePicked -= AndroidOnImagePicked;
        WSClient.instance.ShowLoadingScreen(false);
    }

    private static void IOSOnImagePicked(IOSImagePickResult result)
    {
        if (result.IsSucceeded)
        {
            currentCallback(result.Image);
        }
        IOSCamera.OnImagePicked -= IOSOnImagePicked;
        WSClient.instance.ShowLoadingScreen(false);
    }

    private static void ImagePicked()
    {
#if UNITY_ANDROID

        // Whether to generate thumbnails
        const bool shouldGenerateThumbnails = false;

        // if image is larger it will be downscaled to the max size proportionally
        const ImageResultSize imageResultSize = ImageResultSize.Max1024;
        AGCamera.TakePhoto(
                selectedImage =>
                {
                    // Load received image into Texture2D
                    var imageTexture2D = selectedImage.LoadTexture2D();
                    //var msg = string.Format("{0} was taken from camera with size {1}x{2}",
                    //    selectedImage.DisplayName, imageTexture2D.width, imageTexture2D.height);
                    //AGUIMisc.ShowToast(msg);
                    //Debug.Log(msg);
                    currentCallback(imageTexture2D);

                    // Clean up
                    Resources.UnloadUnusedAssets();
                },
                error => { ShowErrorCamera(); });
#elif UNITY_IOS
#if !DISABLE_IMAGE_PICKERS
        const bool allowEditing = true;
        const float compressionQuality = 0.8f;
        const IGImagePicker.CameraType cameraType = IGImagePicker.CameraType.Front;
        const IGImagePicker.CameraFlashMode flashMode = IGImagePicker.CameraFlashMode.On;

        IGImagePicker.PickImageFromCamera(imageTexture2D =>
        {
            Debug.Log("Successfully picked image from camera");
            currentCallback(imageTexture2D);
            // IMPORTANT! Call this method to clean memory if you are picking and discarding images
            Resources.UnloadUnusedAssets();
        },
            () => { ShowErrorCamera(); },
            compressionQuality,
            allowEditing, cameraType, flashMode);
#else
			ExampleUtil.LogFeatureDisabled();
#endif
#endif
    }

#if UNITY_ANDROID
    [UsedImplicitly]
#endif
    public static void TakePhotoFromCamera(PhotoPickCallback photoPickCallback) 
    {
        currentCallback = photoPickCallback;

#if UNITY_ANDROID
        AGPermissions.ExecuteIfHasPermission(AGPermissions.CAMERA, ImagePicked,
                ShowErrorCamera);
#elif UNITY_IOS
        ImagePicked();
#endif
    }

    public static void ShowErrorCamera() 
    {
        WSClient.instance.ShowLoadingScreen(false);
        MessageBox.instance.ShowMessage("Error de cámara.");

    }

    public static void TakeFromGAllery(PhotoPickCallback photoPickCallback)
    {
        if (!NativeGallery.IsMediaPickerBusy())
        {
            currentCallback = photoPickCallback;
            NativeGallery.GetImageFromGallery((path) =>
            {
                Debug.Log("Image path: " + path);
                if (path != null)
                {
                    // Create Texture from selected image
                    Texture2D texture = NativeGallery.LoadImageAtPath(path, 512);
                    if (texture == null)
                    {
                        Debug.Log("Couldn't load texture from " + path);
                        return;
                    }

                    currentCallback(texture);
                }
            }, "Selecciona una imagen png", "image/png");
        }
    }


    public static void TakeImage(PhotoPickCallback photoPickCallback)
    {
        currentCallback = photoPickCallback;
        int maxSize = 512;
        if (!NativeCamera.IsCameraBusy())
        {
            NativeCamera.TakePicture((path) =>
            {
                Debug.Log("Image path: " + path);
                if (path != null)
                {
                    // Create a Texture2D from the captured image

                    MailSenderCtrl.mailSender.ImagePath = path;


                    Texture2D texture = NativeCamera.LoadImageAtPath(path, maxSize, false);
                    if (texture == null)
                    {
                        ShowErrorCamera();
                        return;
                    }

                    currentCallback(texture);
                }
                else
                {
                    WSClient.instance.loadingScreen.SetActive( false);
                }
            }, maxSize);
        }
    }
}
