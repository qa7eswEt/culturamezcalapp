﻿using UnityEngine;
using UnityEngine.UI;

public class SkipLabel : MonoBehaviour
{
    [SerializeField] Text _targetLabel;
    [SerializeField] SimplePageScroll _pageScroll;
    [SerializeField] string _skipText;
    [SerializeField] string _continueText;

    void OnEnable()
    {
        _pageScroll.OnPageChanged.AddListener(UpdateStatus);
    }

    void OnDisable()
    {
        _pageScroll.OnPageChanged.RemoveListener(UpdateStatus);
    }

    void UpdateStatus(int currentPage)
    {
        _targetLabel.text = currentPage < _pageScroll.totalPages - 1 ? _skipText : _continueText;
    }
}
