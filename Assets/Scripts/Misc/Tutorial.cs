﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tutorial : MonoBehaviour
{
    public string tutorialKey;
    public GameObject barra;
    public UnityEngine.Events.UnityEvent OnTutorialDone;
    public bool activateOnEnabled = true;

	// Use this for initialization
	void OnEnable ()
    {
        if (activateOnEnabled) EnableTutorial();
	}

    public void EnableTutorial()
    {
        if (PlayerPrefs.HasKey(tutorialKey))
        {
            //barra.SetActive(true);
            BarraDeNavegacion.instance.gameObject.SetActive(true);
            OnTutorialDone.Invoke();
        }
        else
        {
            PlayerPrefs.SetInt(tutorialKey, 1);
            //barra.SetActive(false);
            BarraDeNavegacion.instance.gameObject.SetActive(false);
        }
    }

    public void CloseTutorial()
    {
        BarraDeNavegacion.instance.gameObject.SetActive(true);
        OnTutorialDone.Invoke();
    }
}
