﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RectContentFitter : MonoBehaviour
{
    public ScrollRect scroll;

    private int lastChildCount = 0;
    	
	void Start ()
    {

    }
	
	void FixedUpdate ()
    {
        if(lastChildCount < transform.childCount)
        {
            ResizeHeight();
        }
    }

    public void ResizeHeight()
    {
        float maxY = 0;
        foreach (Transform child in transform)
        {
            RectTransform rectTransform = child.GetComponent<RectTransform>();
            if (rectTransform.offsetMax.y - rectTransform.rect.height < maxY) maxY = rectTransform.offsetMax.y - rectTransform.rect.height;
        }
        float height = GetComponent<RectTransform>().rect.height + maxY;
        GetComponent<RectTransform>().offsetMin = new Vector2(GetComponent<RectTransform>().offsetMin.x, height);
        scroll.content = GetComponent<RectTransform>();
    }
}
