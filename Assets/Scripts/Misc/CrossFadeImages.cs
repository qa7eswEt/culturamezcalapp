﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CrossFadeImages : MonoBehaviour
{
    public CanvasGroup[] fadeImages;
    public CanvasGroup[] nextScreens;
    public float fadeSpeed;
    public float fadeDuration;
    public float imageDuration;
    public bool turnOffOnInput;
    public GameObject particulasDeHada;

    public UnityEngine.Events.UnityEvent OnSplashFinished;

    private static bool hasFinishedAnimation = false;

    // Use this for initialization
    void Start ()
    {
        if(!hasFinishedAnimation)
        {
            StartCoroutine(AnimationFlow(fadeImages));
        }
        else
        {
            OnSplashFinished.Invoke();
        }
	}

    IEnumerator AnimationFlow(CanvasGroup[] images)
    {
        foreach(CanvasGroup image in images)
        {
            yield return StartCoroutine(FadeIn(image, fadeSpeed));
            if(image.name == "Splash 2")
            {
                image.GetComponentInChildren<Animator>().Play("AnimacionHada");
                particulasDeHada.SetActive(true);
            }
            yield return new WaitForSeconds(imageDuration);
            yield return StartCoroutine(FadeOut(image, fadeSpeed));
        }
        foreach(CanvasGroup finishScren in nextScreens)
        {
            StartCoroutine(FadeIn(finishScren, fadeSpeed));
        }
        hasFinishedAnimation = true;
    }

    IEnumerator FadeOut(CanvasGroup canvasGroup, float speed)
    {
        while (canvasGroup.alpha > 0)
        {
            canvasGroup.alpha -= Time.deltaTime * speed;
            yield return null;
        }
    }

    IEnumerator FadeIn(CanvasGroup canvasGroup, float speed)
    {
        while (canvasGroup.alpha < 1)
        {
            canvasGroup.alpha += Time.deltaTime * speed;
            yield return null;
        }
    }

    void Update()
    {
        if(turnOffOnInput && Input.anyKeyDown)
        {
            OnSplashFinished.Invoke();
            hasFinishedAnimation = true;
        }
    }
}
