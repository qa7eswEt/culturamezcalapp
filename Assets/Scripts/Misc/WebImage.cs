﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class WebImage : MonoBehaviour
{
    public Color backgroundColor;
    public RawImage rawImage;
    public GameObject spinner;
    public bool HasImage;
	// Use this for initialization
	void Start ()
    {
        Texture2D texture = new Texture2D(2, 2, TextureFormat.RGB24, false);
        Color[] colors = { backgroundColor, backgroundColor, backgroundColor, backgroundColor };
        texture.SetPixels(colors);
        rawImage.texture = texture;
        HasImage = false;
	}

    public void LoadImage(string url)
    {
        //url = url.Replace("culturamezcal.ddns.net", "pre-culturamezcal.ddns.net");
        StartCoroutine(DownloadImage(url));
    }

    IEnumerator DownloadImage(string url)
    {
        if (!HasImage) 
        {
            spinner.SetActive(true);
            //UnityWebRequestTexture
            using (UnityWebRequest www = UnityWebRequestTexture.GetTexture(url))
            {
                //Debug.Log("==== Detail mezcal Loaded: " + url);
                //www.timeout = 10;
                yield return www.SendWebRequest();

                //if (www.timeout == 0)
                  //  Debug.Log("finished");

                if (www.isNetworkError || www.isHttpError)
                {
                    Debug.Log(www.error);

                    StartCoroutine(DownloadImage(url));
                }
                else
                {
                    
                    rawImage.texture = ((DownloadHandlerTexture)www.downloadHandler).texture;
                    rawImage.enabled = true;
                    //www.LoadImageIntoTexture((Texture2D)rawImage.texture);
                    //Debug.Log("==== Detail mezcal Loaded: " + url);
                    spinner.SetActive(false);
                    HasImage = true;
                }
            }
            

            /*Debug.Log("==== Detail mezcal: "+ url);
            WWW www = new WWW(url);
            Debug.Log("==== Detail mezcal Loaded before: " + url);
            yield return www;
            
            if (string.IsNullOrEmpty(www.error))
            {
                rawImage.enabled = true;
                www.LoadImageIntoTexture((Texture2D)rawImage.texture);
                Debug.Log("==== Detail mezcal Loaded: " + url);
                spinner.SetActive(false);
                HasImage = true;
            }
            else
            {
                Debug.Log("==== Detail mezcal Loaded: " + www.error);
                StartCoroutine(DownloadImage(url));
            }*/
        }
    }

    void OnDestroy()
    {
        Destroy(rawImage.texture);
    }

}
