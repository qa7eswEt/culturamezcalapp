﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class SimplePageScroll : MonoBehaviour, IEndDragHandler, IBeginDragHandler
{
    public class PageChangedEvent : UnityEngine.Events.UnityEvent<int> {}
    public PageChangedEvent OnPageChanged = new PageChangedEvent();
    /// <summary>
    /// Canvas que contiene el scroll de paginas
    /// </summary>
    public Canvas rootCanvas;
    /// <summary>
    /// RectTransform que contiene a contentRect y adapta su tamaño, se utiliza para hacer el scroll
    /// </summary>
    public RectTransform dragRect;
    /// <summary>
    /// RectTransform que contiene a todas las paginas.
    /// </summary>
    public RectTransform contentRect;
    /// <summary>
    /// Cantidad de espacio entre cada pagina.
    /// </summary>
    public float paddingX;
    /// <summary>
    /// Velocidad a la que hace el snap (va de 0 a 1)
    /// </summary>
    public float snapLerpSpeed;
    /// <summary>
    /// La velocidad minima para detectar el drag como swipe, en porcentaje de ancho de pagina recorrido por segundo.
    /// </summary>
    public float minSwipeSpeed;
    //[HideInInspector]
    public int totalPages = 7;
    //[HideInInspector]
    private int currentPage = 0;

    private ScrollRect scrollRect;
    private Vector2 startDragPosition;
    private float startDragTime = 0;
    private bool animating = false;

	// Use this for initialization
	void Start ()
    {
        scrollRect = GetComponent<ScrollRect>();
        totalPages = contentRect.childCount;
        ResizeContent(contentRect, dragRect);
    }

    /// <summary>
    /// Agrega una pagina al scroll
    /// </summary>
    /// <param name="newPage"></param>
    /// <param name="pagesRect"></param>
    private void AddPage(RectTransform newPage, RectTransform pagesRect)
    {
        newPage.SetParent(pagesRect.transform);
        ResizeContent(pagesRect, dragRect);
        totalPages++;
    }

    /// <summary>
    /// Acomoda las paginas horizontalmente y adapta el tamaño del rectransform que contiene todas las paginas
    /// </summary>
    /// <param name="pagesRect"></param>
    /// <param name="contentScrollRect"></param>
    private void ResizeContent(RectTransform pagesRect, RectTransform contentScrollRect)
    {
        pagesRect.SetParent(rootCanvas.transform);
        float totalSize = 0;
        for(int i = 0; i < pagesRect.childCount; i++)
        {
            pagesRect.GetChild(i).transform.localPosition = new Vector3(totalSize, 0, 0);
            if (i < pagesRect.childCount - 1) totalSize += pagesRect.GetChild(i).GetComponent<RectTransform>().rect.width + paddingX;
        }
        contentScrollRect.sizeDelta = new Vector3(totalSize, contentScrollRect.sizeDelta.y);
        pagesRect.SetParent(contentScrollRect.transform);
    }

    /// <summary>
    /// Evento de cuando inicia el drag
    /// </summary>
    /// <param name="data"></param>
    public void OnBeginDrag(PointerEventData data)
    {
        StopCoroutine("MoveToSnap");
        startDragPosition = data.position;
        startDragTime = Time.time;
    }

    /// <summary>
    /// Evento cuando termina el drag
    /// </summary>
    /// <param name="data"></param>
    public void OnEndDrag(PointerEventData data)
    {
        float pageWidth = scrollRect.GetComponent<RectTransform>().rect.width;
        float distanceSwiped = startDragPosition.x - data.position.x;
        float dragVelocity = (distanceSwiped/pageWidth) / (Time.time - startDragTime);
        float increments = 1f / (totalPages - 1);
        if (Mathf.Abs(dragVelocity) >= minSwipeSpeed)
        {
            int targetPage = currentPage + (int)Mathf.Sign(dragVelocity);
            if(targetPage >= 0 && targetPage < totalPages)
            {
                StopAllCoroutines();
                ChangeCurrentPage(targetPage);
                StartCoroutine(MoveToSnap(targetPage * increments, targetPage));
                return;
            }
        }
        int pageToSnap = Mathf.RoundToInt( scrollRect.normalizedPosition.x / increments );
        StopAllCoroutines();
        ChangeCurrentPage(pageToSnap);
        StartCoroutine(MoveToSnap(pageToSnap*increments, pageToSnap));
    }

    /// <summary>
    /// Corutina que mueve el scroll hasta hacer snap con la pagina objetivo.
    /// </summary>
    /// <param name="targetPosition"></param>
    /// <param name="targetPage"></param>
    /// <returns></returns>
    IEnumerator MoveToSnap(float targetPosition, int targetPage)
    {
        float fps = 1f / 30f;
        float direction = targetPosition - scrollRect.normalizedPosition.x >= 0 ? 1 : -1;
        float finalSpeed = snapLerpSpeed * direction;
        while (Mathf.Abs(targetPosition - scrollRect.normalizedPosition.x) > snapLerpSpeed)
        {
            scrollRect.normalizedPosition += new Vector2(finalSpeed, 0);
            yield return new WaitForSeconds(fps);
        }
        scrollRect.normalizedPosition = new Vector2(targetPosition, scrollRect.normalizedPosition.y);
    }

    private void ChangeCurrentPage(int page)
    {
        currentPage = page;
        OnPageChanged?.Invoke(currentPage);
    }

    public void ScrollMoved(Vector2 v)
    {
        //print(v);
    }
}
