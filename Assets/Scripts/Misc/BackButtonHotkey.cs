﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class BackButtonHotkey : MonoBehaviour
{
    [SerializeField] Button _targetButton;
    RenderMode _renderMode = RenderMode.ScreenSpaceOverlay;
    EventSystem _eventSystem;

    bool _clickQueued = false;

    void Awake()
    {
        _eventSystem = FindObjectOfType<EventSystem>();
        if (_targetButton == null) _targetButton = GetComponent<Button>();
        _renderMode = _targetButton.GetComponentInParent<Canvas>().renderMode;
    }

    void Update()
    {
        //Check if the left Mouse button is clicked
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            // Debug.Log($"Checking back> ({_targetButton.gameObject.name})");
            if (!_targetButton.enabled || !_targetButton.interactable || !(_targetButton.image?.raycastTarget ?? true))
                return;

            PointerEventData pointerEventData = new PointerEventData(_eventSystem);
            switch (_renderMode)
            {
                case RenderMode.ScreenSpaceOverlay: pointerEventData.position = transform.position; break;
                case RenderMode.ScreenSpaceCamera: pointerEventData.position = Camera.main.WorldToScreenPoint(transform.position); break;
            }

            List<RaycastResult> results = new List<RaycastResult>();
            _eventSystem.RaycastAll(pointerEventData, results);

            if (results.Count > 0 && (results[0].gameObject == _targetButton.gameObject || ( _targetButton.image != null  && results[0].gameObject == _targetButton.image.gameObject)))
            {
                // Debug.Log($"Back on ({_targetButton.gameObject.name})");
                _clickQueued = true;
            }

            // for (int i = 0, n = results.Count; i < n; ++i)
            //     Debug.Log($"Hit ({i}): {results[i].gameObject.name}");
        }
    }

    void LateUpdate()
    {
        if (_clickQueued)
        {
            _clickQueued = false;
            _targetButton.onClick.Invoke();
        }
    }
}
