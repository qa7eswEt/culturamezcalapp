﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ClickOnScreen : MonoBehaviour
{

    public UnityEvent OnClickedScreen;

    private float timeSinceLastTouch = 0;
    private Vector2 lastTouchPosition;

    private bool shouldExit = true;
    private bool firstTouch = true;

    private float timePressingDown = 0;

	// Use this for initialization
	void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (Input.touchCount == 1)
        {
            if(firstTouch)// && timePressingDown > 0.05f)
            {
                lastTouchPosition = Input.GetTouch(0).position;
                firstTouch = false;
            }
            else if(Vector2.Distance(lastTouchPosition, Input.GetTouch(0).position) / Screen.dpi >= 0.5f)
            {
                shouldExit = false;
            }
            //timePressingDown += Time.deltaTime;
        }
        else if(Input.touchCount >= 2)
        {
            shouldExit = false;
        }
        else if(Input.touchCount == 0)
        {
            if(!firstTouch)
            {
                if(shouldExit) OnClickedScreen.Invoke();
                shouldExit = true;
                firstTouch = true;
            }
            //timePressingDown = 0;
        }

    }
}
