﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextCullMask : MonoBehaviour
{
    [SerializeField] Mask _targetMask;

    RectTransform _rectTransform;
    Text _text;

    Vector3[] _vecBufferA = new Vector3[4];
    Vector3[] _vecBufferB = new Vector3[4];

    bool cullEnabled = false;

    void Awake()
    {
        _rectTransform = GetComponent<RectTransform>();
        _text = GetComponent<Text>();
    }

    void OnEnable()
    {
        if (!cullEnabled)
            StartCoroutine(DelayedContentSizeFitterDisable());
    }

    void LateUpdate()
    {
        if (cullEnabled)
        {
            bool cull = CheckCull();
            if (_text != null) _text.enabled = !cull;
        }
    }

    bool CheckCull()
    {
        _rectTransform.GetWorldCorners(_vecBufferA);
        _targetMask.rectTransform.GetWorldCorners(_vecBufferB);

        Vector2 aabbA = new Vector2(_vecBufferA[2].x - _vecBufferA[1].x, _vecBufferA[1].y - _vecBufferA[0].y) * 0.5f;
        Vector2 aabbB = new Vector2(_vecBufferB[2].x - _vecBufferB[1].x, _vecBufferB[1].y - _vecBufferB[0].y) * 0.5f;
        Vector2 posA = (Vector2)_vecBufferA[0] + aabbA;
        Vector2 posB = (Vector2)_vecBufferB[0] + aabbB;

        Vector2 delta = posB - posA;
        Vector2 sum = aabbA + aabbB;

        return !((Mathf.Abs(delta.x) < sum.x) && (Mathf.Abs(delta.y) < sum.y));
    }

    IEnumerator DelayedContentSizeFitterDisable()
    {
        yield return null;
        ContentSizeFitter contentSizeFitter;
        if (TryGetComponent<ContentSizeFitter>(out contentSizeFitter))
            contentSizeFitter.enabled = false;
        cullEnabled = true;
    }
}
