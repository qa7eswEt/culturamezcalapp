﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

[RequireComponent (typeof(EventSystem))]

public class DynamicPixelDragThreshold : MonoBehaviour
{
    private const float inchToCm = 2.54f;
    public float dragThresholdCM = 0.5f;

    void Awake()
    {
        GetComponent<EventSystem>().pixelDragThreshold = Mathf.RoundToInt(dragThresholdCM * Screen.dpi / inchToCm);
    }
}
