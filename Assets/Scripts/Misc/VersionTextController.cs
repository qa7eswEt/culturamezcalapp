﻿using UnityEngine;
using UnityEngine.UI;

public class VersionTextController : MonoBehaviour
{
    public Text target;

    void Awake()
    {
        target.text = $"v{Application.version}";
    }

}
