﻿using UnityEngine;
using UnityEngine.Events;

public class PlatformCheck : MonoBehaviour
{
    public UnityEvent _onIOS;
    public UnityEvent _onAndroid;

    void Awake()
    {
#if UNITY_ANDROID
    _onAndroid?.Invoke();
#endif

#if UNITY_IOS
    _onIOS?.Invoke();
#endif
    }
}
