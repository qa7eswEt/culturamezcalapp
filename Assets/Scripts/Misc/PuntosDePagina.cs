﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PuntosDePagina : MonoBehaviour
{
    public GameObject punto;
    public Sprite puntoPrendido;
    public Sprite puntoApagado;
    public SimplePageScroll pageScroll;

    public int numberOfPages;


    void Start()
    {
        SetNumberOfPages(numberOfPages);
        pageScroll.OnPageChanged.AddListener(OnPageChanged);
    }

	public void SetNumberOfPages(int numberOfPages)
    {
        this.numberOfPages = numberOfPages;
        for(int i = 0; i < numberOfPages -1; i++)
        {
            GameObject.Instantiate(punto, transform);
        }
        SetCurrentPage(0);
    }

    public void SetCurrentPage(int currentPage)
    {
        foreach(Transform child in transform)
        {
            child.GetComponent<Image>().sprite = puntoApagado;
        }
        transform.GetChild(currentPage).GetComponent<Image>().sprite = puntoPrendido;
    }

    private void OnPageChanged(int newPage)
    {
        SetCurrentPage(newPage);
    }

    void OnDestroy()
    {
        pageScroll.OnPageChanged.RemoveListener(OnPageChanged);
    }
}
