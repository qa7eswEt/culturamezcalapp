﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScreenSizeFitter : MonoBehaviour {

	// Use this for initialization
	void Awake ()
    {
        print("h:" + Screen.height);
        print("w:" + Screen.width);
        GetComponent<RectTransform>().sizeDelta = new Vector3(Screen.width, Screen.height);	
    }
}
