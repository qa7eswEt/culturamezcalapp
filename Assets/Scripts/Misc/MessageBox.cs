﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MessageBox : MonoBehaviour
{
    public static MessageBox instance;

    public delegate void MessageCallback(bool accepted);

    private MessageCallback currentCallback;

    public delegate void MessageCallbackN();

    private MessageCallbackN currentCallbackN;

    void Awake()
    {
        if(instance == null)
        {
            instance = this;
        }
        else
        {
            GameObject.Destroy(gameObject);
        }
    }

    public GameObject box;
    public Text textLabel;
    
    public void ShowMessage(string msg, MessageCallback callback = null)
    {
        box.SetActive(true);
        textLabel.text = msg.ToString();
        if (callback != null) currentCallback = callback;
        else currentCallback = null;
    }



    public void ShowMessage(string msg, int aux, MessageCallbackN callback = null)
    {
        box.SetActive(true);
        textLabel.text = msg.ToString();
        if (callback != null) callback.Invoke();
        else currentCallback = null;
    }

    public void HideMessage()
    {
        box.SetActive(false);
    }

    public void AcceptMessage()
    {
        HideMessage();
        if(currentCallback != null) currentCallback(true);
    }

	
}
