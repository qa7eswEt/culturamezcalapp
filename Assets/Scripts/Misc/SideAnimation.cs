﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SideAnimation : MonoBehaviour
{
    public delegate void AnimationEnded();
    public AnimationEnded OnAnimationEnded;

    private const float AnimationFrameWait = 0.01f;

    public void Show()
    {
        StartCoroutine(AnimateShow());
    }

    public void Hide()
    {
        StartCoroutine(AnimateHide());
    }

    public void FadeInCanvas()
    {
        StartCoroutine(AnimationFadeInCanvas());
    }

    public void FadeOutCanvas()
    {
        StartCoroutine(AnimationFadeOutCanvas());
    }

    public void FadeInImage()
    {
        GetComponent<Image>().enabled = true;
        StartCoroutine(AnimationFadeInImage(GetComponent<Image>()));
    }

    public void FadeOutImage()
    {
        StartCoroutine(AnimationFadeOutImage(GetComponent<Image>()));
    }

    private IEnumerator AnimateShow()
    {
        RectTransform contentRect = GetComponent<RectTransform>();
        float t = 0;
        while (t < 1f)
        {
            contentRect.anchoredPosition = new Vector2(Mathf.Lerp(contentRect.rect.width, 0, t), contentRect.anchoredPosition.y);
            t += 0.05f;
            yield return new WaitForSeconds(AnimationFrameWait);
        }
        contentRect.anchoredPosition = new Vector2(0, 0);
        if(OnAnimationEnded != null) OnAnimationEnded();
    }

    private IEnumerator AnimateHide()
    {
        RectTransform contentRect = GetComponent<RectTransform>();
        float t = 1;
        while (t > 0f)
        {
            contentRect.anchoredPosition = new Vector2(Mathf.Lerp(contentRect.rect.width, 0, t), contentRect.anchoredPosition.y);
            t -= 0.05f;
            yield return new WaitForSeconds(AnimationFrameWait);
        }
        contentRect.anchoredPosition = new Vector2(contentRect.rect.width, 0);
        gameObject.SetActive(false);
        if (OnAnimationEnded != null) OnAnimationEnded();
    }

    private IEnumerator AnimationFadeInCanvas()
    {
        CanvasRenderer[] renderers = GetComponentsInChildren<CanvasRenderer>();
        
        foreach (CanvasRenderer renderer in renderers)
        {
            renderer.SetAlpha(0);
        }

        float t = 0;
        while (t < 1f)
        {
            foreach (CanvasRenderer renderer in renderers)
            {
                renderer.SetAlpha(Mathf.Lerp(0, 1, t));
            }
            t += 0.05f;
            yield return new WaitForSeconds(AnimationFrameWait);
        }

        foreach (CanvasRenderer renderer in renderers)
        {
            renderer.SetAlpha(1);
        }
        if (OnAnimationEnded != null) OnAnimationEnded();
    }

    private IEnumerator AnimationFadeOutCanvas()
    {
        CanvasRenderer[] renderers = GetComponentsInChildren<CanvasRenderer>();

        foreach (CanvasRenderer renderer in renderers)
        {
            renderer.SetAlpha(1);
        }

        float t = 1;
        while (t > 0f)
        {
            foreach (CanvasRenderer renderer in renderers)
            {
                renderer.SetAlpha(Mathf.Lerp(0, 1, t));
            }
            t -= 0.05f;
            yield return new WaitForSeconds(AnimationFrameWait);
        }

        foreach (CanvasRenderer renderer in renderers)
        {
            renderer.SetAlpha(0);
        }
        gameObject.SetActive(false);
        if (OnAnimationEnded != null) OnAnimationEnded();
    }

    private IEnumerator AnimationFadeInImage(Image image)
    {
        float t = 0;
        while (t < 1f)
        {
            image.color = new Color(image.color.r, image.color.g, image.color.b, (Mathf.Lerp(0, 1f, t)));
            t += 0.05f;
            yield return new WaitForSeconds(AnimationFrameWait);
        }
        image.color = new Color(image.color.r, image.color.g, image.color.b, 1f);
        if (OnAnimationEnded != null) OnAnimationEnded();
    }

    private IEnumerator AnimationFadeOutImage(Image image)
    {
        float t = 1;
        while (t > 0f)
        {
            image.color = new Color(image.color.r, image.color.g, image.color.b, (Mathf.Lerp(0, 1f, t)));
            t -= 0.05f;
            yield return new WaitForSeconds(AnimationFrameWait);
        }
        image.color = new Color(image.color.r, image.color.g, image.color.b, 0f);
        image.enabled = false;
        if (OnAnimationEnded != null) OnAnimationEnded();
    }
}
