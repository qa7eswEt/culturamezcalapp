﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonSelectedSprite : MonoBehaviour
{
    public Sprite unselectedSprite;
    public Sprite selectedSprite;

	public void ChangeSpriteState(bool selected)
    {
        if(selected)
        {
            GetComponent<Image>().sprite = selectedSprite;
        }
        else
        {
            GetComponent<Image>().sprite = unselectedSprite;
        }
    }
}
