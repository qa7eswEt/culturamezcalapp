﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EstrellasDeCalificacion : MonoBehaviour
{

    public Sprite magueyVacio;
    public Sprite magueyLleno;

    void Limpiar()
    {
        foreach (Transform item in gameObject.transform)
        {   // acceder a todos las calificaciones por medio del padre del boton clickeado
            item.GetComponent<Image>().sprite = magueyVacio;        // Asignar MagueyVacio a todos los elementos
        }
    }

    /// <summary>
    /// Muestra el numero que se pase como calificacion con magueyes llenos
    /// </summary>
    /// <param name="calificacion"></param>
    public void MostrarCalificacion(int calificacion)
    {
        Limpiar();
        for (int i = 0; i < calificacion; i++)
        {
            if (i < gameObject.transform.childCount) gameObject.transform.GetChild(i).GetComponent<Image>().sprite = magueyLleno;
        }
    }
}
