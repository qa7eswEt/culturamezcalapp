﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ComprarFav : MonoBehaviour {

	public string URLTienda;
	public void Comprar(){
		if (URLTienda != "" && URLTienda != null) {
            // Abrir Web mobil de la url de la Tienda de la botella
            WebViewManager.Instance.OpenLink(URLTienda);
		}
	}
}
