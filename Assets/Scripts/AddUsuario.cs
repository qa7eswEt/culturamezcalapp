﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AddUsuario : MonoBehaviour {
	public InputField nombre;
	public InputField correo;
	public InputField contrasena;
	public InputField recontrasena;
	public Toggle condiciones;
	private string Iduser;
	public GameObject Mensaje;
	public Text etiqueta;
	public RawImage ImagenFB;
    public ProfileImage profileImage;
    private string userpass;


	public void PedirNuevoID (){                    // Validacion de Campos 
		
		if (condiciones.isOn) {

			if (nombre.text != "" && correo.text != "" && contrasena.text != "")
            {
				if (contrasena.text == recontrasena.text)
                {
					if (correo.text.Contains("@") && correo.text.Contains ("."))
                    {
						Registrar();                // Registro mandar datos
					} else {
                        MessageBox.instance.ShowMessage("Correo no valido");
					}
					
					} else {
                    MessageBox.instance.ShowMessage("La contraseña no coincide");
				}
			} else {
                MessageBox.instance.ShowMessage("Rellena todos los campos");
			}
		} else {
            MessageBox.instance.ShowMessage("Debes aceptar las Condiciones de uso y compra y la Política de privacidad y Cookies.");
         
		}

	}
	public void PedirNuevoIDFB (){          // Registro con FB

		if (condiciones.isOn)
        {
			if (nombre.text != "" && correo.text != "")
            {
					if (correo.text.Contains("@") && correo.text.Contains ("."))
                {
					RegistrarPB();
				} else
                {
                    MessageBox.instance.ShowMessage("Correo no valido");
                }
			} else
            {
                MessageBox.instance.ShowMessage("Rellena todos los campos");
            }
		} else
        {
            MessageBox.instance.ShowMessage("Debes aceptar las Condiciones de uso y compra y la Política de privacidad y Cookies.");
        }
	}

	void Registrar(){                                                               // Alinear datos para mandar a Servicio y crear nuevo usuario / Registro normal
		CulturaMezcalJsons.AddUsuario au = new  CulturaMezcalJsons.AddUsuario ();
		au.Nombre = nombre.text;
		au.Correo = correo.text;
		au.Password = contrasena.text;
		au.Tipo = "Mail";

		string json = JsonUtility.ToJson(au);			
		WSClient.instance.StartWebService("user/new", json,Registrado, (r) =>
		{
			switch (r.status)
			{
				case 208: MessageBox.instance.ShowMessage("Este usuario ya está registrado."); break;
				case 418: MessageBox.instance.ShowMessage("Ya existe un usuario con esos datos."); break;
				default: MessageBox.instance.ShowMessage("Hubo un problema de conexión."); break;
			}
		});
	}

	void RegistrarPB(){                                                            // Alinear datos para mandar a Servicio y crear nuevo usuario / Registro de Facebook
		CulturaMezcalJsons.AddUsuario au = new  CulturaMezcalJsons.AddUsuario ();
		au.Nombre = nombre.text;
		au.Correo= correo.text;
        au.Password = userpass;
		au.Tipo = "FB";             // Variable diferenciadora

		string json = JsonUtility.ToJson(au);			
		WSClient.instance.StartWebService("user/new", json,RegistradoFB, (r) =>
		{
			switch (r.status)
			{
				case 418: MessageBox.instance.ShowMessage("Ya existe un usuario con esos datos."); break;
				default: MessageBox.instance.ShowMessage("Hubo un problema de conexión."); break;
			}
		}); 
	}

    void Registrado(WSResponse response)                                                 // Preparador de Registro
    {
        string result = response.responseTxt;
        CulturaMezcalJsons.Usuario usuarioJson = JsonUtility.FromJson<CulturaMezcalJsons.Usuario>(result);      // Resivir y envolver datos debueltos de (Registrar)

        CulturaMezcalJsons.Login log = new CulturaMezcalJsons.Login();                                          // Envoltotio de Login

        log.Correo = correo.text;
        log.Password = contrasena.text;
        string json = JsonUtility.ToJson(log);

        if(profileImage.rawImage.texture != null)                   // esto solo cuando el usuario no eligio una imagen y sucede algo inesperado , ya que por defecto esta una 
        {
            CulturaMezcalJsons.UploadPhoto photoJson = new CulturaMezcalJsons.UploadPhoto();
            photoJson.UserID = usuarioJson.UserID;                                                       // recoletar Imagen y asiganarla al Usuario
            byte[] textureBytes = (profileImage.rawImage.texture as Texture2D).EncodeToJPG(100);             
            photoJson.Foto = System.Convert.ToBase64String(textureBytes);
            WSClient.instance.StartWebService("user/new/photo", JsonUtility.ToJson(photoJson), OnPhotoSentSuccess, (r) => MessageBox.instance.ShowMessage("Hubo un problema de conexión."));  // Mandar Foto (imagen)
        }
        else
        {
            WSClient.instance.StartWebService("user/login", json, Login, (r) => MessageBox.instance.ShowMessage("Hubo un problema de conexión."));     // Lanzar a login 
        }
	}

    void OnPhotoSentSuccess(WSResponse response)
    {
        string result = response.responseTxt;
        CulturaMezcalJsons.Login log = new CulturaMezcalJsons.Login();
        log.Correo = correo.text;
        log.Password = contrasena.text;
        string json = JsonUtility.ToJson(log);
        WSClient.instance.StartWebService("user/login", json, Login, (r) => MessageBox.instance.ShowMessage("Hubo un problema de conexión."));   // Establecer Login
    }

	void RegistradoFB(WSResponse response){ // Registro de usuario con Facebook , la imagen no es necesaria se carga la de facebook
        string result = response.responseTxt;
        CulturaMezcalJsons.Login log = new CulturaMezcalJsons.Login ();
		log.Correo = userpass;
		log.Password = userpass;
		string json = JsonUtility.ToJson(log);			
		WSClient.instance.StartWebService("user/login", json,LoginFBUser, (r) => MessageBox.instance.ShowMessage("Hubo un problema de conexión."));  // Mandar Result a LoginFBuser

	}

	void Login(WSResponse response)  // Login del usuario Registrado anteriormente
    {
        string result = response.responseTxt;
        if (result != "No Content")     // si el resultado no contiene un usuario vacio o una respuesta vacia..
        {
            PlayerPrefs.SetString("Correo", correo.text);                       
            PlayerPrefs.SetInt("UsuarioRegistrado", 1);
            CulturaMezcalJsons.Perfil pf = new CulturaMezcalJsons.Perfil();
            pf = JsonUtility.FromJson<CulturaMezcalJsons.Perfil>(result);
            PlayerPrefs.SetString("Password",contrasena.text);
            PlayerPrefs.SetString("UserID", pf.UserID);
            PlayerPrefs.SetString("Nombre",pf.Nombre);
            BarraDeNavegacion.instance.ActivateUserProfile();                        // proceso de activacion de Perfil de usuario 
			gameObject.transform.parent.gameObject.SetActive (false);       // desactivar el Menu de Registro 
        }
	}

    void LoginFBUser(WSResponse response)
    {
        string result = response.responseTxt;
        if (result != "No Content")
        {
            PlayerPrefs.SetString("Correo", correo.text);
            PlayerPrefs.SetString("Password", userpass);            // Se usa el User de Facebook como contraseña para un Login
            PlayerPrefs.SetInt("UsuarioRegistrado", 1);
            PlayerPrefs.SetInt("FB", 1);
            CulturaMezcalJsons.Perfil pf = new CulturaMezcalJsons.Perfil();
            pf = JsonUtility.FromJson<CulturaMezcalJsons.Perfil>(result);
            PlayerPrefs.SetString("UserID", pf.UserID);
            PlayerPrefs.SetString("Nombre", pf.Nombre);

            BarraDeNavegacion.instance.ActivateUserProfile();
            gameObject.transform.parent.gameObject.SetActive(false);
        }
    }

    public void Facebook()
    {
		FBController.instance.LoginFB((success, loginResult) =>
		{
			if (success) LoginFB(loginResult);
		});
	}

	void LoginFB(FacebookJsons.FBLoginResult result)     // Cargar imagen procedente de Facebook
    {
		correo.text = result.email;
        userpass = result.id;
		nombre.text = result.profileName;
		ImagenFB.texture = result.profilePicture;
        Debug.Log("Saving photo to: " + Application.persistentDataPath + "/ProfileImage.jpg");
        Texture2D tex = result.profilePicture as Texture2D;
        System.IO.File.WriteAllBytes(Application.persistentDataPath + "/ProfileImage.jpg", tex.EncodeToJPG(50));
    }

}
