﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResultadoNoEncontrado : MonoBehaviour
{
    public string targetEmail;
    public string subject;
    public string mezcalName;
    public Texture2D photoTaken;

    public void SendNotFoundEmail()
    {
        string body = string.Format("Hola, hice una búsqueda del mezcal {0}, y no lo pude encontrar.", mezcalName);
        string mailMessage = string.Format("mailto:{0}?subject={1}&body={2}", targetEmail, WWW.EscapeURL(subject), WWW.EscapeURL(body));
        Debug.Log("Opening: " + mailMessage);

#if UNITY_ANDROID
        AndroidSocialGate.SendMail(subject, body, subject, targetEmail, photoTaken);
#elif UNITY_IOS
        IOSSocialManager.Instance.SendMail(subject, body, targetEmail, photoTaken);
#endif
    }
}
