﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using CulturaMezcalJsons;
using System;
using System.Linq;
public class WSSearch : MonoBehaviour
{
    public InputField searchInputField;
    public GameObject renglonDeResultado;
    public GameObject parentResultados;
    public GameObject panelResultadosOCR;
    public GoogleOCR googleOCR;
    public GameObject resultadosNoEncontrados;
    public RawImage ocrPhotoIOS;
    public RawImage ocrPhotoAndroid;
    public GameObject resultadosText;
    public GameObject resultadosImage;
    public GameObject Espacio;
    public GameObject SerachText;
    public GameObject SerachImage;
    private string textToSearch;
    private Texture2D imageToSearch;
    public static Dictionary<string, SearchResultInfo> Products;
    public static Dictionary<string, SerachIndex> NameDictionary;
    public static Dictionary<string, SerachIndex> EstadosDictionaries;
    public static Dictionary<string, SerachIndex> MagueysDictionaries;
    public static Dictionary<string, SerachIndex> RegionDictionaries;

    Dictionary<string, GameObject> ResultadosGO;
    public static List<string> mezcalNames;
    public static List<string> mezcalEstado;
    public static List<string> mezcalMaguey;
    public static List<string> mezcalRegion;
    private void Start()
    {
        Products = new Dictionary<string, SearchResultInfo>();
        //ResultadosGO = new List<GameObject>();
        NameDictionary = new Dictionary<string, SerachIndex>();
        EstadosDictionaries = new Dictionary<string, SerachIndex>();
        MagueysDictionaries = new Dictionary<string, SerachIndex>();
        RegionDictionaries = new Dictionary<string, SerachIndex>();
    }

    void Awake()
    {
        ResultadosGO = new Dictionary<string, GameObject>();
    }

   

    public void SendSearchWS(string searchText, bool ocr)
    {
        resultadosText.SetActive(false);
        resultadosNoEncontrados.SetActive(false);
        textToSearch = searchText;
        CulturaMezcalJsons.SendSearch searchJson = new CulturaMezcalJsons.SendSearch();
        searchJson.Human = ocr;
        searchJson.Query = textToSearch;
        GameObject.FindObjectOfType<MainSectionController>().GetComponent<MainSectionController>().ComesFromCamera(true);
        if (Products == null)
        {
            Products = new Dictionary<string, SearchResultInfo>();
        }
        if (ResultadosGO == null)
        {
            ResultadosGO = new Dictionary<string, GameObject>();
        }
        WSClient.instance.StartWebService("mezcal/query", JsonUtility.ToJson(searchJson), SearchSuccess, SearchError);
    }

    public void GetProductsInfo()
    {
        resultadosText.SetActive(false);
        resultadosNoEncontrados.SetActive(false);
        WSClient.instance.StartGetService("mezcal/all", SearchSuccessText, SearchError);
    }

    private void SearchSuccess(WSResponse res)
    {
        string response = res.responseTxt;
        if (panelResultadosOCR != null)
        {
            GameObject.FindObjectOfType<MainSectionController>().GetComponent<MainSectionController>().ComesFromCamera(true);
            if (BarraDeNavegacion.instance == null)
                Debug.Log("No navigation bar");
            BarraDeNavegacion.instance.ActivateSection(3);
        }


        SerachText.SetActive(false);
        SerachImage.SetActive(true);


        if (!response.Contains("No Content"))
        {

            
            Debug.Log("ResultadosGO.Count " + ResultadosGO.Count);
            SearchResults resultados = JsonUtility.FromJson<SearchResults>(response);
            int resultCount = resultados.Resultados.Length;
            int i;
            DelateItems();
            for (i = 0; i < resultCount; i++)
            {
                if (!Products.ContainsKey(resultados.Resultados[i].Nombre))
                    Products.Add(resultados.Resultados[i].Nombre, resultados.Resultados[i]);
            }

            Products = Products.OrderBy(x => x.Key).ToDictionary(x => x.Key, y => y.Value);
            mezcalNames = new List<string>(Products.Keys);
            for (i = 0; i < Products.Count; i++)
            {
                //GameObject renglon = (GameObject)GameObject.Instantiate(renglonDeResultado, parentResultados.transform);
                GameObject renglon = (GameObject)GameObject.Instantiate(renglonDeResultado);
                renglon.transform.localScale = Vector3.one;
                RectTransform rect = renglon.GetComponent<RectTransform>();
                rect.anchoredPosition = new Vector2(0, 0);
                rect.offsetMin = new Vector2(0, rect.offsetMin.y);
                rect.offsetMax = new Vector2(0, rect.offsetMax.y);
                renglon.GetComponent<ResultadoDeBusqueda>().ShowResult(Products[mezcalNames[i]]);
                resultadosText.SetActive(true);
                renglon.transform.SetParent(parentResultados.transform, false);
                ResultadosGO.Add(renglon.GetComponent<ResultadoDeBusqueda>().id, renglon);
            }
            //GameObject espacio = (GameObject)GameObject.Instantiate(Espacio, parentResultados.transform);
            //GameObject espacio = (GameObject)GameObject.Instantiate(Espacio);
            //espacio.transform.localScale = new Vector3(1, 1, 1);
            //espacio.transform.SetParent(parentResultados.transform, false);
        }
        else
        {
            resultadosImage.SetActive(false);
            resultadosText.SetActive(false);
            DelateItems();
            AvisoDeNoResultados(textToSearch, imageToSearch);
        }
        WSClient.instance.ShowLoadingScreen(false);
    }

    private void DelateItems()
    {
        try
        {
            foreach (Transform renglon in parentResultados.transform)
            {
                if (renglon.GetComponent<ResultadoDeBusqueda>() || renglon.gameObject.name.Contains("Espacio"))
                {
                    GameObject.Destroy(renglon.gameObject);
                }
            }

            Products.Clear();

            ResultadosGO.Clear();
            EstadosDictionaries.Clear();
            NameDictionary.Clear();
            MagueysDictionaries.Clear();
            RegionDictionaries.Clear();

        }
        catch (Exception e)
        {
            Debug.Log(e);
        }
    }

    private void SearchSuccessText(WSResponse res)
    {
        string response = res.responseTxt;
        if (panelResultadosOCR != null)
        {
            GameObject.FindObjectOfType<MainSectionController>().GetComponent<MainSectionController>().ComesFromCamera(true);
            if (BarraDeNavegacion.instance == null)
                Debug.Log("No navigation bar");
            BarraDeNavegacion.instance.ActivateSection(3);
        }


        SerachText.SetActive(true);
        SerachImage.SetActive(false);

        if (!response.Contains("No Content"))
        {

            
            Debug.Log("ResultadosGO.Count " + ResultadosGO.Count);
            SearchResultsText resultados = JsonUtility.FromJson<SearchResultsText>(response);
            int resultCount = resultados.Resultados.Length;
            int i;

            DelateItems();



            for (i = 0; i < resultCount; i++)
            {
                if (!Products.ContainsKey(resultados.Resultados[i].ID))
                {
                    SearchResultInfo auxobject = new SearchResultInfo();
                    auxobject.ID = resultados.Resultados[i].ID;
                    auxobject.Nombre = resultados.Resultados[i].Nombre;
                    auxobject.Calificacion = resultados.Resultados[i].Calificacion;
                    auxobject.Foto = resultados.Resultados[i].Foto;
                    auxobject.Agave = resultados.Resultados[i].Agave;
                    auxobject.URL = resultados.Resultados[i].URL;
                    auxobject.Coeficiente = resultados.Resultados[i].Coeficiente;
                    Products.Add(resultados.Resultados[i].ID, auxobject);


                    if (!NameDictionary.ContainsKey(resultados.Resultados[i].Nombre))
                    {
                        SerachIndex serachIndices = new SerachIndex(resultados.Resultados[i].ID);
                        NameDictionary.Add(resultados.Resultados[i].Nombre, serachIndices);
                    }
                    else
                    {
                        NameDictionary[resultados.Resultados[i].Nombre].Ids.Add(resultados.Resultados[i].ID);
                    }

                    if (!EstadosDictionaries.ContainsKey(resultados.Resultados[i].Estado))
                    {
                        SerachIndex serachIndices = new SerachIndex(resultados.Resultados[i].ID);
                        EstadosDictionaries.Add(resultados.Resultados[i].Estado, serachIndices);
                    }
                    else
                    {
                        EstadosDictionaries[resultados.Resultados[i].Estado].Ids.Add(resultados.Resultados[i].ID);
                    }

                    if (!MagueysDictionaries.ContainsKey(resultados.Resultados[i].Maguey))
                    {
                        SerachIndex serachIndices = new SerachIndex(resultados.Resultados[i].ID);
                        MagueysDictionaries.Add(resultados.Resultados[i].Maguey, serachIndices);
                    }
                    else
                    {
                        MagueysDictionaries[resultados.Resultados[i].Maguey].Ids.Add(resultados.Resultados[i].ID);
                    }

                    if (!RegionDictionaries.ContainsKey(resultados.Resultados[i].Region))
                    {
                        SerachIndex serachIndices = new SerachIndex(resultados.Resultados[i].ID);
                        RegionDictionaries.Add(resultados.Resultados[i].Region, serachIndices);
                    }
                    else
                    {
                        RegionDictionaries[resultados.Resultados[i].Region].Ids.Add(resultados.Resultados[i].ID);
                    }
                }
            }

            NameDictionary = NameDictionary.OrderBy(x => x.Key).ToDictionary(x => x.Key, y => y.Value);

            mezcalNames = new List<string>(NameDictionary.Keys);
            mezcalMaguey = new List<string>(MagueysDictionaries.Keys);
            mezcalEstado = new List<string>(EstadosDictionaries.Keys);
            mezcalRegion = new List<string>(RegionDictionaries.Keys);
            for (i = 0; i < mezcalNames.Count; i++)
            {
                if (NameDictionary.ContainsKey(mezcalNames[i]))
                {
                    for (int j = 0; j < NameDictionary[mezcalNames[i]].Ids.Count; j++)
                    {
                        GameObject renglon = (GameObject)GameObject.Instantiate(renglonDeResultado);
                        renglon.transform.localScale = Vector3.one;
                        RectTransform rect = renglon.GetComponent<RectTransform>();
                        rect.anchoredPosition = new Vector2(0, 0);
                        rect.offsetMin = new Vector2(0, rect.offsetMin.y);
                        rect.offsetMax = new Vector2(0, rect.offsetMax.y);
                        renglon.GetComponent<ResultadoDeBusqueda>().ShowResult(Products[NameDictionary[mezcalNames[i]].Ids[j]]);
                        resultadosText.SetActive(true);
                        renglon.transform.SetParent(parentResultados.transform, false);
                        ResultadosGO.Add(NameDictionary[mezcalNames[i]].Ids[j], renglon);
                    }
                }
                //GameObject renglon = (GameObject)GameObject.Instantiate(renglonDeResultado, parentResultados.transform);

            }
            Debug.Log("==== Count ResultadisGo: " + ResultadosGO.Count);
            //GameObject espacio = (GameObject)GameObject.Instantiate(Espacio, parentResultados.transform);
            //GameObject espacio = (GameObject)GameObject.Instantiate(Espacio);
            //espacio.transform.localScale = new Vector3(1, 1, 1);
            //espacio.transform.SetParent(parentResultados.transform, false);
        }
        else
        {
            DelateItems();
            AvisoDeNoResultados(textToSearch, imageToSearch);
        }
        WSClient.instance.ShowLoadingScreen(false);
    }

    private void SearchError(WSResponse res)
    {
        MessageBox.instance.ShowMessage("Hubo un problema de conexión.");
        string response = res.errorTxt;
        Debug.Log(response);
    }

    public void SearchButton()
    {
        //SendSearchWS(searchInputField.text, true);
        OnSearchInput();
        //searchInputField.text = "";
    }

    public void OnSearchInput()
    {
        resultadosNoEncontrados.SetActive(false);
        string value = searchInputField.text.ToLower();
        if (!string.IsNullOrEmpty(value))
        {
            List<string> resultName = new List<string>();
            List<string> resultMaguey = new List<string>();
            List<string> resultEstado = new List<string>();
            List<string> resultRegion = new List<string>();
            List<string> Ids = new List<string>();
            int i;
            int j;
            for (i = 0; i < mezcalNames.Count; i++)
            {
                if (mezcalNames[i].ToLower().Contains(value))
                {
                    resultName.Add(mezcalNames[i]);
                }
            }

            for (i = 0; i < mezcalEstado.Count; i++)
            {
                if (mezcalEstado[i].ToLower().Contains(value))
                {
                    resultEstado.Add(mezcalEstado[i]);
                }
            }

            for (i = 0; i < mezcalMaguey.Count; i++)
            {
                if (mezcalMaguey[i].ToLower().Contains(value))
                {
                    resultMaguey.Add(mezcalMaguey[i]);
                }
            }

            for (i = 0; i < mezcalRegion.Count; i++)
            {
                if (mezcalRegion[i].ToLower().Contains(value))
                {
                    resultRegion.Add(mezcalRegion[i]);
                }
            }

            for (i = 0; i < resultName.Count; i++)
            {
                Debug.Log("== Nombre: " + resultName[i]);
                if (NameDictionary.ContainsKey(resultName[i]))
                {
                    for (j = 0; j < NameDictionary[resultName[i]].Ids.Count; j++)
                    {
                        if (!Ids.Contains(NameDictionary[resultName[i]].Ids[j]))
                        {
                            Ids.Add(NameDictionary[resultName[i]].Ids[j]);
                        }
                    }
                }
            }

            for (i = 0; i < resultMaguey.Count; i++)
            {
                Debug.Log("== Maguey : " + resultMaguey[i]);
                if (MagueysDictionaries.ContainsKey(resultMaguey[i]))
                {
                    for (j = 0; j < MagueysDictionaries[resultMaguey[i]].Ids.Count; j++)
                    {
                        if (!Ids.Contains(MagueysDictionaries[resultMaguey[i]].Ids[j]))
                        {
                            Ids.Add(MagueysDictionaries[resultMaguey[i]].Ids[j]);
                        }
                    }
                }
            }

            for (i = 0; i < resultEstado.Count; i++)
            {
                Debug.Log("== Estado: " + resultEstado[i]);
                if (EstadosDictionaries.ContainsKey(resultEstado[i]))
                {
                    for (j = 0; j < EstadosDictionaries[resultEstado[i]].Ids.Count; j++)
                    {
                        if (!Ids.Contains(EstadosDictionaries[resultEstado[i]].Ids[j]))
                        {
                            Ids.Add(EstadosDictionaries[resultEstado[i]].Ids[j]);
                        }
                    }
                }
            }

            for (i = 0; i < resultRegion.Count; i++)
            {
                Debug.Log("== Region: " + resultRegion[i]);
                if (RegionDictionaries.ContainsKey(resultRegion[i]))
                {
                    for (j = 0; j < RegionDictionaries[resultRegion[i]].Ids.Count; j++)
                    {
                        if (!Ids.Contains(RegionDictionaries[resultRegion[i]].Ids[j]))
                        {
                            Ids.Add(RegionDictionaries[resultRegion[i]].Ids[j]);
                        }
                    }
                }
            }

            SetActiveResultadosGO(false);
            for (i = 0; i < Ids.Count; i++)
            {
                ResultadosGO[Ids[i]].SetActive(true);
                ResultadosGO[Ids[i]].GetComponent<ResultadoDeBusqueda>().LoadImage(Products[Ids[i]].Foto);
            }

            if (Ids.Count == 0)
            {
                AvisoDeNoResultados(searchInputField.text, imageToSearch);
            }
            else
            {
                resultadosNoEncontrados.SetActive(false);
            }
        }
        else
        {
            SetActiveResultadosGO(true);
        }

        LayoutRebuilder.ForceRebuildLayoutImmediate(parentResultados.GetComponent<RectTransform>());
        /*
         *
         */
    }

    private void SetActiveResultadosGO(bool value)
    {
        List<string> idsList = new List<string>(ResultadosGO.Keys);
        for(int i = 0; i < idsList.Count; i++)
        {
            ResultadosGO[idsList[i]].SetActive(value);
        }
    }

    
    public void SearchWithOCRButton()
    {
        WSClient.instance.ShowLoadingScreen(true);
        googleOCR.OnOCRSuccess += OnOCRSuccess;
        googleOCR.GetImageFromCamera();
    }

    public void OnOCRSuccess(string result, Texture2D photoTaken)
    {
        panelResultadosOCR.SetActive(true);
        print(photoTaken.width +"x"+ photoTaken.height);
        if(photoTaken.width > photoTaken.height)
        {
            ocrPhotoAndroid.texture = photoTaken;
            ocrPhotoIOS.gameObject.SetActive(false);
        }
        else
        {
            ocrPhotoIOS.texture = photoTaken;
            ocrPhotoAndroid.gameObject.SetActive(false);
        }

        imageToSearch = photoTaken;
        googleOCR.OnOCRSuccess -= OnOCRSuccess;
        SerachText.SetActive(false);
        SerachImage.SetActive(true);
        if (result != "")
        {
            SendSearchWS(result, false);
        }
        else
        {
            WSClient.instance.ShowLoadingScreen(false);
            BarraDeNavegacion.instance.ActivateSection(3);
            DelateItems();
            AvisoDeNoResultados(result, photoTaken);
        }
    }

    private void AvisoDeNoResultados(string searchText, Texture2D photo = null)
    {
        resultadosNoEncontrados.SetActive(true);
        ResultadoNoEncontrado resultadoNoEncontrado = resultadosNoEncontrados.GetComponent<ResultadoNoEncontrado>();
        resultadoNoEncontrado.mezcalName = searchText;
        resultadoNoEncontrado.photoTaken = photo;
        resultadosImage.SetActive(false);
        //GameObject espacio = (GameObject)GameObject.Instantiate(Espacio, parentResultados.transform);
        //espacio.transform.localScale = new Vector3(1, 1, 1);
    }


}
