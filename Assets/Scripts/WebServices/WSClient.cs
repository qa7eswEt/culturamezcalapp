using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using System.Text;
using System.Text.RegularExpressions;

public class WSClient : MonoBehaviour
{
    public static WSClient instance;
    ///http://165.22.219.239/api-culturamezcal/1.0/
    //http://pre-culturamezcal.ddns.net/api-culturamezcal/1.1/
    //"http://165.22.219.239/api-culturamezcal/1.0/
    //https://culturamezcal.com/api-culturamezcal/1.0/mezcal/all
    public const string WS_URL = "https://culturamezcal.com/api-culturamezcal/1.0/";
    /*
     * 
     */
    private const float TIMEOUT_TIME = 40f;
    public GameObject loadingScreen;

    public delegate void WSResponseCallback(WSResponse response);
    public delegate void OnThumbnailGenerated(Texture2D thumbnail);

    private int loadingScreenCount = 0;

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
            QualitySettings.vSyncCount = 0;
            Application.targetFrameRate = 24;
        }
        else
        {
            GameObject.Destroy(gameObject);
        }
    }

    private void Start()
    {

    }

    public void StartGetService(string endpoint, WSResponseCallback successCallback, WSResponseCallback errorCallback)
    {
        StartCoroutine(ConsumeGet(endpoint, successCallback, errorCallback));
    }

    IEnumerator ConsumeGet(string endpoint, WSResponseCallback successCallback, WSResponseCallback errorCallback)
    {
        ShowLoadingScreen(true);
        string url = WS_URL + endpoint;
        Debug.Log("To URL: " + url);
        var request = new UnityWebRequest(url, "GET");
        request.downloadHandler = (DownloadHandler)new DownloadHandlerBuffer();
        yield return request.SendWebRequest();
        if (request.responseCode == 200)
        {
            successCallback(new WSResponse()
            {
                success = true,
                status = (int)request.responseCode,
                responseTxt = request.downloadHandler.text,
                errorTxt = ""
            });
        }
        else
        {
            MessageBox.instance.ShowMessage("Hubo un problema de conexión.");
            if (errorCallback != null)
            {
                errorCallback(new WSResponse()
                {
                    success = false,
                    status = (int)request.responseCode,
                    responseTxt = "",
                    errorTxt = request.error
                });
            }
        }
        ShowLoadingScreen(false);
    }


    public void StartWebService(string endpoint, string json, WSResponseCallback successCallback, WSResponseCallback errorCallback)
    {
        StartCoroutine(ConsumeWebService(endpoint, json, successCallback, errorCallback));
    }

    /// <summary>
    /// Envia al servidor un request para consumir el webservice al endpoint especificado
    /// </summary>
    /// <param name="endpoint">El endpoint del webservice a consumir</param>
    /// <param name="json">El json que se va a enviar</param>
    /// <param name="successCallback">Funcion que se va a ejecutar al consumir el WS con exito</param>
    /// <param name="errorCallback">Funcion que se va a ejecutar al encontrar un error al consumir el WS</param>
    /// <returns></returns>
    private IEnumerator ConsumeWebService(string endpoint, string json, WSResponseCallback successCallback, WSResponseCallback errorCallback)
    {
        ShowLoadingScreen(true);

        string url = WS_URL + endpoint;

        Debug.Log("Sending JSON: " + json);

        //json = Cryptor.EncryptWSString (json);

        Debug.Log("To URL: " + url);

        WWW request = ConstructPOST(json, url);

        float timer = 0;
        bool timeOut = false;
        while (!request.isDone)
        {
            if (timer > TIMEOUT_TIME) { timeOut = true; break; }
            timer += Time.deltaTime;
            yield return null;
        }
        if (timeOut)
        {
            MessageBox.instance.ShowMessage("Hubo un problema de conexión.");
            Debug.Log("Timeout Error");
            if (errorCallback != null)
                errorCallback(new WSResponse()
                {
                    success = false,
                    status = 408,
                    responseTxt = "",
                    errorTxt = request.error
                });
        }
        else
        {
            if (!string.IsNullOrEmpty(request.error))
                Debug.LogError($"Request error: {request.error}");

            int responseCode = 0;
            if (request.responseHeaders["STATUS"] != null)
            {
                var components = request.responseHeaders["STATUS"].Split(' ');
                if (components.Length >= 3)
                {
                    System.Int32.TryParse(components[1], out responseCode);
                }
            }

            WSResponse wsResponse = new WSResponse()
            {
                success = true,
                status = responseCode,
                responseTxt = request.text,
                errorTxt = request.error
            };

            if (request.responseHeaders != null)
            {
                if (request.responseHeaders.ContainsKey("STATUS"))
                {
                    if (string.IsNullOrEmpty(request.error))
                    {
                        Debug.Log("Response status: " + request.responseHeaders["STATUS"] + " Response: " + request.text);
                        string statusCode = request.responseHeaders["STATUS"];
                        string Code = Regex.Replace(statusCode, @"[^\d]", "");
                        switch (Code)
                        {
                            case "11204":
                                wsResponse.responseTxt = "No Content";
                                successCallback(wsResponse);
                                break;
                            case "11208":
                                // MessageBox.instance.ShowMessage("Este usuario ya está registrado.");
                                wsResponse.success = false;
                                if (errorCallback != null) errorCallback(wsResponse);
                                break;
                            default:
                                successCallback(wsResponse);
                                break;
                        }
                        ShowLoadingScreen(false);
                        yield break;
                    }
                }
            }

            wsResponse.success = false;
            if (string.IsNullOrEmpty(request.error))
            {
                // MessageBox.instance.ShowMessage("Hubo un problema de conexión.");
                if (errorCallback != null) errorCallback(wsResponse);
            }
            else
            {
                if (request.error.Contains("401") || request.error.Contains("unauthorized"))
                {
                    // MessageBox.instance.ShowMessage("Datos de usuario incorrectos.");
                    if (errorCallback != null) errorCallback(wsResponse);
                }
                else if (request.error.Contains("418"))
                {
                    // MessageBox.instance.ShowMessage("Ya existe un usuario con esos datos.");
                    if (errorCallback != null) errorCallback(wsResponse);
                }
                else
                {
                    // MessageBox.instance.ShowMessage("Hubo un problema de conexión.");
                    if (errorCallback != null) errorCallback(wsResponse);
                }
            }
        }
        ShowLoadingScreen(false);
    }

    public void GenerateThumnbail(byte[] imageBytes, OnThumbnailGenerated callback)
    {
        StartCoroutine(SendGenerateThumbnail(imageBytes, callback));
    }

    private IEnumerator SendGenerateThumbnail(byte[] imageBytes, OnThumbnailGenerated callback)
    {
        ShowLoadingScreen(true);
        Dictionary<string, string> headersDictionary = new Dictionary<string, string>();
        headersDictionary.Add("Content-Type", "application/octet-stream");
        headersDictionary.Add("Ocp-Apim-Subscription-Key", "3faef4cba0ce444b95e0b7900011ad4c");

        string url = "https://westus.api.cognitive.microsoft.com/vision/v1.0/generateThumbnail?width=512&height=512&smartCropping=true";

        WWW w = new WWW(url, imageBytes, headersDictionary);

        print("sending photo...");
        ShowLoadingScreen(true);
        float timer = 0;
        bool timeOut = false;
        while (!w.isDone)
        {
            if (timer > TIMEOUT_TIME) { timeOut = true; break; }
            timer += Time.deltaTime;
            print(timer);
            yield return null;
        }
        if (timeOut)
        {
            w.Dispose();
            MessageBox.instance.ShowMessage("Hubo un problema de conexión.");
            Debug.Log("Timeout Error");
        }
        else
        {
            if (!string.IsNullOrEmpty(w.error))
            {
                MessageBox.instance.ShowMessage("Hubo un problema de conexión.");
                print(w.error);
            }
            else
            {
                print("Success");
                callback(w.texture);
            }
        }
        ShowLoadingScreen(false);
    }

    private WWW ConstructPOST(string lcData, string lcURL)
    {
        Debug.Log("Data: " + lcData + " URL: " + lcURL);

        Dictionary<string, string> loHeaders = new Dictionary<string, string>();
        loHeaders.Add("Content-Type", "application/json");
        UTF8Encoding encoder = new UTF8Encoding();
        WWW loWWW = new WWW(lcURL, encoder.GetBytes(lcData), loHeaders);
        return loWWW;
    }

    public void ShowLoadingScreen(bool active)
    {
        if (loadingScreen != null)
        {
            if (active)
            {
                loadingScreenCount++;
                loadingScreen.SetActive(true);
            }
            else
            {
                loadingScreenCount--;
                if (loadingScreenCount <= 0)
                {
                    loadingScreenCount = 0;
                    loadingScreen.SetActive(false);
                }
            }
            Debug.Log("loadingScreenCount: " + loadingScreenCount);
        }
        else Debug.LogError("loadingScreen es null");
    }

}

public class WSResponse
{
    public bool success;
    public int status;
    public string responseTxt;
    public string errorTxt;
}
