﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ControllerMenu : MonoBehaviour
{
	Animator am;
	public Animator amdesplegable;
	public GameObject principal;
    public Image fondoNegro;

	void Start ()
    {
		am = transform.GetComponent<Animator> ();		// referencia a Animator
	}

	public void Abrir()
    {							                        // Mover Paneles hacia la derecha ejecutando animacion
		bool abrir = am.GetBool("Abrir");				// Referncia con parametro abrir  de animator Local
		bool uno = am.GetBool("Uno");					// Referncia con parametro uno  de animator Local
		bool Abierto = amdesplegable.GetBool ("Abierto"); // Referncia con parametro Abierto  de animator externo ( panel desplegable)
		am.SetBool ("Abrir",!abrir);					// Abrir local sera igual al opuesto de su valor True >>! False
		am.SetBool ("Uno", !uno);						// Uno Local es igual al opuesto de su valor
		amdesplegable.SetBool ("Abierto", !Abierto);	// Abierto externo es igual al opuesto de su valor
        if (Abierto)
        {
            fondoNegro.raycastTarget = false;
           StartCoroutine(crossFade(fondoNegro, 0f, 0.5f));
        }
        else
        {
            fondoNegro.raycastTarget = true;
            StartCoroutine(crossFade(fondoNegro, 0.6f, 0.5f));
        }
	}

    private IEnumerator crossFade(Image image, float target, float duration)
    {
        float totalChange = target - image.color.a;
        float changePerSecond = totalChange / duration;
        float totalTime = 0;
        while (totalTime < duration)
        {
            totalTime += Time.deltaTime;
            float increment = Time.deltaTime * changePerSecond;
            image.color = new Color(image.color.r, image.color.g, image.color.b, image.color.a + increment);

            yield return new WaitForEndOfFrame();
        }

        image.color = new Color(image.color.r, image.color.g, image.color.b, target);

        yield break;
    }

    public void CloseSession()
    {
        print("Closing session");
        PlayerPrefs.DeleteAll();
        if(System.IO.File.Exists(Application.persistentDataPath + "/ProfileImage.jpg"))
        {
            System.IO.File.Delete(Application.persistentDataPath + "/ProfileImage.jpg");
        }
        UnityEngine.SceneManagement.SceneManager.LoadScene(UnityEngine.SceneManagement.SceneManager.GetActiveScene().buildIndex);
    }

}
