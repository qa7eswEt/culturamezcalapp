﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class BajarImagen : MonoBehaviour {
	public RawImage imagen;
	void Start(){
		StartCoroutine (DescargarFoto (gameObject.transform.parent.name));
	}

	public IEnumerator DescargarFoto(string Url)
	{		
		imagen.transform.GetChild (0).gameObject.SetActive (true);
		WWW www = new WWW (Url); 
		yield return www;
		Destroy (imagen.transform.GetChild (0).gameObject);
		imagen.texture = www.texture;
	

	}
}
