﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Facebook.Unity;

public class LoginPerfil : MonoBehaviour {
	public GameObject Login;
	public GameObject MenuPerfil;

	// Use this for initialization
	void Start () {

	}
	public string ID;
	void LoginUserID(){
        //juancastillo
			CulturaMezcalJsons.Login log = new CulturaMezcalJsons.Login ();
			log.Correo = PlayerPrefs.GetString ("Correo");
			log.Password = PlayerPrefs.GetString ("Password");
			string json = JsonUtility.ToJson (log);			
		    WSClient.instance.StartWebService ("user/login", json, UserID, (r) => MessageBox.instance.ShowMessage("Hubo un problema de conexión."));  
	}

	void UserID(WSResponse response){
        string result = response.responseTxt;
		CulturaMezcalJsons.Perfil pf = new CulturaMezcalJsons.Perfil ();
		pf=JsonUtility.FromJson<CulturaMezcalJsons.Perfil>(result);

		if (pf.UserID!=null) {
			ID = pf.UserID;
            PlayerPrefs.SetString("UserID", pf.UserID);
		}
	}
	public void Get_UserID(){
		LoginUserID ();
	}

	public void Iniciar(){
		if (PlayerPrefs.HasKey("UserID")) {
			//MenuPerfil.SetActive (true);
			//MenuPerfil.GetComponent<PerfilUsuario> ().Comenzar ();
		} else {
			Login.SetActive (true);
		}
	}

}
