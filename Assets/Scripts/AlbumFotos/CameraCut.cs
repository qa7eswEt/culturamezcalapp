﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.Events;

public class CameraCut : MonoBehaviour
{
    [Tooltip("Es el viewport que contiene la foto de la camara.")]
    public RectTransform rectTransformViewport;

    [Tooltip("Imagen de la foto de la camara en el canvas principal.")]
    public RawImage rawImage;

    [Tooltip("RectTransform de la Imagen de la foto de la camara en el canvas principal.")]
    public RectTransform rectTransformRawImage;

    [Tooltip("Imagen de la foto de la camara en el canvas de la camara que cortará.")]
    public RawImage rawImageToCut;

    [Tooltip("RectTransform de la Imagen de la foto de la camara en el canvas de la camara que cortará.")]
    public RectTransform rectTransformRawImageToCut;

    [Tooltip("Ancho de la imagen.")]
    public int textureWidth = 630;

    [Tooltip("Ancho de la imagen.")]
    public int textureHeight = 630;

    [Tooltip("Camara que se usa para el cortado de la imagen.")]
    public Camera myCamera;
    public RawImage prueba;
    public Texture2D tex;
    public GameObject Loading;

    /// <summary>
    /// 
    /// 
    /// Es el render texture que usará la camara.
    /// </summary>
    private RenderTexture goRenderTexture;

    /// <summary>
    /// Funcion que se llama al terminar el crop.
    /// </summary>
    private UnityAction<Texture2D> goOnFinishCrop = null;

    /// <summary>
    /// Fix te camera.
    /// </summary>
    void Start()
    {
        goRenderTexture = new RenderTexture(textureWidth, textureHeight, 16, RenderTextureFormat.Default);
        goRenderTexture.antiAliasing = 2;
        goRenderTexture.Create();
        myCamera.targetTexture = goRenderTexture;
        myCamera.aspect = 1;
        myCamera.enabled = false;

    }
    public void empezar()
    {
        StartCrop(tex, pruebas);
    }
    public void pruebas(Texture2D textura)
    {
        prueba.texture = textura;
    }

    //private void Update()
    //{
    //    myCamera.aspect = 1;

    //    //<< Activamos la camara para la captura.
    //    myCamera.enabled = true;

    //    //<< Establecemos el width y height del rectTransform.
    //    rectTransformRawImageToCut.sizeDelta = rectTransformRawImage.sizeDelta;

    //    //<< Establecemos la textura de la imagen.
    //    rawImageToCut.texture = rawImage.texture;

    //    //<< Establecemos la escala de la imagen original.
    //    rawImageToCut.transform.localScale = rawImage.transform.localScale;

    //    //<< Establecemos la posicion con base en la posicion del rawImage original y la posicion en el viewport.
    //    rawImageToCut.transform.localPosition = rawImage.transform.localPosition;
    //}

    /// <summary>
    /// Capturamos el corte de la imagen.
    /// </summary>
    public void StartCrop(Texture2D loTexture2D, UnityAction<Texture2D> loUnityAction = null)
    {
        Debug.Log("Dentro del Scatrcrop");
       
        //Loading.SetActive(true);
        rawImage.rectTransform.rotation = Quaternion.Euler(0, 0, 0);
        rawImageToCut.rectTransform.rotation = Quaternion.Euler(0, 0,0);
        rectTransformRawImage.transform.rotation = Quaternion.Euler(0, 0,0);
        rectTransformRawImageToCut.transform.rotation = Quaternion.Euler(0, 0, 0);


        rawImage.texture = loTexture2D;
        //<< Activamos nuestro elemento para el corte de camara.
        gameObject.SetActive(true);

        //<< Evento que se llama cuando termina el crop.
        goOnFinishCrop = loUnityAction;

        //<< Establecemos el width y height del rectTransform.
        rectTransformRawImage.sizeDelta = new Vector2(loTexture2D.width, loTexture2D.height);

        //<< Establecemos el centro de la posicion.
        rawImage.SetNativeSize();
       // rectTransformRawImage.SetNativeSize();

#if UNITY_ANDROID
        // Desde la galerya de android en su vercion altual entrega la textura al mismo tamaño la frontal por lo que , hay que girarla 
        if (loTexture2D.width > loTexture2D.height)     // si es mas acha que alta 
        {
            rawImage.rectTransform.rotation = Quaternion.Euler(0, 0, -90);                             
            rawImageToCut.rectTransform.rotation = Quaternion.Euler(0, 0, -90);                 // ROTACION -90 A todo 
            rectTransformRawImage.transform.rotation = Quaternion.Euler(0, 0, -90);
            rectTransformRawImageToCut.transform.rotation = Quaternion.Euler(0, 0, -90);
        }

        Debug.Log(Screen.orientation);
#elif UNITY_IOS
        
#endif

    
    }
    
   // Crop de la imagen tomada con Camara 

    public void StartCrop2(Texture2D loTexture2D,int posicion, UnityAction<Texture2D> loUnityAction = null)
    {
        Debug.Log("Posicion de Camara:" + posicion);
      
                rawImage.rectTransform.rotation = Quaternion.Euler(0, 0, 0);
                rawImageToCut.rectTransform.rotation = Quaternion.Euler(0, 0, 0);
                rectTransformRawImage.transform.rotation = Quaternion.Euler(0, 0, 0);           // Recetear a zero Todo
                rectTransformRawImageToCut.transform.rotation = Quaternion.Euler(0, 0, 0);

        /*
                      if (Input.acceleration.x > -1 && Input.acceleration.x < -.55)
                      {
                          rawImage.rectTransform.rotation = Quaternion.Euler(0, 0, 0);
                          rawImageToCut.rectTransform.rotation = Quaternion.Euler(0, 0, 0);           // cuando Fue tomada la foto de Lado la Posicion no cambia
                          rectTransformRawImage.transform.rotation = Quaternion.Euler(0, 0,0);
                          rectTransformRawImageToCut.transform.rotation = Quaternion.Euler(0, 0, 0);
                          Debug.Log("Lan2");

                      }
                      else if (Input.acceleration.x > -.54&& Input.acceleration.x < .55)
                      {
                          Debug.Log("port");
                          rawImage.rectTransform.rotation = Quaternion.Euler(0, 0, -90);
                          rawImageToCut.rectTransform.rotation = Quaternion.Euler(0, 0, -90);           // cuando Fue tomada la foto de frente Port - Girar hacia la derecha 90°
                          rectTransformRawImage.transform.rotation = Quaternion.Euler(0, 0, -90);
                          rectTransformRawImageToCut.transform.rotation = Quaternion.Euler(0, 0, -90);
                      }
                      else if (Input.acceleration.x > -.56 && Input.acceleration.x < 1)
                      {
                          rawImage.rectTransform.rotation = Quaternion.Euler(0, 0, -180);
                          rawImageToCut.rectTransform.rotation = Quaternion.Euler(0, 0, -180);         // cuando Fue tomada la foto de lado in vertida  - Girar hacia la derecha 180°
                          rectTransformRawImage.transform.rotation = Quaternion.Euler(0, 0, -180);
                          rectTransformRawImageToCut.transform.rotation = Quaternion.Euler(0, 0, -180);
                          Debug.Log("Land");
                      }


                      */
        if (posicion==0)
        {
            rawImage.rectTransform.rotation = Quaternion.Euler(0, 0, 0);
            rawImageToCut.rectTransform.rotation = Quaternion.Euler(0, 0, 0);           // cuando Fue tomada la foto de Lado la Posicion no cambia
            rectTransformRawImage.transform.rotation = Quaternion.Euler(0, 0, 0);
            rectTransformRawImageToCut.transform.rotation = Quaternion.Euler(0, 0, 0);
            Debug.Log("Lan2");
        }
        else if(posicion==1){
            rawImage.rectTransform.rotation = Quaternion.Euler(0, 0, -90);
            rawImageToCut.rectTransform.rotation = Quaternion.Euler(0, 0, -90);           // cuando Fue tomada la foto de frente Port - Girar hacia la derecha 90°
            rectTransformRawImage.transform.rotation = Quaternion.Euler(0, 0, -90);
            rectTransformRawImageToCut.transform.rotation = Quaternion.Euler(0, 0, -90);

        }
        else if (posicion == 2)
        {
            rawImage.rectTransform.rotation = Quaternion.Euler(0, 0, -180);
            rawImageToCut.rectTransform.rotation = Quaternion.Euler(0, 0, -180);         // cuando Fue tomada la foto de lado in vertida  - Girar hacia la derecha 180°
            rectTransformRawImage.transform.rotation = Quaternion.Euler(0, 0, -180);
            rectTransformRawImageToCut.transform.rotation = Quaternion.Euler(0, 0, -180);
            Debug.Log("Land");
        }


        rawImage.texture = loTexture2D;
        //<< Activamos nuestro elemento para el corte de camara.
        gameObject.SetActive(true);
        //<< Evento que se llama cuando termina el crop.
        goOnFinishCrop = loUnityAction;
        //<< Establecemos el width y height del rectTransform.
        rectTransformRawImage.sizeDelta = new Vector2(loTexture2D.width, loTexture2D.height);
        //<< Establecemos el centro de la posicion.
        rectTransformRawImage.localPosition = Vector3.zero;
   
    }
     
    public void StartCrop3(Texture2D loTexture2D ,UnityAction<Texture2D> loUnityAction = null)
    {

        rawImage.texture = loTexture2D;
        //<< Activamos nuestro elemento para el corte de camara.
        gameObject.SetActive(true);
        //<< Evento que se llama cuando termina el crop.
        goOnFinishCrop = loUnityAction;
        //<< Establecemos el width y height del rectTransform.
        rectTransformRawImage.sizeDelta = new Vector2(loTexture2D.width, loTexture2D.height);
        //<< Establecemos el centro de la posicion.
        rectTransformRawImage.localPosition = Vector3.zero;
    }




    /// <summary>
    /// Cancelamos el corte.
    /// </summary>
    public void OnCancel()
    {
        StartCoroutine(TextureCoroutine());
        //<< Desactivamos nuestro elemento para el corte de camara.
        gameObject.SetActive(false);
    }

    /// <summary>
    /// Capturamos el corte de la imagen.
    /// </summary>
    public void OnAccept()
    {
        //<< Activamos la camara para la captura.
        myCamera.enabled = true;

        //<< Establecemos el width y height del rectTransform.
        rectTransformRawImageToCut.sizeDelta = rectTransformRawImage.sizeDelta;

        //<< Establecemos la textura de la imagen.
        rawImageToCut.texture = rawImage.texture;

        //<< Establecemos la escala de la imagen original.
        rawImageToCut.transform.localScale = rawImage.transform.localScale;

        //<< Establecemos la posicion con base en la posicion del rawImage original y la posicion en el viewport.
        rawImageToCut.transform.localPosition = rawImage.transform.localPosition;

        //<< Iniciamos la generacion de la imagen.
        StartCoroutine(TextureCoroutine());
    }

    /// <summary>
    /// Coroutina para esperar un frame y llamar la funcion postframe.
    /// </summary>
    /// <param name="loUnityAction"></param>
    /// <returns></returns>
    IEnumerator TextureCoroutine()
    {
        yield return new WaitForEndOfFrame();
        PostFrame();
    }

    /// <summary>
    /// Se debe llamar cuando ya pasó un frame.
    /// </summary>
    private void PostFrame()
    {
        RenderTexture loRenderTexture = goRenderTexture;

        //<< De los datos de nuestro render texture obtenemos las dimenciones de nuestra textura 2D y su formato.
        Texture2D loTexture2D = new Texture2D(loRenderTexture.width, loRenderTexture.height, TextureFormat.RGB24, false);

        //<< Una ves que tenemos en RenderTexture nuestra imagen completa la establecemos como la activa para que Texture2D obtenga de ahi los pixeles.
        RenderTexture.active = loRenderTexture;

        //<< Leemos los pixeles de RenderTexture.active...
        loTexture2D.ReadPixels(new Rect(0, 0, loRenderTexture.width, loRenderTexture.height), 0, 0);
        loTexture2D.Apply();

        //<< Desactivamos la camara.
        myCamera.enabled = true;

        //<< Llamamos el evento de que terminamos y pasamos la textura 2D.
        if (goOnFinishCrop != null) goOnFinishCrop.Invoke(loTexture2D);
        goOnFinishCrop = null;

        //<< Desactivamos nuestro elemento para el corte de camara.
        gameObject.SetActive(false);
    }
}