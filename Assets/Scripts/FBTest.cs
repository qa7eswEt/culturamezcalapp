﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FBTest : MonoBehaviour
{
    public UnityEngine.UI.RawImage testProfileImage;

	public void LoginTest()
    {
        // FBController.instance.LoginFB(loginTestCallback);
    }

    public void loginTestCallback(FacebookJsons.FBLoginResult result)
    {
        print("Name: " + result.profileName);
        print("Email: " + result.email);
        testProfileImage.texture = result.profilePicture;
    }

    
}
