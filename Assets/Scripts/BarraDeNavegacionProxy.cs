﻿using UnityEngine;

public class BarraDeNavegacionProxy : MonoBehaviour
{
    public void ActivateSection(int canvasIndex)
    {
        BarraDeNavegacion.instance.ActivateSection(canvasIndex);
    }
}
