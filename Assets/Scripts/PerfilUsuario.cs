﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using CulturaMezcalJsons;


public class PerfilUsuario : MonoBehaviour
{
    public GameObject menuDespegable;
    public GameObject FavoritosContent;
    public GameObject HistoricoContent;
    public GameObject TendenciasContent;
    public GameObject PrefabFavoritos;
    public GameObject PrefabTendencias;
    public GameObject PrefabHistorico;
    public int contador;
    public Sprite mageylleno;
    public Sprite MageyVacio;
    public Sprite CorazonVerde;
    public Sprite CorazonAmarillo;
    public Text Nombre;
    public GameObject PrefabFichaM;
    public GameObject TituloTendencias;

    void OnEnable()
    {
        RefreshPerfil();
    }

    public void RefreshPerfil()
    {
        if (menuDespegable != null) menuDespegable.SetActive(true);
        foreach (Transform item in gameObject.transform)
        {
            if (item.transform.name.Contains("Ficha"))
            {
                menuDespegable.SetActive(false);
            
        }
        }
        if (PlayerPrefs.HasKey("UserID"))
        {
            Nombre.text = PlayerPrefs.GetString("Nombre");
            us.UserID = PlayerPrefs.GetString("UserID");
            if (FavoritosContent.transform.parent.gameObject.activeSelf) CargarFavoritos();
            if (HistoricoContent.transform.parent.gameObject.activeSelf) CargarHistorico();
            if (TendenciasContent.transform.parent.gameObject.activeSelf) CargarTendencias();
        }
    }

    void RefreshPorFicha()
    {
        if (transform.GetComponentsInChildren<FichaMezcales>().Length <= 1)
        {
            RefreshPerfil();
            menuDespegable.SetActive(true);
        }
    }

    void OnDisable()
    {
        if (menuDespegable != null) menuDespegable.SetActive(false);
    }

    CulturaMezcalJsons.Usuario us = new CulturaMezcalJsons.Usuario();

    void Rellenar(string result)
    {

        CulturaMezcalJsons.Perfil pf = new CulturaMezcalJsons.Perfil();
        pf = JsonUtility.FromJson<CulturaMezcalJsons.Perfil>(result);
        Nombre.text = pf.Nombre;
        us.UserID = pf.UserID;
        string json = JsonUtility.ToJson(us);
        WSClient.instance.StartWebService("user/favorite/list", json, listarFav, (r) => MessageBox.instance.ShowMessage("Hubo un problema de conexión."));

    }

    CulturaMezcalJsons.Historico His = new CulturaMezcalJsons.Historico();
    CulturaMezcalJsons.Favorito Fav = new CulturaMezcalJsons.Favorito();
    CulturaMezcalJsons.Tendencias top5 = new CulturaMezcalJsons.Tendencias();

    void listarFav(WSResponse response)
    {
        string result = response.responseTxt;
        vaciar(FavoritosContent);
        if (result != "No Content")
        {
            Fav = JsonUtility.FromJson<CulturaMezcalJsons.Favorito>(result);
            Favoritos();
        }
    }
    void listarHis(WSResponse response)
    {
        string result = response.responseTxt;
        Debug.Log("ListaHistorico");
        vaciar(HistoricoContent);
        if (result != "No Content")
        {
            Debug.Log("Lista Si contect");
            His = JsonUtility.FromJson<CulturaMezcalJsons.Historico>(result);
            Historico();
        }
    }
    void listarTen(WSResponse response)
    {
        string result = response.responseTxt;
        vaciar(TendenciasContent);
        if (result != "No Content")
        {
            top5 = JsonUtility.FromJson<CulturaMezcalJsons.Tendencias>(result);
            Tendencias();
        }
        else
        {
            Vector3 pos = TendenciasContent.transform.position;
            var resumen = (GameObject)Instantiate(PrefabTendencias, TendenciasContent.transform);
            resumen.transform.localScale = new Vector3(1, 1, 1);
            resumen.transform.GetChild(2).GetComponent<Text>().text = FavoritosContent.transform.childCount + " Favoritos"; // Cantidad de Favoritos añadidos
            Estadisticas(resumen.gameObject);
        }
    }

    public void CargarFavoritos()
    {
        string json = JsonUtility.ToJson(us);
        WSClient.instance.StartWebService("user/favorite/list", json, listarFav, (r) => MessageBox.instance.ShowMessage("Hubo un problema de conexión."));
    }
    public void CargarHistorico()
    {
        Debug.Log("CargarHistorico");
        string json = JsonUtility.ToJson(us);
        WSClient.instance.StartWebService("user/degustacion/list", json, listarHis, (r) => MessageBox.instance.ShowMessage("Hubo un problema de conexión."));
    }
    public void CargarTendencias()
    {
        if (us.UserID != "" && us.UserID != null)
        {
            string json = JsonUtility.ToJson(us);
            WSClient.instance.StartWebService("mezcal/tendencias", json, listarTen, (r) => MessageBox.instance.ShowMessage("Hubo un problema de conexión."));
        }
    }
    public void Favoritos()
    {
        if (Fav.Favoritos.Length > 0)
        {
            for (int i = 0; i < Fav.Favoritos.Length; i++)
            {
                var producto = (GameObject)Instantiate(PrefabFavoritos);
                producto.GetComponentInChildren<CrearFicha>().OnCloseFicha += RefreshPorFicha;
                producto.GetComponentInChildren<CrearFicha>().MenuDesplegable = menuDespegable;
                producto.transform.parent = FavoritosContent.transform;
                producto.transform.localScale = new Vector3(1, 1, 1);
                // LLenar el Producto
                producto.transform.GetChild(6).GetComponent<Text>().text = "€" + Fav.Favoritos[i].Precio; //Precio
                for (int j = 0; j < 5; j++)
                {
                    if (j < Fav.Favoritos[i].Calificacion)
                    {                                                       // Calificacion con Mageys
                        producto.transform.GetChild(5).transform.GetChild(j).GetComponent<Image>().sprite = mageylleno;
                    }
                    else
                    {
                        producto.transform.GetChild(5).transform.GetChild(j).GetComponent<Image>().sprite = MageyVacio;
                    }
                }
                producto.name = Fav.Favoritos[i].IdProducto;
                producto.transform.GetChild(3).GetComponent<Text>().text = Fav.Favoritos[i].Agave.ToString(); // Nombre

                producto.transform.GetChild(4).GetComponent<Text>().text = Fav.Favoritos[i].Nombre.ToString(); // Marca Origen
                producto.transform.GetChild(4).GetComponent<Truncar>().Trunca(18);
                producto.transform.GetChild(2).GetComponent<Text>().text = Fav.Favoritos[i].Fecha;

                producto.GetComponentInChildren<WebImage>().LoadImage(Fav.Favoritos[i].Foto);

                if (Fav.Favoritos[i].URL != "")
                {
                    producto.transform.GetChild(8).GetComponent<ComprarFav>().URLTienda = Fav.Favoritos[i].URL;
                }
                else
                {
                    producto.transform.GetChild(8).GetComponent<Button>().interactable = false;
                }
                producto.name = Fav.Favoritos[i].IdProducto;
            }
        }
        LayoutRebuilder.ForceRebuildLayoutImmediate(FavoritosContent.GetComponent<RectTransform>());
    }

    public void Historico()
    {
        Debug.Log("Dentro Historico");
        Vector3 pos = HistoricoContent.transform.position;
        if (His != null)
        {
            Debug.Log("Historico diferente de null");
            Debug.Log("Cant:"+His.Degustaciones.Length);
            for (int i = 0; i < His.Degustaciones.Length; i++)
            {
               
                    Debug.Log("5 es mayor a Historico");
                    var producto = (GameObject)Instantiate(PrefabHistorico, HistoricoContent.transform);
                    producto.transform.localScale = new Vector3(1, 1, 1);
                    producto.GetComponentInChildren<CrearFicha>().OnCloseFicha += RefreshPorFicha;
                    producto.GetComponentInChildren<CrearFicha>().MenuDesplegable = menuDespegable;
                    // Llenar Producto
                    producto.transform.GetChild(2).GetComponent<Text>().text = His.Degustaciones[i].fecha; // Fecha de Degustacion
                    producto.transform.GetChild(3).GetComponent<Text>().text = His.Degustaciones[i].producto.Nombre;// Nombre 
                    producto.transform.GetChild(4).GetComponent<Text>().text = His.Degustaciones[i].producto.Agave; // Etiqueta / Agave
                    producto.transform.GetChild(3).GetComponent<Truncar>().Trunca(18);
                    for (int j = 0; j < 5; j++)
                    {
                        if (j < His.Degustaciones[i].producto.Calificacion)
                        {
                            producto.transform.GetChild(5).transform.GetChild(j).GetComponent<Image>().sprite = mageylleno; // Calificacion con Mageys Promedio
                        }
                        else
                        {
                            producto.transform.GetChild(5).transform.GetChild(j).GetComponent<Image>().sprite = MageyVacio;
                        }
                        if (j < His.Degustaciones[i].Calificacion)
                        {
                            producto.transform.GetChild(6).transform.GetChild(j).GetComponent<Image>().sprite = mageylleno; // Calificacion con Mageys Usuario
                        }
                        else
                        {
                            producto.transform.GetChild(6).transform.GetChild(j).GetComponent<Image>().sprite = MageyVacio;
                        }
                    }
                    producto.transform.GetChild(7).GetComponent<Text>().text = string.Format("€{0}", His.Degustaciones[i].producto.Precio); // Precio ,costo
                    producto.name = His.Degustaciones[i].id;
                    if (His.Degustaciones[i].Favorito)
                    {
                        producto.transform.GetChild(9).GetComponent<Image>().sprite = CorazonAmarillo;
                    }
                    else
                    {
                        producto.transform.GetChild(9).GetComponent<Image>().sprite = CorazonVerde;
                    }

                    producto.GetComponentInChildren<WebImage>().LoadImage(His.Degustaciones[i].producto.Foto);
                    pos.y -= 340; // Acomodar Espacion de Rect para el Prefabs
                    if (His.Degustaciones[i].producto.URL != "")
                    {
                        producto.transform.GetChild(8).GetComponent<ComprarFav>().URLTienda = His.Degustaciones[i].producto.URL;

                    }
                    else
                    {
                        producto.transform.GetChild(8).GetComponent<Button>().interactable = false;
                    }
                    producto.name = His.Degustaciones[i].producto.IdProducto;
                
            }
        }
        LayoutRebuilder.ForceRebuildLayoutImmediate(HistoricoContent.GetComponent<RectTransform>());
    }

    public void Tendencias()
    {
        Vector3 pos = TendenciasContent.transform.position;
        var titulo = (GameObject)Instantiate(TituloTendencias, TendenciasContent.transform);
        titulo.transform.localScale = new Vector3(1, 1, 1);
        for (int i = 0; i < top5.Top5.Length; i++)
        {
            if (5 >= top5.Top5.Length)
            {
                var producto = (GameObject)Instantiate(PrefabFavoritos, TendenciasContent.transform);
                producto.GetComponentInChildren<CrearFicha>().OnCloseFicha += RefreshPorFicha;
                producto.GetComponentInChildren<CrearFicha>().MenuDesplegable = menuDespegable;
                producto.transform.localScale = new Vector3(1, 1, 1);
                // Llenar Producto
                producto.transform.GetChild(2).GetComponent<Text>().text = "";          // Nombre

                producto.transform.GetChild(4).GetComponent<Text>().text = top5.Top5[i].Nombre;          // Nombre
                producto.transform.GetChild(4).GetComponent<Truncar>().Trunca(18);

                producto.transform.GetChild(3).GetComponent<Text>().text = top5.Top5[i].Agave;              // Marca
                producto.transform.GetChild(6).GetComponent<Text>().text = string.Format("€ " + top5.Top5[i].Precio); // Precio.ToString ("F")); //Precio
                for (int j = 0; j < 5; j++)
                {
                    int k = (top5.Top5[i].Calificacion);
                    if (j < k)
                    {                                                       // Calificacion ,con Mageys
                        producto.transform.GetChild(5).transform.GetChild(j).GetComponent<Image>().sprite = mageylleno;
                    }
                }
                
                producto.GetComponentInChildren<WebImage>().LoadImage(top5.Top5[i].Foto);

                if (top5.Top5[i].URL != "")
                {
                    producto.transform.GetChild(8).GetComponent<ComprarFav>().URLTienda = top5.Top5[i].URL;
                }
                else
                {
                    producto.transform.GetChild(8).GetComponent<Button>().interactable = false;
                }
                producto.name = top5.Top5[i].ProductID;
                if (top5.Top5[i].Favorito)
                {
                    producto.transform.GetChild(9).GetComponent<Image>().sprite = CorazonAmarillo;
                }
                else
                {
                    producto.transform.GetChild(9).GetComponent<Image>().sprite = CorazonVerde;
                }
            }
        }

        var resumen = (GameObject)Instantiate(PrefabTendencias, TendenciasContent.transform);
        resumen.transform.localScale = new Vector3(1, 1, 1);
        resumen.transform.GetChild(2).GetComponent<Text>().text = FavoritosContent.transform.childCount + " Favoritos"; // Cantidad de Favoritos añadidos
        Estadisticas(resumen.gameObject);

    }

    public void Estadisticas(GameObject Todo)
    {
        if (Fav.Favoritos != null)
        {
            for (int i = 0; i < Fav.Agaves.Length; i++)
            {
                var cat1 = Todo.transform.GetChild(3).transform.GetChild(i).gameObject;
                cat1.GetComponent<Text>().text = Fav.Agaves[i].Agave;
                cat1.transform.GetChild(0).GetComponent<Text>().text = Fav.Agaves[i].Porcentaje + "%";
                cat1.transform.GetChild(0).transform.GetChild(0).GetComponent<RectTransform>().sizeDelta = new Vector2((Fav.Agaves[i].Porcentaje) * 3, 30);
            }
            for (int j = 0; j < Fav.Regiones.Length; j++)
            {
                var cat2 = Todo.transform.GetChild(4).transform.GetChild(j).gameObject;
                cat2.GetComponent<Text>().text = Fav.Regiones[j].Region;
                cat2.transform.GetChild(0).GetComponent<Text>().text = Fav.Regiones[j].Porcentaje + "%";
                cat2.transform.GetChild(0).transform.GetChild(0).GetComponent<RectTransform>().sizeDelta = new Vector2((Fav.Regiones[j].Porcentaje) * 3, 30);
            }
            for (int k = 0; k < Fav.Alcohol.Length; k++)
            {
                var cat3 = Todo.transform.GetChild(5).transform.GetChild(k).gameObject;
                cat3.GetComponent<Text>().text = Fav.Alcohol[k].Alcohol+"°";
                cat3.transform.GetChild(0).GetComponent<Text>().text = Fav.Alcohol[k].Porcentaje + "%";
                cat3.transform.GetChild(0).transform.GetChild(0).GetComponent<RectTransform>().sizeDelta = new Vector2((Fav.Alcohol[k].Porcentaje) * 3, 30);
            }
        }
        LayoutRebuilder.ForceRebuildLayoutImmediate(TendenciasContent.GetComponent<RectTransform>());
    }

    void vaciar(GameObject objeto)
    {
        for (int i = 0; i < objeto.transform.childCount; i++)
        {
            Destroy(objeto.transform.GetChild(i).gameObject);
        }
    }


    public void VerFicha()
    {
        GameObject fichaM = (GameObject)GameObject.Instantiate(PrefabFichaM);
        fichaM.GetComponent<FichaMezcales>().Iniciar(gameObject.name);
    }


}
