﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class UpdateUser : MonoBehaviour
{
    public ProfileImage sectionProfileImage;
    public ProfileImage hamburgerMenuProfileImage;

    public InputField Nombre;
    public InputField correo;
    public RawImage Foto;
    public GameObject Mensaje;
    public GameObject perfil;
    public Button cambiarImagenBoton;
    public Button reiniciarContraseñaBoton;
    public Button guardarBoton;
    public Text leyendaApple;
    public Text leyendaFacebook;

    CulturaMezcalJsons.Perfil pf = new CulturaMezcalJsons.Perfil();
    CulturaMezcalJsons.updateUser udp = new CulturaMezcalJsons.updateUser();

    public void cargar()
    {
        if (PlayerPrefs.HasKey("UserID"))
        {
            string correo = PlayerPrefs.GetString("Correo");
            string pass = PlayerPrefs.GetString("Password");
            if (PlayerPrefs.HasKey("FB"))
            {
                CulturaMezcalJsons.Login log = new CulturaMezcalJsons.Login();
                log.Correo = pass;
                log.Password = pass;
                string json = JsonUtility.ToJson(log);
                WSClient.instance.StartWebService("user/login", json, Extraer, (r) => MessageBox.instance.ShowMessage("Hubo un problema de conexión."));
            }
            else if (PlayerPrefs.HasKey("Apple"))
            {
                this.correo.text = PlayerPrefs.GetString("Correo", "");
                this.Nombre.text = PlayerPrefs.GetString("Nombre", "");
                
                pf = new CulturaMezcalJsons.Perfil();
                pf.Correo = PlayerPrefs.GetString("Correo", "");
                pf.Nombre = PlayerPrefs.GetString("Nombre", "");
                pf.UserID = PlayerPrefs.GetString("UserID", "");
            }
            else
            {
                CulturaMezcalJsons.Login log = new CulturaMezcalJsons.Login();
                log.Correo = correo;
                log.Password = pass;
                string json = JsonUtility.ToJson(log);
                WSClient.instance.StartWebService("user/login", json, Extraer, (r) => MessageBox.instance.ShowMessage("Hubo un problema de conexión."));
            }
            SetupUI();
        }
    }

    public void Extraer(WSResponse response)
    {
        string result = response.responseTxt;
        pf = JsonUtility.FromJson<CulturaMezcalJsons.Perfil>(result);
        correo.text = pf.Correo;
        Nombre.text = pf.Nombre;
    }

    public void cambiar()
    {
        udp.Correo = correo.text;
        udp.Nombre = Nombre.text;
        udp.UserID = pf.UserID;
        string json = JsonUtility.ToJson(udp);
        WSClient.instance.StartWebService("user/update", json, Cambiado, (r) => MessageBox.instance.ShowMessage("Hubo un problema de conexión."));
    }

    void SetupUI()
    {
        bool isFB = PlayerPrefs.HasKey("FB");
        bool isApple = PlayerPrefs.HasKey("Apple");
        this.correo.interactable = !isApple && !isFB;
        this.Nombre.interactable = !isApple && !isFB;
        cambiarImagenBoton.interactable = !isFB;
        reiniciarContraseñaBoton.gameObject.SetActive(!(isApple || isFB));
        guardarBoton.gameObject.SetActive(!isFB);
        leyendaApple.gameObject.SetActive(isApple);
        leyendaFacebook.gameObject.SetActive(isFB);
    }

    void Cambiado(WSResponse response)
    {
        string result = response.responseTxt;
        CulturaMezcalJsons.Exito ex = new CulturaMezcalJsons.Exito();
        ex = JsonUtility.FromJson<CulturaMezcalJsons.Exito>(result);
        if (ex.Success)
        {
            // Cambiado correcto
            MessageBox.instance.ShowMessage("Datos modificados");
            PlayerPrefs.SetString("Correo", correo.text);
            PlayerPrefs.SetString("Nombre", Nombre.text);
            if (Foto.texture != null)
            {
                UpdateFoto();
            }
        }
        else
        {
            Debug.Log("Datos No cambiados");
        }
    }

    public void CambiarPass()
    {
        CulturaMezcalJsons.updateUserPwd udp = new CulturaMezcalJsons.updateUserPwd();
        udp.Correo = correo.text;
        string json = JsonUtility.ToJson(udp);
        WSClient.instance.StartWebService("user/new/password", json, Solicitar, (r) => MessageBox.instance.ShowMessage("Hubo un problema de conexión."));
    }

    public void Solicitar(WSResponse response)
    {
        string result = response.responseTxt;
        CulturaMezcalJsons.Exito ex = new CulturaMezcalJsons.Exito();
        ex = JsonUtility.FromJson<CulturaMezcalJsons.Exito>(result);
        if (ex.Success)
        {
            //Mensaje.transform.parent.transform.parent.gameObject.SetActive(true);
            //Mensaje.GetComponent<Text>().text = "Se le ha enviado un correo electrónico con un vínculo para que actualice su contraseña";
            MessageBox.instance.ShowMessage("Se le ha enviado un correo electrónico con un vínculo para que actualice su contraseña");
        }
    }

    void UpdateFoto()
    {
        Debug.Log("Registrando Usuario con imagen de perfil");
        CulturaMezcalJsons.UploadPhoto photoJson = new CulturaMezcalJsons.UploadPhoto();
        photoJson.UserID = PlayerPrefs.GetString("UserID");
        byte[] textureBytes = (Foto.texture as Texture2D).EncodeToJPG(80);

        Debug.Log("Saving photo to: " + Application.persistentDataPath + "/ProfileImage.jpg");
        System.IO.File.WriteAllBytes(Application.persistentDataPath + "/ProfileImage.jpg", textureBytes);

        sectionProfileImage.rawImage.texture = Foto.texture;
        hamburgerMenuProfileImage.rawImage.texture = Foto.texture;

        photoJson.Foto = System.Convert.ToBase64String(textureBytes);
        WSClient.instance.StartWebService("user/new/photo", JsonUtility.ToJson(photoJson), OnPhotoSentSuccess, (r) => MessageBox.instance.ShowMessage("Hubo un problema de conexión."));
    }

    void OnPhotoSentSuccess(WSResponse response)
    {
        Debug.Log("Envio exitoso de imagen");
    }
}
