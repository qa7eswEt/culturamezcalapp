﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class ListaMezcales : MonoBehaviour {
    public GameObject prefabrelacionados;
    public Text Clave;
    public Transform Contect;
    // Use this for initialization
    public void llenar(CulturaMezcalJsons.BusquedaTag result, string valor)
    {
        Clave.text = valor;
        foreach (var item in result.Resultados)
        {
            GameObject mez = GameObject.Instantiate(prefabrelacionados,Contect);
            mez.GetComponent<DatosMezcalLista>().Iniciar(item);
            mez.transform.localScale = new Vector3(1, 1, 1);

        }

    }
    public void cerrar()
    {
        Destroy(gameObject);
    }

    
}
