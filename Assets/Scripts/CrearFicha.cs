﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class CrearFicha : MonoBehaviour
{
	public GameObject PrefabFichaM;
    public GameObject MenuDesplegable;
    public FichaMezcales.WillCloseDelegate OnCloseFicha;

    private void Start()
    {
        
    }

    public void VerFicha()                                      
    {
        if (MenuDesplegable != null) MenuDesplegable.SetActive(false);                                     //  ei el menu esta prendido apagarlo
		GameObject fichaM = (GameObject)GameObject.Instantiate (PrefabFichaM,GetLastParent());
        if(gameObject.transform.root.name== "PerfilUsuario"){                                               // si el padre d ela ramma es Perfil de Uausario
            MenuDesplegable.SetActive(false);                                                                  // todas sus fichas apagaran el menu desplegable 
        }

		fichaM.GetComponent<FichaMezcales> ().Iniciar (gameObject.transform.parent.name);
        if (OnCloseFicha != null) fichaM.GetComponent<FichaMezcales>().OnClose += OnCloseFicha;
    }
    private Transform GetLastParent()
    {
        Transform currentParent = transform.parent;
        while(currentParent.parent != null)
        {
            currentParent = currentParent.parent;
        }
        return currentParent;
    }
}
