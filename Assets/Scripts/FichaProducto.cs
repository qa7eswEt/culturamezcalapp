﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using CulturaMezcalJsons;
using System;
public class FichaProducto : MonoBehaviour {
	//public GameObject OpinionEscrita;	// Activador ( Titulo )
	public GameObject PrefabOpiniones;	
	public GameObject Opinion;			// Objeto contendero de Opiniones
	public Sprite Magueylleno;			// Imagen
	public Text CantidadOpinones;		// Etiqueta que muestar la Cantidad de Opiniones que contiene este Mezcal
	public GameObject Ficha;			// Localizacion de la instancia Us para los datos del usuario de Toda la interfaz ( Canvas Ficha Producto)
	public AllOpiniones op = new AllOpiniones();					// instancia de Areglo de Todas las Opiniones
    public bool desplegado;
    public GameObject vermas;
    public GameObject vermenos;
    public GameObject PreNohayComentario;
    //public GameObject WSClient;

    public Opinion us2 = new Opinion ();		// instacia para todos los demas consultas,  con referencia a las Opiniones
	// Use this for initialization

	int contadorOpinones=0;				
	public void Inicio (){
			
		us2.ProductID = Ficha.transform.GetComponent<FichaMezcales>().us.ProductID;	// Referencia de IdProducto delñ valor de Ficha
		ConsultarMas ();									// Iniciar la vista en el comentario mkas actual de su respectiva pagina 
	}

	// Recorrer la Pagina hasta la ultima
	public void ConsultarMas(){
        vaciar();                                    // Limpiar  Valores anteriores del Contect
        contadorOpinones = 3;                               // Variable de inicio de apertura de los primeros 3 comentarios
        desplegado = false;                                 // La ficha esta no desplegada 
        int page = 0;                                       // iniciar cargando en la pagina 0
		us2.Pagina = page;									// asignar Valor a la Instancia 
		string json2 = JsonUtility.ToJson(us2);				// Convertir en Json
        vermenos.SetActive(false);                          // Apagar Boton de Vermenos
        vermas.SetActive(true);                             // prender boton de Ver mas
		WSClient.instance.StartWebService("mezcal/opiniones", json2, Opino, (r) => MessageBox.instance.ShowMessage("Hubo un problema de conexión."));  // Obtener Opiniones del mezcal
    }


	// Deviar Opiniones  

	public void Opino(WSResponse response){	
        string result = response.responseTxt;
		if(result!= "No Content")                       // El contenido debe de tener un Jso para proseguir 
        {
		    op=JsonUtility.FromJson<AllOpiniones>(result);	// Recojer Valores y Convertirlas en Objeto
            
                
            if (op.Opiniones.Length < contadorOpinones) // las opiniones deven ser  menores al contador para reajustar indice
            {
                contadorOpinones = op.Opiniones.Length;

            }
            if (op.LastPage)                            // solo si la pagina de los comentarios no indica que es la ultima 
            {
                vermas.SetActive(false);                // apagar el boton de Ver mas
            }
            else
            {
                vermas.SetActive(true);
            }
           
		VerTodos (contadorOpinones,op);                 // Proceder a Crear Prefabs apartir de un Indice Establecido (Encontrado)
       
        }
        CantidadOpinones.text = "(" + op.Totales + ")";
        if (op.Totales == 0)
        {
          Instantiate(PreNohayComentario, Opinion.transform, false);
            vermas.SetActive(false);
        }
    }

	// Cargar 3 Comentarios Mas

	public void Vermas(){

		if (desplegado){			// Solo los primeros 3

            if (!op.LastPage)
            {               // Mismo Proceso sin embargo diferente Funcion de Carga y Relkacion con Page se conviuerte en Agregacion 
                int page = us2.Pagina;
                page++;
                us2.Pagina = page;
                string json3 = JsonUtility.ToJson(us2);
                WSClient.instance.StartWebService("mezcal/opiniones", json3, Opino, (r) => MessageBox.instance.ShowMessage("Hubo un problema de conexión."));
              
            }
            else
            {
                Debug.Log("No hay mas Paginas ");
              
            }
        }
        else {
        vaciar();
        VerTodos(op.Opiniones.Length, op);		// Proceder a Creacion de Prefab
        desplegado = true;
        }
	}

	// Cargar 3 comentarios antes de cada Pagina
	public void VerMenos(){
        desplegado = false;
         ConsultarMas();
        }

	public void VerTodos (int a,AllOpiniones all)		// Metodo para crear nuevas instancias de cada opinion 
	{
        if (all.Opiniones.Length>0&&all.Opiniones!=null){
            //	pos.y -= 100;	// -100 de espaciado inicia
            for (int i=0; i < a; i++) {
                var producto = (GameObject)Instantiate (PrefabOpiniones, Opinion.transform,false);					
				producto.transform.GetChild (0).GetComponent<Text> ().text = all.Opiniones [i].Nombre;
				producto.transform.GetChild (1).GetComponent<Text> ().text = all.Opiniones [i].Comentario;
				foreach (Transform item in producto.transform.GetChild (2)) {
					int k = Int32.Parse (item.name);
					if (k <= all.Opiniones [i].Calificacion) {
						item.GetComponent<Image> ().sprite = Magueylleno;
					}
               producto.transform.localScale = new Vector3(1, 1, 1);
				
			}
		}
            //    Canvas.ForceUpdateCanvases();
      
        }
        Ficha.GetComponent<ContetZise>().ApagarContent();
      
    }
public void vaciar()						// Borrar todos los elementos hijos de un respetibo objeto
	{
		foreach (Transform item in Opinion.transform) {	// acceder a cada hijo
			Destroy (item.gameObject);					// eliminar 
		}
	}
   
}
