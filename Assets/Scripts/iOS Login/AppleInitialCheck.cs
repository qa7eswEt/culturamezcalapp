using System.Collections;
using UnityEngine;

public class AppleInitialCheck : MonoBehaviour
{
#if UNITY_IOS
    static AppleInitialCheck _instance = null;
    public static AppleInitialCheck Instance => _instance;

    void Awake()
    {
        if (_instance == null)
        {
            _instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    void Start()
    {
        Init();
    }

    void Init()
    {
        StartCoroutine(CheckInitialCredentials());
    }

    IEnumerator CheckInitialCredentials()
    {
        bool checkForAppleId = PlayerPrefs.GetInt("Apple", 0) == 1;
        Debug.Log($"Checking for previous apple id: {checkForAppleId}");
        if (checkForAppleId)
        {
            Debug.Log($"Checking past apple credential status...");
            AppleAuthentication appleAuthentication = FindObjectOfType<AppleAuthentication>();
            bool initialCheckSuccess = true;
            bool credentialsCheckCompleted = false;
            bool credentialsState = false;
            appleAuthentication.CheckCredentialStatus((b) =>
            {
                credentialsCheckCompleted = true;
                credentialsState = b;
            });
            yield return new WaitUntil(() => credentialsCheckCompleted);

            if (credentialsState)
            {
                // Keep userid
                Debug.Log($"Apple credential status OK. Login in with last values:\nApple Id: {appleAuthentication.AppleUserId}\nUser Id: {PlayerPrefs.GetString("UserID", "")}");
            }
            else
            {
                //Skip QuickLogin
                initialCheckSuccess = false;

                //     // Try QuickLogin
                //     Debug.Log($"Apple credential status failed. Trying quick login...");

                //     bool quickLoginCompleted = false;
                //     AppleAuthentication.AppleCredentialsPayload appleCredentialsResponse = null;
                //     appleAuthentication.TryQuickLogin((credentials) =>
                //     {
                //         quickLoginCompleted = true;
                //         appleCredentialsResponse = credentials;
                //     });

                //     yield return new WaitUntil(() => quickLoginCompleted);

                //     if (appleCredentialsResponse.success)
                //     {
                //         // Login
                //         var loginAppleRequest = new CulturaMezcalJsons.LoginAppleRequest()
                //         {
                //             AuthorizationCode = appleCredentialsResponse.authorizationCode,
                //             IdentityToken = appleCredentialsResponse.identityToken
                //         };

                //         bool responseRecieved = false;
                //         CulturaMezcalJsons.Perfil response = null;
                //         WSClient.instance.StartWebService("user/login/apple", JsonUtility.ToJson(loginAppleRequest),
                //             (r) =>
                //             {
                //                 responseRecieved = true;
                //                 response = JsonUtility.FromJson<CulturaMezcalJsons.Perfil>(r.responseTxt);
                //             },
                //             (r) =>
                //             {
                //                 responseRecieved = true;
                //                 initialCheckSuccess = false;
                //             }
                //         );

                //         yield return new WaitUntil(() => responseRecieved);
                //         if (response != null)
                //         {
                //             Debug.Log($"Apple quick login success!");
                //             LoginApple(response, appleCredentialsResponse.user);
                //         }
                //     }
                //     else
                //     {
                //         Debug.Log($"Quick login failed.");
                //         initialCheckSuccess = false;
                //     }
            }

            if (!initialCheckSuccess)
            {
                // Clear sesion data
                Debug.Log($"Apple initial check failed. Removing session data.");
                PlayerPrefs.DeleteAll();
                if (System.IO.File.Exists(Application.persistentDataPath + "/ProfileImage.jpg"))
                {
                    System.IO.File.Delete(Application.persistentDataPath + "/ProfileImage.jpg");
                }
            }
        }
    }

    void LoginApple(CulturaMezcalJsons.Perfil perfil, string appleId)
    {
        PlayerPrefs.SetString("Correo", perfil.Correo);
        PlayerPrefs.SetString("UserID", perfil.UserID);
        PlayerPrefs.SetString("Nombre", perfil.Nombre);
        PlayerPrefs.SetInt("Apple", 1);
        PlayerPrefs.SetInt("FB", 0);
        PlayerPrefs.SetString("AppleId", appleId);

        Debug.Log($"Loading profile:\nUser Id:{perfil.UserID}\nFull name:{perfil.Nombre}\nEmail:{perfil.Correo}\nApple Id:{appleId}");
    }

#endif
}