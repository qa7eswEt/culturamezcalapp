﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using UnityEngine;
#if UNITY_IOS
using AppleAuth.Enums;
using AppleAuth.Interfaces;
using AppleAuth;
using AppleAuth.Native;
#endif

public class AppleAuthentication : MonoBehaviour
{
#if UNITY_IOS
    private IAppleAuthManager appleAuthManager;
    private string AppleUserIdKey = "AppleUserIdKey";

    public string AppleUserId
    {
        get => PlayerPrefs.GetString(AppleUserIdKey, "");
        set => PlayerPrefs.SetString(AppleUserIdKey, value);
    }

    public bool IsApple => PlayerPrefs.HasKey("Apple");

    void Awake()
    {

        // // If the current platform is supported
        // if (AppleAuthManager.IsCurrentPlatformSupported)
        // {
        //     // Creates a default JSON deserializer, to transform JSON Native responses to C# instances
        //     var deserializer = new PayloadDeserializer();
        //     // Creates an Apple Authentication manager with the deserializer
        //     this.appleAuthManager = new AppleAuthManager(deserializer);
        // }

        // Creates a default JSON deserializer, to transform JSON Native responses to C# instances
        var deserializer = new PayloadDeserializer();
        // Creates an Apple Authentication manager with the deserializer
        this.appleAuthManager = new AppleAuthManager(deserializer);

        // Listen to credentials revoked notification
        this.appleAuthManager.SetCredentialsRevokedCallback(result =>
        {
            // Sign in with Apple Credentials were revoked.
            // Discard credentials/user id and go to login screen.
            if (IsApple)
                PlayerPrefs.DeleteAll();
            UnityEngine.SceneManagement.SceneManager.LoadScene(0);
        });
    }

    void Update()
    {

        // Updates the AppleAuthManager instance to execute
        // pending callbacks inside Unity's execution loop
        if (this.appleAuthManager != null)
        {
            this.appleAuthManager.Update();
        }

    }

    public void TryLogin(Action<AppleCredentialsPayload> callback)
    {
        var loginArgs = new AppleAuthLoginArgs(LoginOptions.IncludeEmail | LoginOptions.IncludeFullName);

        this.appleAuthManager.LoginWithAppleId(
            loginArgs,
            credential =>
            {
                // Obtained credential, cast it to IAppleIDCredential
                var appleIdCredential = credential as IAppleIDCredential;

                string authCode = System.Text.Encoding.UTF8.GetString(appleIdCredential.AuthorizationCode);
                string idToken = System.Text.Encoding.UTF8.GetString(appleIdCredential.IdentityToken);

                // You should save the user ID somewhere in the device
                // And now you have all the information to create/login a user in your system
                AppleUserId = credential.User;


                var callbackPayload = new AppleCredentialsPayload()
                {
                    success = true,
                    authorizationCode = authCode,
                    identityToken = idToken,
                    user = appleIdCredential.User,
                    fullname = appleIdCredential.FullName != null ? AppleAuth.Extensions.PersonNameExtensions.ToLocalizedString(appleIdCredential.FullName) : null,
                    email = appleIdCredential.Email
                };

                Debug.Log($"-------" +
                    $"\nApple login credential recieved!: " +
                    $"\nAuthorization code: {authCode}" +
                    $"\nId Token: {idToken}" +
                    $"\nUser Id:{appleIdCredential.User}" +
                    $"\nFull name: {callbackPayload.fullname}" +
                    $"\nemail: {callbackPayload.email}" +
                    $"\n-------"
                );

                callback?.Invoke(callbackPayload);
            },
            error =>
            {
                // Something went wrong
                Debug.LogWarning($"Login failed.");
                PlayerPrefs.DeleteAll();
                var callbackPayload = new AppleCredentialsPayload()
                {
                    success = false
                };
                callback?.Invoke(callbackPayload);
            });
    }

    public void TryQuickLogin(Action<AppleCredentialsPayload> callback)
    {
        var quickLoginArgs = new AppleAuthQuickLoginArgs();

        this.appleAuthManager.QuickLogin(
            quickLoginArgs,
            credential =>
            {
                // Received a valid credential!
                // Try casting to IAppleIDCredential or IPasswordCredential

                // Previous Apple sign in credential
                var appleIdCredential = credential as IAppleIDCredential;

                // Saved Keychain credential (read about Keychain Items)
                var passwordCredential = credential as IPasswordCredential;

                AppleUserId = credential.User;

                AppleCredentialsPayload callbackPayload = null;

                if (appleIdCredential != null)
                {
                    string authCode = System.Text.Encoding.UTF8.GetString(appleIdCredential.AuthorizationCode);
                    string idToken = System.Text.Encoding.UTF8.GetString(appleIdCredential.IdentityToken);

                    callbackPayload = new AppleCredentialsPayload()
                    {
                        success = true,
                        user = appleIdCredential.User,
                        authorizationCode = authCode,
                        identityToken = idToken,
                    };

                    Debug.Log($"-------" +
                        $"\nApple quick login credential recieved!: " +
                        $"\nAuthorization code: {authCode}" +
                        $"\nId Token: {idToken}" +
                        $"\nUser Id:{appleIdCredential.User}" +
                        $"\n-------"
                    );
                }
                else
                {
                    // How do keychain credentials work?
                    Debug.LogWarning($"Keychain credentials are not yet supported!");
                    PlayerPrefs.DeleteAll();
                    callbackPayload = new AppleCredentialsPayload()
                    {
                        success = false
                    };
                }
                callback?.Invoke(callbackPayload);
            },
            error =>
            {
                // Quick login failed. Go to login screen
                Debug.LogWarning($"Quick login failed.");
                PlayerPrefs.DeleteAll();
                var callbackPayload = new AppleCredentialsPayload()
                {
                    success = false
                };
                callback?.Invoke(callbackPayload);
            });
    }

    public void CheckCredentialStatus(System.Action<bool> callback)
    {
        string userId = AppleUserId;
        if (string.IsNullOrEmpty(userId))
        {
            callback.Invoke(false);
            return;
        }

        this.appleAuthManager.GetCredentialState(
            userId,
            state =>
            {
                switch (state)
                {
                    case CredentialState.Authorized:
                        // User ID is still valid. Login the user.
                        callback.Invoke(true);
                        break;

                    case CredentialState.Revoked:
                        // User ID was revoked. Go to login screen.
                        callback.Invoke(false);
                        break;

                    case CredentialState.NotFound:
                        // User ID was not found. Go to login screen.
                        callback.Invoke(false);
                        break;
                }
            },
            error =>
            {
                // Something went wrong
                callback.Invoke(false);
            });
    }

    public class AppleCredentialsPayload
    {
        public bool success;
        public string authorizationCode;
        public string identityToken;
        public string user = null;
        public string fullname = null;
        public string email = null;
    }

    [System.Serializable]
    public class FailedUserData
    {
        [System.Serializable]
        public class User
        {
            public string appleId = null;
            public string fullname = null;
            public string email = null;
        }

        [SerializeField] List<User> _userCollection = new List<User>();

        public User FindUser(string appleId)
        {
            return _userCollection.FirstOrDefault((user) => user.appleId.Equals(appleId));
        }

        public void AddUser(User user)
        {
            _userCollection.Add(user);
        }

        public void RemoveUser(User user)
        {
            _userCollection.Remove(user);
        }

        public static FailedUserData LoadUserFile()
        {
            FailedUserData data = new FailedUserData();
            string path = Application.persistentDataPath + "/" + "u_err.dat";

            if (File.Exists(path))
            {
                BinaryFormatter bf = new BinaryFormatter();
                using (FileStream fs = new FileStream(path, FileMode.Open))
                {
                    data = bf.Deserialize(fs) as FailedUserData;
                }
            }
            return data;
        }

        public static void SaveUserFile(FailedUserData data)
        {
            string path = Application.persistentDataPath + "/" + "u_err.dat";
            BinaryFormatter bf = new BinaryFormatter();
            using (FileStream fs = new FileStream(path, FileMode.Create))
            {
                bf.Serialize(fs, data);
            }
        }
    }
#endif
}