﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Facebook.Unity;
using System;

public class FBController : MonoBehaviour
{
    public string ShareComment = "¡Quiero compartir esta botella de {0}!";
    public static FBController instance;
    NativeShare Sharer;
    private string shareURL;
    private string shareTitle;
    private string shareMsg;
    private string shareImg;

    public delegate void FBLoginCallback(bool success, FacebookJsons.FBLoginResult result);
    private FBLoginCallback currentLoginCallback;
    private FacebookJsons.FBLoginResult loginResult;

    void Awake()
    {
        if(instance == null)
        {
            instance = this;
            GameObject.DontDestroyOnLoad(gameObject);
        }
        else
        {
            GameObject.Destroy(gameObject);
        }
    }

    private void Start()
    {
        Sharer = new NativeShare();
        Sharer.SetSubject("CulturaMezcal");
        Sharer.SetTitle("Compartir CulturaMezcal");
    }

    #region Login

    /// <summary>
    /// Inicia proceso de login y regresa datos de login en el callback.
    /// </summary>
    /// <param name="loginCallback"></param>
    public void LoginFB(FBLoginCallback loginCallback)
    {
        currentLoginCallback = loginCallback;
        loginResult = new FacebookJsons.FBLoginResult();
        LoginStart();
    }

    /// <summary>
    /// Checa si el sdk esta inicializado y si el usuario esta logeado antes de ejecutar el comando.
    /// </summary>
    private void LoginStart()
    {
        Debug.Log($"Facebook Login Start");
        if (FB.IsInitialized)
        {
            Debug.Log($"Facebook Initialized");
            if (FB.IsLoggedIn)
            {
                Debug.Log($"Facebook Logged In");
                GetLoginInfo();
            }
            else
            {
                Debug.Log($"Facebook non Logged In");
                FB.LogInWithReadPermissions(
                new List<string>() { "public_profile", "email" },
                AuthCallbackLogin
                );
            }
        }
        else
        {
            Debug.Log($"Facebook non-initialized");
            FB.Init(LoginStart);
            //FB.ActivateApp();

        }
    }

    /// <summary>
    /// Callback cuando el usuario hace login, si el login es exitoso se llama ShareApp() de nuevo para continuar con el flujo del share.
    /// </summary>
    /// <param name="result"></param>
    private void AuthCallbackLogin(ILoginResult result)
    {
        if (result == null)
        {
            Debug.LogWarning("Null Response\n");
            MessageBox.instance.ShowMessage("Hubo un problema de conexión.");
            return;
        }
        
        if (!string.IsNullOrEmpty(result.Error))
        {
            Debug.LogWarning("Error Response:\n" + result.Error);
            MessageBox.instance.ShowMessage("Hubo un problema de conexión.");
        }
        else if (result.Cancelled)
        {
            Debug.LogWarning("Cancelled Response:\n" + result.RawResult);
            MessageBox.instance.ShowMessage("Hubo un problema de conexión.");
        }
        else if (!string.IsNullOrEmpty(result.RawResult))
        {
            Debug.LogWarning("Success Response:\n" + result.RawResult);
            GetLoginInfo();
        }
        else
        {
            Debug.LogWarning("Empty Response\n");
            MessageBox.instance.ShowMessage("Hubo un problema de conexión.");
        }
        
    }

    /// <summary>
    /// Envia al graph request de los datos de usuario
    /// </summary>
    private void GetLoginInfo()
    {
        FB.API("/me?fields=email,name,picture", HttpMethod.GET, ProfileDataCallback);
    }

    /// <summary>
    /// Guarda los datos de login en userData, despues baja la imagen de perfil. 
    /// </summary>
    /// <param name="result"></param>
    private void ProfileDataCallback(IGraphResult result)
    {
        if (String.IsNullOrEmpty(result.Error) && !result.Cancelled)
        {
            print(result.RawResult);

            FacebookJsons.FacebookLoginJson loginJson = JsonUtility.FromJson<FacebookJsons.FacebookLoginJson>(result.RawResult);

            loginResult.profileName = loginJson.name;
            loginResult.email = loginJson.email;
            loginResult.id = loginJson.id;

            string fbProfilePictureURL = String.Format("https://graph.facebook.com/{0}/picture?type=normal", loginResult.id);

            StartCoroutine(FetchProfilePic(fbProfilePictureURL));
        }
        else
        {
            currentLoginCallback(false, null);
            // MessageBox.instance.ShowMessage("Hubo un problema de conexión.");
        }
    }

    /// <summary>
    /// Obtiene el perfil de usuario y despues ejecuta el callback de login.
    /// </summary>
    /// <param name="url"></param>
    /// <returns></returns>
    private IEnumerator FetchProfilePic(string url)
    {
        WWW www = new WWW(url);
        yield return www;
        loginResult.profilePicture = www.texture;

        if(string.IsNullOrEmpty(www.error))
        {
            PlayerPrefs.SetString("FB_PicURL", url);
            currentLoginCallback(true, loginResult);
        }
        else
        {
            // MessageBox.instance.ShowMessage("Hubo un problema de conexión.");
            currentLoginCallback(false, null);
        }
    }

    /*
    void UpdateFoto(Texture2D picture)
    {
        Debug.Log("Registrando Usuario con imagen de perfil");
        CulturaMezcalJsons.UploadPhoto photoJson = new CulturaMezcalJsons.UploadPhoto();
        photoJson.UserID = PlayerPrefs.GetString("UserID");
        byte[] textureBytes = picture.EncodeToJPG(50);

        Debug.Log("Saving photo to: " + Application.persistentDataPath + "/ProfileImage.jpg");
        System.IO.File.WriteAllBytes(Application.persistentDataPath + "/ProfileImage.jpg", textureBytes);

        photoJson.Foto = System.Convert.ToBase64String(textureBytes);
        WSClient.instance.StartWebService("user/new/photo", JsonUtility.ToJson(photoJson), OnPhotoSentSuccess, null);
    }
    */
    #endregion

    #region Share

    
    public void Share(string nombreProducto, string urlTienda) 
    {
        Sharer.SetText(string.Format(ShareComment, nombreProducto) + "\n" + urlTienda);
        Sharer.Share();
    }
    #endregion
}

namespace FacebookJsons
{
    public class FBLoginResult
    {
        public Texture profilePicture;
        public string profileName;
        public string email;
        public string id;
    }

    [Serializable]
    public class FacebookLoginJson
    {
        public string email;
        public string name;
        public FacebookLoginPicture picture;
        public string id;
    }

    [Serializable]
    public class FacebookLoginPicture
    {
        public FacebookPictureData data;
    }

    [Serializable]
    public class FacebookPictureData
    {
        public string url;
        public bool is_silhouette;
    }
}




