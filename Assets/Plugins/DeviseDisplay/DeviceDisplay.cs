﻿using UnityEngine;
using System.Collections;
using System.Runtime.InteropServices;

public class DeviceDisplay : MonoBehaviour {
#if UNITY_IOS
	[DllImport ("__Internal")]
	private static extern int _scaleFactor();
#endif
    public static int scaleFactor{
		get{
			int pow = 1;
			// We check for UNITY_IPHONE again so we don't try this if it isn't iOS platform.
			#if UNITY_IOS
			// Now we check that it's actually an iOS device/simulator, not the Unity Player. You only get plugins on the actual device or iOS Simulator.
			if (Application.platform == RuntimePlatform.IPhonePlayer) {
				pow = _scaleFactor ();
			}
			#endif
			// TODO:  You could test for Android, PC, Mac, Web, etc and do something with a plugin for them here.
		
			return pow;
		}
	}
}
