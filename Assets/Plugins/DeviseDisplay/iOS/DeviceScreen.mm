#import <UIKit/UIKit.h>
extern "C"
{
    int _scaleFactor()
    {
        // Just a simple example of returning an int valu
        return [[UIScreen mainScreen] scale];
    }
}
