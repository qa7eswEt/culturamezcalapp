﻿using System;
using System.Collections.Generic;

namespace CulturaMezcalJsons
{

    [Serializable]
    public class MapaDegustacion
    {
        public float Humo;
        public float Madera;
        public float Caramelo;
        public float Citrico;
        public float Hierba;
        public float Tierra;
        public float Fermentos;
        public float Especias;
        public float Frutal;
        public float Alcohol;
        public float Floral;
        public float Mineral;
        public float Humedad;
    }

    [Serializable]
    public class MapaSensacion
    {
        public int Picante;
        public int Aspero;
        public int Calor;
        public int Untuoso;
        public int Fresco;
    }

    [Serializable]
    public class MapaGeneral
    {
        public MapaDegustacion MapaDegustacion;
        public MapaSensacion MapaSensacion;
    }

    [Serializable]
    public class MapaPersonal
    {
        public MapaDegustacion MapaDegustacion;
        public MapaSensacion MapaSensacion;
    }

    [Serializable]
    public class Etiqueta
    {
        public string tag;
        public string val;
    }



    [Serializable]
    public class ProductoRelacionado
    {
        public string ProductID;
        public string Nombre;
        public string Foto;
        public string Agave;
        public int Calificacion;
        public Etiqueta[] EtiquetasRel;
    }

    [Serializable]
    public class FichaMezcal
    {

        public string Nombre;
        public int CalificacionUsuario;
        public int CalificacionPromedio;
        public string TiendaURL;
        public string Foto;
        public string Precio;
        public bool Favorito;
        public string AcercaDe;
        public string Agave;
        public string Region;
        public string Estado;
        public string Maguey;
        public string Maestro;
        public int NumeroDeEstadisticas;
        public int Year;
        public int Litros;
        public string Origen;
        public string Maduracion;
        public string Terruno;
        public string Cocimiento;
        public string Molido;
        public string Agua;
        public string Alcohol;
        public string Fermentacion;
        public string Destilador;
        public string Destilaciones;
        public string Ajuste;
        public string Abocado;
        public MapaGeneral MapaGeneral;
        public MapaPersonal MapaPersonal;
        public ProductoRelacionado[] ProductosRelacionados;
        public Etiqueta[] Etiquetas;
    }

    [Serializable]
    public class Favorito
    {
        public Favoritos[] Favoritos;
        public Agaves[] Agaves;
        public Regiones[] Regiones;
        public Alcoho[] Alcohol;
    }

    [Serializable]
    public class Favoritos
    {
        public string Nombre;
        public int Calificacion;
        public string Agave;
        public string Foto;
        public string Precio;
        public string Fecha;
        public string URL;
        public string IdProducto;
    }
    [Serializable]
    public class Agaves
    {
        public string Agave;
        public int Porcentaje;
    }
    [Serializable]
    public class Regiones
    {
        public string Region;
        public int Porcentaje;
    }

    [Serializable]
    public class Alcoho
    {
        public string Alcohol;
        public int Porcentaje;
    }

    [Serializable]
    public class Historico
    {
        public Degustaciones[] Degustaciones;
    }
    [Serializable]
    public class Producto
    {
        public string Nombre;
        public int Calificacion;
        public string Agave;
        public string Foto;
        public string Precio;
        public string URL;
        public string IdProducto;
    }
    [Serializable]


    public class Degustaciones
    {
        public string id;
        public string fecha;
        public int Calificacion;
        public bool Favorito;

        public Producto producto;

    }
    [Serializable]
    public class Tendencias
    {
        public Top5[] Top5;
    }

    [Serializable]
    public class Top5
    {
        public string Nombre;
        public int Calificacion;
        public string Agave;
        public string Foto;
        public string Precio;
        public string URL;
        public string ProductID;
        public bool Favorito;
    }


    [Serializable]
    public class SendSearch
    {
        public string Query;
        public bool Human;
    }

    [Serializable]
    public class SearchResultInfo
    {
        public string ID;
        public string Nombre;
        public int Calificacion;
        public string Foto;
        public string Agave;
        public string URL;
        public int Coeficiente;
    }

    public class SerachIndex
    {
        public List<string> Ids;

        public SerachIndex(string id)
        {
            Ids = new List<string>();
            Ids.Add(id);
        }
    }
    [Serializable]
    public class SearchResults
    {
        public SearchResultInfo[] Resultados;
    }

    [Serializable]
    public class SearchResultsText
    {
        public SearchResultInfoText[] Resultados;
    }


    [Serializable]
    public class SearchResultInfoText
    {
        public string ID;
        public string Nombre;
        public int Calificacion;
        public string Foto;
        public string Agave;
        public string URL;
        public int Coeficiente;
        public string Estado;
        public string Maguey;
        public string Region;
    }

    [Serializable]
    public class UsuarioID
    {
        public string UserID;
        public string ProductID;
    }

    [Serializable]
    public class Opinion
    {
        public string ProductID;
        public int Pagina;
    }

    [Serializable]
    public class Opiniones
    {
        public int Calificacion;
        public string Nombre;
        public string Comentario;
    }
    [Serializable]
    public class AllOpiniones
    {
        public Opiniones[] Opiniones;
        public bool LastPage;
        public int Totales;
    }

    [Serializable]
    public class Comentarios
    {
        public string UserID;
        public string ProductID;
        public string Comentario;
    }

    [Serializable]
    public class Calificar
    {
        public string UserID;
        public string ProductID;
        public int Calificacion;
    }

    [Serializable]
    public class Exito
    {
        public bool Success;
    }

    [Serializable]
    public class Usuario
    {
        public string UserID;
    }

    [Serializable]
    public class AddUsuario
    {
        public string Password;
        public string Nombre;
        public string Correo;
        public string Tipo;
    }

    [Serializable]
    public class RegistroAppleRequest
    {
        public string AuthorizationCode;
        public string IdentityToken;
        public string Nombre;
        public string Correo;
        public string Tipo;
    }

    [Serializable]
    public class RegistroAppleResponse
    {
        public string UserID;
    }

    [Serializable]
    public class LoginAppleRequest
    {
        public string AuthorizationCode;
        public string IdentityToken;
    }

    [Serializable]
    public class UploadPhoto
    {
        public string UserID;
        public string Foto;
    }

    [Serializable]
    public class Pin
    {
        public string Device;
    }
    [Serializable]
    public class Login
    {
        public string Correo;
        public string Password;


    }
    [Serializable]
    public class Perfil
    {
        public string UserID;
        public string Foto;
        public string Nombre;
        public string Correo;
    }

    [Serializable]
    public class EnviarMapa
    {
        public string UserID;
        public string ProductID;
        public int Humo;
        public int Madera;
        public int Caramelo;
        public int Citrico;
        public int Hierba;
        public int Tierra;
        public int Fermentos;
        public int Especias;
        public int Frutal;
        public int Alcohol;
        public int Floral;
        public int Mineral;
        public int Humedad;
        public int Picante;
        public int Aspero;
        public int Calor;
        public int Untuoso;
        public int Fresco;
    }


    [Serializable]
    public class updateUser
    {
        public string UserID;
        public string Nombre;
        public string Correo;
    }

    [Serializable]
    public class updateUserPwd
    {
        public string Correo;
    }

    [Serializable]
    public class updatephoto
    {
        public string UserID;
        public string Foto;
    }
    [Serializable]
    public class BusquedaTag
    {
        public ResultadosTag[] Resultados;
    }

    [Serializable]
    public class ResultadosTag
    {
        public string ID;
        public string Nombre;
        public int Calificacion;
        public string Foto;
        public string Agave;
        public string URL;
    }

    [Serializable]
    public class BuscarTag
    {
        public string Tag;
        public string Valor;
    }

}