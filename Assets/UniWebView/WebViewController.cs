using System;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.Events;

public class WebViewController : MonoBehaviour
{

    public RectTransform WebViewTransform;
	public GameObject uniWebViewInstanced;
    public GameObject LoginPanel;
    public GameObject WebviewPanel;


    public Button closeButton;
    public Button backButton;
    public Button forwardButton;
    public Button reloadButton;

    private bool gotError;

    public float timeoutTime;

    private IEnumerator timer;

    private bool initialized = false;
    private bool loading = false;

    void Start()
    {
        if (!initialized) Initialize();
    }

    void Initialize()
    {
        gotError = false;
        timeoutTime = 60;
        
        UniWebView wvInstanceado = uniWebViewInstanced.GetComponent<UniWebView>();
        wvInstanceado.OnPageFinished += (view, statusCode, url) =>
        {
            Debug.Log("OnPageFinished");
            wvInstanceado.Show();
            LoginPanel.SetActive(false);
            loading = false;
        };

        wvInstanceado.OnShouldClose += (wv) =>
        {
            Debug.Log("OnShouldClose");
            LoginPanel.SetActive(false);
            DestroyWebview();
            return true;
        };

        wvInstanceado.OnWebContentProcessTerminated += (wv) =>
        {
            Debug.Log("OnWebContentProcessTerminated");
            LoginPanel.SetActive(false);
            DestroyWebview();
        };

        wvInstanceado.OnPageErrorReceived += (wv, errCode, errMessage) =>
        {
            Debug.Log($"OnPageErrorReceived: ({errCode}): {errMessage}");
            LoginPanel.SetActive(false);
            loading = false;
        };
        
        wvInstanceado.SetBackButtonEnabled(false);
        
        initialized = true;
    }

    private void OnApplicationFocus(bool hasFocus)
    {
        if (loading)
        {
            loading = false;
            LoginPanel.SetActive(false);
            DestroyWebview();
        }
    }

    /// <summary>
    /// Comienza la carga del URL proporcionado en el webview y activa la barra 
    /// </summary>
    /// /// <param name="URL">URL de la noticia que se abrirá en el navegador</param>
    /// /// <param name="loNavigationBar">Referencia a la barra de navegación para activarla y desactivarla cuando se necesario</param>
    public void OpenLink(string URL)
    {
        //UniWebView wvInstanceado = GameObject.Instantiate(goUniWebView).GetComponent<UniWebView>();
        //UniWebViewCanvas.SetActive(true);
        WebviewPanel.SetActive(true);
        LoginPanel.SetActive(true);
        uniWebViewInstanced.SetActive(true);

        if (!initialized) Initialize();
        UniWebView wvInstanceado = uniWebViewInstanced.GetComponent<UniWebView>();

        //timer = TimeoutTimer(wvInstanceado);
        wvInstanceado.ReferenceRectTransform = WebViewTransform;
        //wvInstanceado.OnLoadComplete -= OnLoadComplete;

        //wvInstanceado.OnLoadComplete += OnLoadComplete;
        
        //<< Setup de la barra de navegación
        NavigationBarSetup(wvInstanceado);

        gotError = false;
        loading = true;
        Debug.Log("go to: " + URL);
        //WSClient.instance.ShowLoadingScreen(true);
        //wvInstanceado.CleanCache();
        wvInstanceado.Load(URL);
        
        
        //StartCoroutine(timer);
    }



    /// <summary>
    /// Configura todos los elementos necesarios para la barra de navegación del webView.  
    /// </summary>
    void NavigationBarSetup(UniWebView webview)
    {       
        //closeButton.onClick.RemoveAllListeners();
        backButton.onClick.RemoveAllListeners();
        forwardButton.onClick.RemoveAllListeners();

        //<< Referencias a los gameObjects de los botones de anterior, siguiente y cerrar.
        //closeButton.onClick.AddListener(() => { DestroyWebview(); });//CloseWebView(webview);
        backButton.onClick.AddListener(() => { PreviousPage(webview); });
        forwardButton.onClick.AddListener(() => { NextPage(webview); });
        reloadButton.onClick.AddListener(() => { Reload(webview); });

        //goNavigationBar.SetActive(false);
        //blankPage.SetActive(false);
    }

    /// <summary>
    /// Si existen páginas posteriores avanza a la siguiente página 
    /// </summary>
    public void NextPage(UniWebView webview)
    {
        if (webview.CanGoForward)
            webview.GoForward();
    }

    public void Reload(UniWebView webview)
    {
        webview.Reload();
    }

    /// <summary>
    /// Si existen páginas anteriores regresa a la página anterior 
    /// </summary>
    public void PreviousPage(UniWebView webview)
    {
        if (webview.CanGoBack)
            webview.GoBack();
    }

    /// <summary>
    /// Cierra el webview y desactiva la barra de navegación 
    /// </summary>
    public void CloseWebView(UniWebView uniweb)
    {
        //if(timer != null) StopCoroutine(timer);

        closeButton.onClick.RemoveAllListeners();
        backButton.onClick.RemoveAllListeners();
        forwardButton.onClick.RemoveAllListeners();
        reloadButton.onClick.RemoveAllListeners();
        uniweb.Hide();
        uniweb.CleanCache();
        
        uniweb.Stop();

        //uniweb.Hide();



        WebviewPanel.SetActive(false);
        LoginPanel.SetActive(false);

        uniWebViewInstanced.SetActive(false);
        //UniWebViewCanvas.SetActive(false);
        //uniweb.OnLoadComplete -= OnLoadComplete;
    }

    /// <summary>
    /// Evento automático al terminar de realizarse la carga  
    /// </summary>
    void OnLoadComplete(UniWebView webView, bool success, string errorMessage)
    {
        if (timer != null) StopCoroutine(timer);
        //Debug.Log("TimeoutTimer detenido");

        WSClient.instance.ShowLoadingScreen(false);

        if (success && !gotError)
        {


            

            webView.Show();
        } else
        {
            ShowErrorMessage();
            Debug.Log("Something wrong in web view loading: " + errorMessage + "\n and url: " + webView.Url);
            CloseWebView(webView);
            gotError = true;
        }
    }

    IEnumerator TimeoutTimer(UniWebView uniweb)
    {
        Debug.Log("TimeoutTimer iniciado");
        yield return new WaitForSeconds(timeoutTime);
        CloseWebView(uniweb);
        ShowErrorMessage();
        Debug.Log("TimeoutTimer terminado por tiempo");
    }

    public void ReloadScene()
    {
        WSClient.instance.ShowLoadingScreen(true);
    }

    private void ShowErrorMessage()
    {
        MessageBox.instance.ShowMessage("Hubo un problema de conexión.");
        WSClient.instance.ShowLoadingScreen(false);
    }

    public void DestroyWebview()
    {
        Destroy(gameObject);
    }
}
