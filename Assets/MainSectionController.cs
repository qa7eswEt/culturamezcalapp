﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainSectionController : MonoBehaviour
{
    public GameObject[] sections;                   // secciones de menu de la barra de navegacion inferior
    public GameObject menuDesplegable;              // Menu desplegable del perfil de usuario.
    public LoginPerfil loginScript;                 // LoginUsuario Menu de Canvas
    public WSSearch AuxSearch;
    public bool fromCamera;
    public static MainSectionController instance;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            //GameObject.DontDestroyOnLoad(gameObject);
        }
  
    }


    public void ActivateSection(int canvasIndex)
    {
        Debug.Log("ActivateSection");
        menuDesplegable.SetActive(false);             // Apagar Menu desplegable
        foreach (GameObject section in sections)
        {
            if (section != null && section.activeSelf) section.SetActive(false);    // activar o apar las secciones correspondientes 
        }

        if (sections[canvasIndex] != null) sections[canvasIndex].SetActive(true);
        if (canvasIndex == 3 && !fromCamera)
            AuxSearch.GetProductsInfo();
        //StartCoroutine(GetData());
    }

    public void ComesFromCamera(bool value) 
    {
        fromCamera = value;
    }

    IEnumerator GetData() 
    {
        yield return new WaitForSeconds(1.4f);
        Transform.FindObjectOfType<WSSearch>().GetProductsInfo();
    }

    public void ActivateUserProfile()
    {
        loginScript.Iniciar();
        if (PlayerPrefs.HasKey("UserID"))
        {
            ActivateSection(4);             // encender Perfil de usuario.
            BarraDeNavegacion.instance.TurnButtonOn(4);
        }
    }

    public void ActivatarSeccionBarraNavegacion(int index)
    {
        BarraDeNavegacion.instance.ActivateSection(index);
    }
}
